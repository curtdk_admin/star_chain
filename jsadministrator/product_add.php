<?
require_once("../config/dbconn.php");require_once("../config/powercls.php");

if ($id){
	$sql1="select * from {$db_prefix}products where id='$id'";
	$rs1=$db->get_one($sql1);
}

if($act=='add'){
	$msg='';
	if(trim($psortid)=='') $msg.="请输入产品分类\\n";
	if(trim($productname)=='') $msg.="请输入产品名\\n";
	if(trim($scprice)=='') $msg.="请输入市场价\\n";
	if(trim($price)=='') $msg.="请输入会员价\\n";
	if(trim($gcprice)=='') $msg.="请输入积分价\\n";
	if(trim($pv)=='') $msg.="请输入积分\\n";
	if ($msg!=''){
		echo "<script>alert('$msg');history.back();</script>";exit();
	}
	$pimgname="";
	if ($_FILES['file']['size']>0){
		list($fwidth, $fheight, $ftype, $fattr) = getimagesize($_FILES['file']['tmp_name']);
		switch($ftype){
			case 1:$ftype1=".gif";break;
			case 2:$ftype1=".jpg";break;
			case 3:$ftype1=".png";break;
			default:$ftype1="...";break;
		}
		if ($ftype1=="...") $msg.="图片格式错误\\n";
		$proimgpth="../uploads/productimgs/";
		$pimgname=rand(100,999).time().$ftype1;
		if (!@copy($_FILES['file']['tmp_name'],$proimgpth.$pimgname)) $msg.="图片上传失败\\n";
	}
	
	if ($msg!=''){
		echo "<script>alert('$msg');history.back();</script>";exit();
	}
	
	if ($pimgname!=''){
		@unlink($proimgpth.$rs1['pimg']);
	}
	
	//插入数据库
	$dataArray['psortid']=$psortid;
	$dataArray['productname']=$productname;
	if ($pimgname!='') $dataArray['pimg']=$pimgname;
	$dataArray['price']=$price;
	$dataArray['scprice']=$scprice;
	$dataArray['gcprice']=$gcprice;
	$dataArray['pv']=$pv;
	$dataArray['content']=addslashes($content);
	$dataArray['addtime']=$curtime;
	
	if ($id){
		$condition="id='$id'";
		$db->update("{$db_prefix}products",$dataArray,$condition);
		echo "<script>alert('产品已修改');location.href='product_lst.php?pageno={$pageno}';</script>";exit();
	}else{
		$db->insert("{$db_prefix}products",$dataArray);
		echo "<script>alert('产品添加成功');location.href='product_lst.php';</script>";exit();
	}
	
}
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	
}
-->
</style>

<link href="images/skin.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" height="29" valign="top" background="images/mail_leftbg.gif"><img src="images/left-top-right.gif" width="17" height="29" /></td>
    <td width="935" height="29" valign="top" background="images/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">产品<?=($id)?"修改":"添加"?></div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="images/mail_rightbg.gif"><img src="images/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td height="71" valign="middle" background="images/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9"><table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td height="13" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td valign="top">
		<form action="?act=add" method="post" enctype="multipart/form-data" name="form1" id="form1">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td colspan="3"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="nowtable">
                <tr>
                  <td class="left_bt2">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td height="30" colspan="3">
			  
			  <table width="100%" height="240" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td height="30" align="center" bgcolor="#f2f2f2" class="left_txt">分类</td>
                  <td height="30" bgcolor="#f2f2f2" class="left_txt"><select name="psortid">
                    <option value="">请选择</option>
                    <?
		$sql="select * from {$db_prefix}psort where 1 order by orders asc";
		$result=$db->query($sql);
		while($rs=$db->fetch_array($result)){
			echo "<option value='".$rs['id']."'";
			if ($rs1['psortid']==$rs['id']) echo " selected";
			echo ">".str_repeat("------",$rs['depth']-1).$rs['flname']."</option>";
		}
		$db->free_result($result);
		?>
                  </select></td>
                  </tr>
                <tr>
                  <td height="30" align="center" class="left_txt">产品</td>
                  <td height="30" class="left_txt"><input name="productname" type="text" id="productname" value="<?=$rs1['productname']?>" /></td>
                  </tr>
                <tr>
                  <td height="30" align="center" bgcolor="#f2f2f2" class="left_txt">图片</td>
                  <td height="30" bgcolor="#f2f2f2" class="left_txt"><label>
                  <input type="file" name="file">
                  </label></td>
                </tr>
                <tr>
                  <td height="30" align="center" class="left_txt">市场价</td>
                  <td height="30" class="left_txt"><label>
                    <input name="scprice" type="text" id="scprice" value="<?=$rs1['scprice']?>">
                  </label></td>
                </tr>
				   <tr>
                  <td height="30" align="center" bgcolor="#f2f2f2" class="left_txt">购物券</td>
                  <td height="30" bgcolor="#f2f2f2" class="left_txt"><label>
                    <input name="price" type="text" id="price" value="<?=$rs1['price']?>">
                  </label></td>
                </tr>
                <td height="30" align="center" bgcolor="#f2f2f2" class="left_txt">积分</td>
                  <td height="30" bgcolor="#f2f2f2" class="left_txt"><label>
                    <input name="gcprice" type="text" id="gcprice" value="<?=$rs1['gcprice']?>">
                  </label></td>
                </tr>
				   <tr>
				     <td height="30" align="center" class="left_txt">赠送积分</td>
				     <td height="30" class="left_txt"><label>
				       <input name="pv" type="text" id="pv" value="<?=$rs1['pv']?>">
				     </label></td>
				     </tr>
				   <tr>
				     <td height="30" align="center" bgcolor="#f2f2f2" class="left_txt">说明</td>
				     <td height="30" bgcolor="#f2f2f2" class="left_txt">&nbsp;</td>
				     </tr>
				   <tr>
                  <td height="30" colspan="2" align="center" bgcolor="#f2f2f2" class="left_txt">
				  <script charset="utf-8" src="../kindeditor/kindeditor-min.js"></script>
			<script charset="utf-8" src="../kindeditor/lang/zh_CN.js"></script>
					<script>
					KindEditor.ready(function(K) {
						K.create('#content', {
							themeType : 'simple'
						});
					});
				</script>
				<textarea id="content" name="content" style="width:100%;height:200px;visibility:hidden;"><?=stripslashes($rs1['content'])?></textarea>
				  </td>
                  </tr>
              </table></td>
            </tr>
            
            <tr>
              <td height="30" colspan="3">&nbsp;</td>
            </tr>
            <tr>
              <td width="50%" height="30" align="right"><input name="id" type="hidden" id="id" value="<?=$id?>">
              <input name="pageno" type="hidden" id="pageno" value="<?=$pageno?>">
<input type="submit" value="完成以上修改" name="B1" /></td>
              <td width="6%" height="30" align="right">&nbsp;</td>
              <td width="44%" height="30"><input type="reset" value="取消设置" name="B12" /></td>
            </tr>
            <tr>
              <td height="30" colspan="3">&nbsp;</td>
            </tr>
          </table>
		  </form>
		  </td>
      </tr>
    </table></td>
    <td background="images/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="middle" background="images/mail_leftbg.gif"><img src="images/buttom_left2.gif" width="17" height="17" /></td>
      <td height="17" valign="top" background="images/buttom_bgs.gif"><img src="images/buttom_bgs.gif" width="17" height="17" /></td>
    <td background="images/mail_rightbg.gif"><img src="images/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>

</body>
</html>