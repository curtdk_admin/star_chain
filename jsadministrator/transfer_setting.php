<?
require_once("../config/dbconn.php");require_once("../config/powercls.php");

//获取设置
// $config = file_get_contents('./transfer_setting_config.php');
include('./transfer_setting_config.php');
$act = isset($_GET['act']) ? $_GET['act'] : '';
$is_post = isset($_POST['B1']) ? $_POST['B1'] : '';
if(isset($_POST['B1']) && $act == 'edit'){
    $is_open = isset($_POST['is_open']) ? intval($_POST['is_open']) : 0;
    $allow_users = isset($_POST['allow_users']) ? txl_trim($_POST['allow_users']) : '';
    $black_list = isset($_POST['blacklist']) ? txl_trim($_POST['blacklist']) : '';
    if(!empty($allow_users)){
        $allow_users = str_replace('，',',' , $allow_users);
        $allow_users = strtoupper($allow_users);
    }

    if(!empty($black_list)){
        $black_list = str_replace('，',',' , $black_list);
        $black_list = strtoupper($black_list);
    }
    if(!in_array($is_open,array(1,2))) $msg = '请选择是否开启点对点转账';
    if ($msg!=''){
        echo "<script>alert('$msg');history.back();</script>";exit();
    }
    $data = array('is_open'=>$is_open,'allow_users'=>$allow_users,'blacklist'=>$black_list);
    file_put_contents('./transfer_setting_config.php', '<?php $transfer_setting = '.var_export($data,true).';');
    echo "<script>alert('修改成功');history.back();</script>";exit();
}
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
<!--
body {
    margin-left: 0px;
    margin-top: 0px;
    margin-right: 0px;
    margin-bottom: 0px;
    
}
.red{
    color:red
}
td{
    font-size:12px;
}
-->
</style>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/utils.js"></script>
<script type="text/javascript" src="js/user_edit.js"></script>
<link href="images/skin.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" height="29" valign="top" background="images/mail_leftbg.gif"><img src="images/left-top-right.gif" width="17" height="29" /></td>
    <td width="935" height="29" valign="top" background="images/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">点对点转账设置</div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="images/mail_rightbg.gif"><img src="images/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td height="71" valign="middle" background="images/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9"><table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td height="13" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td valign="top">
        <form id="form1" name="form1" method="post" action="?act=edit" onSubmit="return editdo1(this);">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td colspan="3"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="nowtable">
                <tr>
                  <td class="left_bt2">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td height="30" colspan="3">
              
              <table width="100%" height="750" border="0" cellpadding="0" cellspacing="0">
                
                <tr>
                  <td height="30" align="center" bgcolor="#f2f2f2" class="left_txt">是否启用</td>
                  <td height="30" bgcolor="#f2f2f2" class="left_txt"><label>
                    <input type="radio" name="is_open" id="" <?php if($transfer_setting['is_open'] == 1) echo 'checked = "checked"'; ?> value="1" />开启
                    <input type="radio" name="is_open" id="" <?php if($transfer_setting['is_open'] == 2) echo 'checked = "checked"'; ?> value="2" />关闭
                    <span class="red">*</span>
                  </label></td>
                </tr>
                <tr>
                  <td height="30" align="center" bgcolor="#f2f2f2" class="left_txt">允许使用此功能的用户列表</td>
                  <td height="30" bgcolor="#f2f2f2" class="left_txt"><label>
                    <textarea name="allow_users" id="" cols="100" rows="10"><?php echo $transfer_setting['allow_users']; ?></textarea>
                    <span class="red">*</span>
                  </label><br /><span class="red">注:多个用户编号用逗号,隔开。留空说明对全部用户开放</span></td>
                </tr>
                <tr>
                  <td height="30" align="center" bgcolor="#f2f2f2" class="left_txt">黑名单限制</td>
                  <td height="30" bgcolor="#f2f2f2" class="left_txt"><label>
                    <textarea name="blacklist" id="" cols="100" rows="10"><?php echo $transfer_setting['blacklist']; ?></textarea>
                    <span class="red">*</span>
                  </label><br /><span class="red">注:多个用户编号用逗号,隔开。留空说明所有用户都可使用</span></td>
                </tr>
              </table></td>
            </tr>
            
            <tr>
              <td height="30" colspan="3" align="center"><label id="reg_notice" class="red"></label></td>
            </tr>
            <tr>
              <td width="50%" height="30" align="right"><span class="left_txt">
                <input name="pageno" type="hidden" id="pageno" value="<?=$pageno?>">
                <input type="hidden" name="id" id="id" value="<?=$id?>">
                </span>
                <input type="submit" value="完成以上修改" name="B1" /></td><td width="6%" height="30" align="right">&nbsp;</td>
                <td width="44%" height="30">&nbsp;</td>
            </tr>
            <tr>
              <td height="30" colspan="3">&nbsp;</td>
            </tr>
          </table>
          </form>
          </td>
      </tr>
    </table></td>
    <td background="images/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="middle" background="images/mail_leftbg.gif"><img src="images/buttom_left2.gif" width="17" height="17" /></td>
      <td height="17" valign="top" background="images/buttom_bgs.gif"><img src="images/buttom_bgs.gif" width="17" height="17" /></td>
    <td background="images/mail_rightbg.gif"><img src="images/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>

</body>
</html>