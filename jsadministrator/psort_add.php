<?
require_once("../config/dbconn.php");require_once("../config/powercls.php");

if($act=='add'){
	$msg='';
	if(trim($flname)=='') $msg="请输入分类名\\n";
	if ($msg!=''){
		echo "<script>alert('$msg');history.back();</script>";exit();
	}
	
	if (!$id){
		//获取下列参数
		$orders=1;$depth=1;$path='';
		if ($fid==0){
			$sqlm="select id,orders,depth,path from {$db_prefix}psort where 1 order by orders desc limit 1";
			$rsm=$db->get_one($sqlm);
			if ($rsm['id']){
				$orders=$rsm['orders']+1;
				$depth=1;
				$path='';
			}else{
				$orders=1;
				$depth=1;
				$path='';
			}
			
		}else{
			$sqlm="select id,orders,depth,path from {$db_prefix}psort where id='$fid'";
			$rsm=$db->get_one($sqlm);
			$depth=$rsm['depth']+1;
			if ($rsm['path']) $path=$rsm['path'].",".$rsm['id'];else $path=$rsm['id'];
			//获取这个分类同级别的上一个
			$sqlm1="select id,orders,depth,path from {$db_prefix}psort where fid='$fid' order by orders desc limit 1";
			$rsm1=$db->get_one($sqlm1);
			if ($rsm1['id']){
				$mid1=$rsm1['id'];
				//获取他的下级最大的orders
				$sqlxj="select * from {$db_prefix}psort where find_in_set('$mid1',path)>0 order by orders desc limit 1";
				$rsxj=$db->get_one($sqlxj);
				if ($rsxj['id']){
					$orders=$rsxj['orders']+1;
				}else{
					$orders=$rsm1['orders']+1;
				}
			}else{
				$orders=$rsm['orders']+1;
			}		
			
		}
	}else{
		//修改的时候不需要变化
	}
	//插入数据库
	if (!$id){
		$sqlcr="insert into {$db_prefix}psort(flname,fid,path,depth,addtime,orders) values('$flname','$fid','$path','$depth','$curtime','$orders')";
		$db->query($sqlcr);
		$CRID=$db->insert_id();
		//更新orders
		$sqlgx="update {$db_prefix}psort set orders=orders+1 where id!='$CRID' and orders>='$orders'";
		$db->query($sqlgx);
		echo "<script>alert('产品分类添加成功');location.href='psort_lst.php';</script>";exit();
	}else{
		$sqlgx="update {$db_prefix}psort set flname='$flname' where id='$id'";
		$db->query($sqlgx);
		echo "<script>alert('产品分类已修改');location.href='psort_lst.php?pageno={$pageno}';</script>";exit();
	}
	
}

if ($id){
	$sql1="select * from {$db_prefix}psort where id='$id'";
	$rs1=$db->get_one($sql1);
}
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	
}
-->
</style>

<link href="images/skin.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" height="29" valign="top" background="images/mail_leftbg.gif"><img src="images/left-top-right.gif" width="17" height="29" /></td>
    <td width="935" height="29" valign="top" background="images/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">产品分类<?=($id)?"修改":"添加"?></div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="images/mail_rightbg.gif"><img src="images/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td height="71" valign="middle" background="images/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9"><table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td height="13" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td valign="top">
		<form id="form1" name="form1" method="post" action="?act=add">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td colspan="3"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="nowtable">
                <tr>
                  <td class="left_bt2">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td height="30" colspan="3">
			  
			  <table width="100%"  border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td height="30" align="center" bgcolor="#f2f2f2" class="left_txt">上一级</td>
                  <td height="30" bgcolor="#f2f2f2" class="left_txt">
				  <?
				  if (!$id){
				  ?>
				  <select name="fid">
                    <option value="0">顶级分类</option>
                    <?
		$sql="select * from {$db_prefix}psort where 1 order by orders asc";
		$result=$db->query($sql);
		while($rs=$db->fetch_array($result)){
			echo "<option value='".$rs['id']."'";
			if ($fid==$rs['id']) echo " selected";
			if ($rs['id']==$rs1['fid']) echo " selected";
			echo ">".str_repeat("------",$rs['depth']-1).$rs['flname']."</option>";
		}
		$db->free_result($result);
		?>
                  </select>
				  <?
				  }else{
				  	$cpath=$rs1['path'];
					$pathary=explode(',',$cpath);
					foreach($pathary as $v1){
						if ($v1>0){
							$sqlf="select flname from {$db_prefix}psort where id='$v1'";
							$rsf=$db->get_one($sqlf);
							echo "--".$rsf['flname'];
						}else{
							echo "顶层";
						}
					}
				  }
				  ?>				  </td>
                  </tr>
                <tr>
                  <td height="30" align="center" class="left_txt">产品分类</td>
                  <td height="30" class="left_txt"><input type="text" name="flname" value="<?=$rs1['flname']?>" /></td>
                  </tr>
              </table></td>
            </tr>
            
            <tr>
              <td height="30" colspan="3">&nbsp;</td>
            </tr>
            <tr>
              <td width="50%" height="30" align="right"><input name="pageno" type="hidden" id="pageno" value="<?=$pageno?>">
              <input name="id" type="hidden" id="id" value="<?=$id?>">
              <input type="submit" value="完成以上修改" name="B1" /></td>
              <td width="6%" height="30" align="right">&nbsp;</td>
              <td width="44%" height="30"><input type="reset" value="取消设置" name="B12" /></td>
            </tr>
            <tr>
              <td height="30" colspan="3">&nbsp;</td>
            </tr>
          </table>
		  </form>
		  </td>
      </tr>
    </table></td>
    <td background="images/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="middle" background="images/mail_leftbg.gif"><img src="images/buttom_left2.gif" width="17" height="17" /></td>
      <td height="17" valign="top" background="images/buttom_bgs.gif"><img src="images/buttom_bgs.gif" width="17" height="17" /></td>
    <td background="images/mail_rightbg.gif"><img src="images/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>

</body>
</html>