<?
require_once("../config/dbconn.php");require_once("../config/powercls.php");
$sql="select * from {$db_prefix}imtoken where id = {$id}";
$result = $db->get_one($sql);
#已处理完成
if($act == 'finish'){
    if(!$result){
        echo '<script>alert("处理失败,请稍候再试");</script>';
    }else{
        if($result['status'] != 0){
            echo '<script>alert("该记录已被处理");</script>';
        }else{
            $update_sql = "update {$db_prefix}imtoken set status = 1 ,finishtime = ".time()." where id = {$id}";
            $res = $db->query($update_sql);
            if(!$res){
                echo '<script>alert("处理失败,请稍候再试");</script>';
            }else{
                echo '<script>alert("处理成功");</script>';
            }
        }    
    }
    
}

#处理失败或拒绝处理
if($act == 'no_finish'){
    $remark = trim($remark);
   if(!$result){
        echo '<script>alert("处理失败,请稍候再试");</script>';
   }else{
      if($result['status'] != 0){
            echo '<script>alert("该记录已被处理");</script>';
      }elseif(!empty($remark)){
            $db->query('start transaction');#开启事物
          try{
                $update_sql = "update {$db_prefix}imtoken set status = 2 ,finishtime = ".time()." ,remark = '{$remark}'  where id = {$id}";
                $res = $db->query($update_sql);
                if(!$res){
                    throw new Exception("处理失败");            
                }

                // $update_user = "update {$db_prefix}users set jyprice = jyprice+{$result['price']} where id = {$result['userid']}";
                // $user_res = $db->query($update_user);
                // if(!$user_res){
                //     throw new Exception("处理失败");                    
                // }
                // hyepricejl($result['userid'],$result['price'],17,5,time(),$remark);
                $db->query('commit');#提交
                echo '<script>alert("处理成功");</script>';

          }  catch(Expection $e){
                $db->query('rollback');#回滚
                $e->getMessage();
                echo '<script>alert("处理失败,请稍候再试");</script>';
          }
      }else{
                echo '<script>alert("处理失败,备注不能为空");</script>';

      }          
   }
    
}

$sql="select im.id,u.username,u.nickname,im.imtoken,im.price,im.status,im.remark,im.addtime,im.finishtime,im.wallet_type from {$db_prefix}users as u inner join {$db_prefix}imtoken as im on u.id = im.userid where im.id = {$id}";
$rs=$db->get_one($sql);

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	
}
-->
</style>

<link href="images/skin.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" height="29" valign="top" background="images/mail_leftbg.gif"><img src="images/left-top-right.gif" width="17" height="29" /></td>
    <td width="935" height="29" valign="top" background="images/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">ImToken查看</div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="images/mail_rightbg.gif"><img src="images/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td height="71" valign="middle" background="images/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9"><table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td height="13" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td valign="top">
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td colspan="3"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="nowtable">
                <tr>
                  <td class="left_bt2">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td height="30" colspan="3">
			  
			  <table width="100%" height="120" border="0" cellpadding="0" cellspacing="0">

                <tr>
                  <td align="center" bgcolor="#f2f2f2" class="left_txt">用户名</td>
                  <td height="30" bgcolor="#f2f2f2" class="left_txt"><? echo $rs['username']?></td>
                </tr>
                <tr>
                  <td align="center" class="left_txt">姓名</td>
                  <td height="30" class="left_txt">
                    <?=$rs['nickname']?>                 </td>
                </tr>
				   
                  <tr style="display:">
                      <td align="center" class="left_txt">钱包地址</td>
                      <td height="30" class="left_txt"><? echo $rs['imtoken']?></td>
                  </tr>
                  <tr>
                      <td align="center" class="left_txt">钱包类型</td>
                      <td height="30" class="left_txt"><?php if($rs['wallet_type'] == 0) echo '未知'; elseif($rs['wallet_type'] == 1) echo '交易钱包'; elseif($rs['wallet_type'] == 2) echo '购物钱包';else echo '其他钱包';?></td>
                  </tr>
                  <tr>
                      <td align="center" class="left_txt">金额</td>
                      <td height="30" class="left_txt"><? echo $rs['price']?></td>
                  </tr>
                  <tr style="display:">
                      <td align="center" class="left_txt">处理状态</td>
                      <td height="30" class="left_txt"><?php if($rs['status'] == 0) echo '未处理'; elseif($rs['status'] == 1) echo '已处理完成'; elseif($rs['status'] == 2) echo '已拒绝处理';?></td>
                  </tr>
                  <tr style="display:">
                      <td align="center" class="left_txt">备注</td>
                      <td height="30" class="left_txt"><? echo $rs['remark']?></td>
                  </tr>
                  <tr style="display:">
                      <td align="center" class="left_txt">申请时间</td>
                      <td height="30" class="left_txt"><?php if($rs['addtime']) echo date("Y-m-d H:i:s",$rs['addtime']);else echo '--:--'; ?></td>
                  </tr>
                  <tr style="display:">
                      <td align="center" class="left_txt">处理时间</td>
                      <td height="30" class="left_txt"><?php if($rs['finishtime']) echo date("Y-m-d H:i:s",$rs['finishtime']);else echo '--:--'; ?></td>
                  </tr>
                   <tr>
                     <td align="center" bgcolor="#f2f2f2" class="left_txt">&nbsp;</td>
                     <td height="30" bgcolor="#f2f2f2" class="left_txt">&nbsp;</td>
                    </tr>
                 <tr style="display:">
                      <td align="center" class="left_txt"><a href="imtoken_view.php?act=finish&id=<?=$rs['id']?>">已处理完成</a></td>
                      <td align="center" class="left_txt"><a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">处理失败</a></td>
                  </tr>
                   <tr></tr>
              </table></td>
            </tr>
            
            <tr></tr>
            <tr>
              <td width="50%" height="30" align="right"><input type="button" value="返回列表" name="B1" onClick="location.href='imtoken.php';" /></td>
              <td width="6%" height="30" align="right">&nbsp;</td>
              <td width="44%" height="30">&nbsp;</td>
            </tr>
            <tr>
              <td height="30" colspan="3">&nbsp;</td>
            </tr>
          </table>
		 
		  </td>
      </tr>
    </table></td>
    <td background="images/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="middle" background="images/mail_leftbg.gif"><img src="images/buttom_left2.gif" width="17" height="17" /></td>
      <td height="17" valign="top" background="images/buttom_bgs.gif"><img src="images/buttom_bgs.gif" width="17" height="17" /></td>
    <td background="images/mail_rightbg.gif"><img src="images/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>
<!-- <p>示例弹出层：<a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">请点这里</a></p>  -->
<div id="light" class="white_content">拒绝/处理失败. 
    <a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'">点这里关闭本窗口</a>
    <form action="imtoken_view.php?id=<?=$rs['id']?>" method="post">
        <input type="hidden" name="act" value="no_finish">
        <input type="hidden" name="id" value="<?php echo $rs['id']?>">
        <textarea name="remark" id="" cols="30" rows="10" placeholder='请输入拒绝/处理失败原因'></textarea><br />
        <input type="submit"  value="提交">
    </form>
</div> 
<div id="fade" class="black_overlay"></div> 
<style> 
        .black_overlay{ 
            display: none; 
            position: absolute; 
            top: 0%; 
            left: 0%; 
            width: 100%; 
            height: 100%; 
            background-color: black; 
            z-index:1001; 
            -moz-opacity: 0.8; 
            opacity:.80; 
            filter: alpha(opacity=88); 
        } 
        .white_content { 
            display: none; 
            position: absolute; 
            top: 25%; 
            left: 25%; 
            width: 55%; 
            height: 55%; 
            padding: 20px; 
            border: 4px solid #336699; 
            background-color: white; 
            z-index:1002; 
            overflow: auto; 
        } 
    </style>
</body>
</html>
