<?
require_once("../config/dbconn.php");require_once("../config/powercls.php");

if($act=='add'){
	$msg='';
	if(trim($father)=='') $msg="请选择所在城市\\n";
	if(trim($area)=='') $msg="请输入区县\\n";
	if ($msg!=''){
		echo "<script>alert('$msg');history.back();</script>";exit();
	}
	//
	
	//插入数据库
	$dataArray['father']=$father;
	$dataArray['area']=$area;
	if ($id){
		$condition="id='$id'";
		$db->update("{$db_prefix}area",$dataArray,$condition);
		echo "<script>alert('区县已修改');location.href='area_lst.php?pageno={$pageno}';</script>";exit();
	}else{
		//获取最大的cityID
		$sqlg="select max(areaID) as c from {$db_prefix}area";
		$rsg=$db->get_one($sqlg);
		$areaID=($rsg['c']+1);
		$dataArray['areaID']=$areaID;
		$db->insert("{$db_prefix}area",$dataArray);
		echo "<script>alert('区县添加成功');location.href='area_lst.php';</script>";exit();
	}	
}

if ($id){
	$sql1="select * from {$db_prefix}area where id='$id'";
	$rs1=$db->get_one($sql1);
	$sql2="select * from {$db_prefix}city where cityID='".$rs1['father']."'";
	$rs2=$db->get_one($sql2);
}
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/utils.js"></script>
<script type="text/javascript" src="js/area_add.js"></script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	
}
td{
	font-size:12px;
}
.red{
	color:red;
}
-->
</style>

<link href="images/skin.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" height="29" valign="top" background="images/mail_leftbg.gif"><img src="images/left-top-right.gif" width="17" height="29" /></td>
    <td width="935" height="29" valign="top" background="images/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">区县<?=($id)?"修改":"添加"?></div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="images/mail_rightbg.gif"><img src="images/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td height="71" valign="middle" background="images/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9"><table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td height="13" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td valign="top">
		<form id="form1" name="form1" method="post" action="?act=add" onSubmit="return area_addsubmit(this);">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td colspan="3"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="nowtable">
                <tr>
                  <td class="left_bt2">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td height="30" colspan="3">
			  
			  <table width="100%" height="60" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td height="30" align="center" bgcolor="#f2f2f2" class="left_txt">省份城市</td>
                  <td height="30" bgcolor="#f2f2f2" class="left_txt"><label>
                    <select name="province" id="province" onChange="provincechange(this.value)">
					<option value="">请选择</option>
					<?
					$sqlsf="select province,provinceID from {$db_prefix}province where 1";
					$resultsf=$db->query($sqlsf);
					while($rssf=$db->fetch_array($resultsf)){
						echo "<option value='".$rssf['provinceID']."'";
						if ($rssf['provinceID']==$rs2['father']) echo " selected";
						echo ">".$rssf['province']."</option>";
					}
					$db->free_result($resultsf);
					?>
                    </select>
                  -
                  <select name="father" id="father">
				  <?
				  if ($rs1['father']){
				  	$sqlcs="select city,cityID from {$db_prefix}city where father='".$rs2['father']."'";
					$resultcs=$db->query($sqlcs);
					while($rscs=$db->fetch_array($resultcs)){
						echo "<option value='".$rscs['cityID']."'";
						if ($rscs['cityID']==$rs1['father']) echo " selected";
						echo ">".$rscs['city']."</option>";
					}
					$db->free_result($resultcs);
				  }
				  ?>
                  </select>
                  </label>  <label class="red" id="province_notice"></label></td>
                </tr>
                <tr>
                  <td height="30" align="center" class="left_txt">区县</td>
                  <td height="30" class="left_txt"><label>
                    <input name="area" type="text" id="area" value="<?=$rs1['area']?>">
                  </label> <label class="red" id="area_notice"></label></td>
                </tr>
              </table></td>
            </tr>
            
            <tr>
              <td height="30" colspan="3" align="center">&nbsp;
                <label class="red" id="reg_notice"></label></td>
            </tr>
            <tr>
              <td width="50%" height="30" align="right"><input name="id" type="hidden" id="id" value="<?=$id?>">
              <input name="pageno" type="hidden" id="pageno" value="<?=$pageno?>">
<input type="submit" value="完成以上修改" name="B1" /></td>
              <td width="6%" height="30" align="right">&nbsp;</td>
              <td width="44%" height="30"></td>
            </tr>
            <tr>
              <td height="30" colspan="3">&nbsp;</td>
            </tr>
          </table>
		  </form>
		  </td>
      </tr>
    </table></td>
    <td background="images/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="middle" background="images/mail_leftbg.gif"><img src="images/buttom_left2.gif" width="17" height="17" /></td>
      <td height="17" valign="top" background="images/buttom_bgs.gif"><img src="images/buttom_bgs.gif" width="17" height="17" /></td>
    <td background="images/mail_rightbg.gif"><img src="images/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>

</body>
</html>