<?
require_once("../config/dbconn.php");require_once("../config/powercls.php");

if($act=='glnetedit'){
	$msg='';
	if(trim($username)=='') $msg.="请输入要移动的用户名\\n";
	if(trim($gluser)=='') $msg.="请输入新的管理用户名\\n";
	if(trim($pos)=='') $msg.="请选择新的区位\\n";
	if ($msg!=''){
		echo "<script>alert('$msg');history.back();</script>";exit();
	}
	//用户名是否存在
	$sqlchk="select id,gluser,confirmtime,regpv,pos,gldepth from {$db_prefix}users where username='".trim($username)."'";
	$rschk=$db->get_one($sqlchk);
	if(!$rschk['id']){
		$msg.="移动的用户不存在\\n";
	}
	if ($msg!=''){
		echo "<script>alert('$msg');history.back();</script>";exit();
	}
	$oldgldepth=$rschk['gldepth'];
	
	//新的管理会员是否存在
	$sqlchk1="select id,username,gldepth from {$db_prefix}users where username='".trim($gluser)."'";
	$rschk1=$db->get_one($sqlchk1);
	if(!$rschk1['id']){
		$msg.="新的管理用户不存在\\n";
	}else{
		$sqlchk2="select id from {$db_prefix}users where gluser='".trim($gluser)."' and pos='$pos'";
		$rschk2=$db->get_one($sqlchk2);
		if ($rschk2['id']) $msg.="新的管理区位已经被占用\\n";
	}
	if ($msg!=''){
		echo "<script>alert('$msg');history.back();</script>";exit();
	}
	$newgldepth=$rschk1['gldepth']+1;
	
	//相差多少？
	$xgldepth=$newgldepth-$oldgldepth;
	
	//新位置是否允许放置
	$sqlgx1="select glstr from {$db_prefix}glgx where userid='".$rschk1['id']."'";
	$rsgx1=$db->get_one($sqlgx1);
	if ($rsgx1['glgx']){
		$glnetary1=explode(',',$rsgx1['glgx']);
		if (in_array($rschk['id'],$glnetary1)){
			$msg.="新位置不允许放置\\n";
		}
	}
	if ($msg!=''){
		echo "<script>alert('$msg');history.back();</script>";exit();
	}
	
	
	//插入到修改记录中
	unset($dataArray);
	$dataArray['username']=trim($username);
	$dataArray['gluser']=trim($gluser);
	$dataArray['pos']=trim($pos);
	$dataArray['oldgluser']=$rschk['gluser'];
	$dataArray['oldpos']=$rschk['pos'];
	$dataArray['addtime']=$curtime;
	$db->insert("{$db_prefix}glnetedit",$dataArray);
	
	//更改会员的glstr和管理深度。业绩不处理了
	$sqlxj="select * from {$db_prefix}glgx where find_in_set('".$rschk['id']."',glstr)>0 or userid='".$rschk['id']."'";
	$resultxj=$db->query($sqlxj);
	while($rsxj=$db->fetch_array($resultxj)){
		$newglstr='';
		if ($rsxj['userid']==$rschk['id']){
			$sqlup="update {$db_prefix}users set gluser='".trim($gluser)."',pos='$pos',gldepth='".$newgldepth."' where username='".trim($username)."'";
			$db->query($sqlup);
			if ($rsgx1['glstr']) $newglstr=$rsgx1['glstr'].",".$rschk1['id'];else $newglstr=$rschk1['id'];
			$sqlup1="update {$db_prefix}glgx set glstr='".trim($newglstr)."' where userid='".$rschk['id']."'";
			$db->query($sqlup1);
		}else{
			$gltmpary=explode(",{$rschk['id']},",$rsxj['glstr']);
			if ($rsgx1['glstr']) $newglstr=$rsgx1['glstr'].",{$rschk['id']},".$gltmpary[1];else $newglstr="{$rschk['id']},".$gltmpary[1];
			$sqlup="update {$db_prefix}users set gldepth=gldepth+".intval($xgldepth)."' where id='".$rsxj['userid']."'";
			$db->query($sqlup);
			$sqlup1="update {$db_prefix}glgx set glstr='".trim($newglstr)."' where userid='".$rsxj['userid']."'";
			$db->query($sqlup1);
		}
	}
	$db->free_result($resultxj);
	
	echo "<script>alert('管理网体修改成功');location.href='nets_glxg.php';</script>";exit();
}
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	
}
.red{
	color:#FF0000
}
-->
</style>

<link href="images/skin.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" height="29" valign="top" background="images/mail_leftbg.gif"><img src="images/left-top-right.gif" width="17" height="29" /></td>
    <td width="935" height="29" valign="top" background="images/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">管理网体修改</div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="images/mail_rightbg.gif"><img src="images/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td height="71" valign="middle" background="images/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9"><table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td height="13" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td valign="top">
		<form id="form1" name="form1" method="post" action="?act=glnetedit">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td colspan="3"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="nowtable">
                <tr>
                  <td class="left_bt2">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td height="30" colspan="3">
			  
			  <table width="100%" height="90" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td width="51%" height="30" align="center" bgcolor="#f2f2f2" class="left_txt">要移动的会员</td>
                  <td width="49%" height="30" bgcolor="#f2f2f2" class="left_txt">
                    <input name="username" type="text" id="username">
                 <label class="red"> *</label></td>
                </tr>
                <tr>
                  <td height="30" align="center" class="left_txt">新的管理会员</td>
                  <td height="30" class="left_txt"><input name="gluser" type="text" id="gluser">
                    <label class="red"> *</label></td>
                </tr>
                <tr>
                  <td height="30" align="center" class="left_txt">新区位</td>
                  <td height="30" class="left_txt"><label>
                    <select name="pos" id="pos">
                      <option value="">请选择</option>
                      <?
					    foreach($sysposary as $k1=>$v1){
							echo "<option value='{$k1}'";
							if ($pos==$k1) echo " selected";
							echo ">{$v1}</option>";
						  }
					   ?>
                    </select>
                  </label>                    <label class="red"> *</label></td>
                </tr>
              </table></td>
            </tr>
            
            <tr>
              <td height="30" colspan="3" align="center"><label></label></td>
            </tr>
            <tr>
              <td width="50%" height="30" align="right"><input type="submit" value="确定修改" name="B1" /></td>
              <td width="6%" height="30" align="right">&nbsp;</td>
              <td width="44%" height="30">&nbsp;</td>
            </tr>
            <tr>
              <td height="30" colspan="3">&nbsp;</td>
            </tr>
          </table>
		  </form>
		  </td>
      </tr>
    </table></td>
    <td background="images/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="middle" background="images/mail_leftbg.gif"><img src="images/buttom_left2.gif" width="17" height="17" /></td>
      <td height="17" valign="top" background="images/buttom_bgs.gif"><img src="images/buttom_bgs.gif" width="17" height="17" /></td>
    <td background="images/mail_rightbg.gif"><img src="images/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>

</body>
</html>