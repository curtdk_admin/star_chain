<?php 

require_once("../config/dbconn.php");
relase();
//资产钱包释放
function relase(){
    global $db,$db_prefix,$curtime,$glo_release_property;
    /*今天的最后一秒*/
    $time1 =  strtotime(date('Y-m-d 23:59:59'));
    $time2 =  strtotime(date('Y-m-d 00:00:00'));
    $release_pro = $glo_release_property;
    /*查询所有可以释放资产的会员*/
    $sql = "select id,username,rank,price,shifang_time from {$db_prefix}users where regtime<{$time2} and price>0 and shifang_time < {$time1}";
    $alluser = $db->get_all($sql);
    foreach ($alluser as $k=>$v){
        $userid = $v['id'];             //会员编号
        $username = $v['username'];     //会员名
        $price = $v['price'];           //资产钱包余额
        /*判断当前会员资产钱包余额*/
        if($price>0){
            $db->query('start transaction');#开启事物
            try{
                //释放金额
                $shifang_money = $price * ($release_pro/100);

                $sqlkk="update {$db_prefix}users set price=price-'".floatval($shifang_money)."',sfprice = sfprice+'".floatval($shifang_money)."',ljsfprice = ljsfprice+'".floatval($shifang_money)."',shifang_time =$time1 where id={$userid}";
                $row =  $db->query($sqlkk);
                if(!empty($row)){
                    //记录财务信息
                    //资产钱包财务记录
                    $e_userid=$userid;$e_price=-floatval($shifang_money);$e_type=4;$e_ptype=1;$e_addtime=$curtime;$e_memo="每日释放,释放前资产钱包余额：".$price;
                    hyepricejl($e_userid,$e_price,$e_type,$e_ptype,$e_addtime,$e_memo);

                    //释放钱包财务记录
                    $e_userid=$userid;$e_price=floatval($shifang_money);$e_type=1;$e_ptype=2;$e_addtime=$curtime;$e_memo="每日释放,释放TXL个数为：".floatval($shifang_money);
                    hyepricejl($e_userid,$e_price,$e_type,$e_ptype,$e_addtime,$e_memo);
                    unset($txldata);
                    $txldata['userid']=$userid;
                    $txldata['username']=$username;
                    $txldata['price']=$price;
                    $txldata['shifang_txl']=floatval($shifang_money);
                    $txldata['add_time']=$curtime;
                    $res2 = $db->insert("{$db_prefix}shifangprice_log",$txldata);
                    if(!$res2){
                        throw new Exception('插入失败');
                    }
                }
            $db->query('commit');#提交
            }catch(Exception $e){
                $db->query('rollback');#回滚
            }
        }
    }
    $db->free_result();
    file_put_contents(APP_ROOT.'\relase.txt','参数:'.serialize($txldata).'======='.PHP_EOL, FILE_APPEND);
    return true;
}
