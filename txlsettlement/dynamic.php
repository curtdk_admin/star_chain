<?php 


require_once("../config/dbconn.php");

dynamicAwards();
//动态奖结算
function dynamicAwards(){
    global $db,$db_prefix,$curtime,$glo_returnDynamicTxl_1,$glo_returnDynamicTxl_2,$glo_returnDynamicTxl_3,$glo_returnDynamicTxl_4,$glo_returnDynamicTxl_5,$glo_returnDynamicTxl_6,$glo_dynamicCapping,$glo_dynamicNum,$glo_heavyAwayMulriple,$glo_heavyAwayRatio,$glo_redeliveryMulriple,$sysposnum;
        //    今天的最后一秒
        $time1 = strtotime(date('Y-m-d 23:59:59'));
        $time2 =  strtotime(date('Y-m-d 00:00:00'));
        //      查询所有可以允许结算动态奖的会员
        $sql = "select id,username,price,sfprice,slprice,jyprice,ljslprice,fh_money from {$db_prefix}users where regtime<{$time2} and (price+sfprice)>0";
        $dynamic_all = $db->get_all($sql);
        foreach ($dynamic_all as $k=>$v){
            $userid = $v['id'];
            $username = $v['username'];
            $jyprice = $v['jyprice'];	//交易钱包
            $ljslprice = $v['ljslprice'];   //算力钱包累计收益
            $ljznprice = $v['fh_money'];   //智能钱包累计收益

            //计算当前会员投资额度
            $investmentAmounts = $v['price'] + $v['sfprice'];

            //复投额度
            $overlapping_investment = $investmentAmounts * $glo_redeliveryMulriple;
            //累计收益（动静结合）
            $ljpriceall =$ljznprice+$ljslprice;
            $ft_isend=0;

            if($overlapping_investment <= $ljpriceall){
                    $ft_isend =1;
                }
            $db->query('start transaction');#开启事物
            try {
                //达到复投条件
                if ($ft_isend == 1) {
                    //改变复投状态
                        $sqlkk = "update {$db_prefix}users set ft_isend=1 where id={$userid}";
                        $row = $db->query($sqlkk);
                        if(!$row){
                            throw new Exception('更新失败');
                        }

                    #记录该会员资产钱包、释放钱包以及累计收益数据
                    unset($txldata);
                    $txldata['userid']=$userid;
                    $txldata['price']=$v['price'];
                    $txldata['sfprice']=$v['sfprice'];
                    $txldata['ljznprice']=$v['fh_money'];
                    $txldata['ljslprice']=$v['ljslprice'];
                    $txldata['addtime']=$curtime;
                    $is_ok =$db->insert("{$db_prefix}redeliverylog",$txldata);
                    if(!$is_ok){
                        throw new Exception('数据备份失败');
                    }

                    # 处理此会员资产钱包、释放钱包以及累计收益数据
                    $sql="select count(userid) as c from wd_redeliverylog where userid={$userid}";
                    $row = $db->get_one($sql);
                    #第一种情况（自动复投）：持币量（资产钱包+释放钱包）<=交易钱包余额（第一轮上限1万，其余不限）
                    #从交易钱包中扣除等量的钱，清空累计收入，不处理会员的资产钱包和释放钱包,并将复投状态置为0（有收益状态）
                    if ($investmentAmounts <= $v['jyprice']) {
                        if($row['c'] ==1 ){
                            if($investmentAmounts>=10000){
                                $ft_money = 10000;
                            }else if($investmentAmounts>0 && $investmentAmounts<10000){
                                $ft_money = $investmentAmounts;                            
                            }
                        }
                    	$sqlclear =$db->query("update wd_users set jyprice=jyprice-{$ft_money},fh_money=0,ljslprice=0,ft_isend=0 where id={$userid}");
                    	#记录交易钱包复投记录
                    	hyepricejl($userid,-$ft_money,15,5,$curtime,"累计收入达到2倍复投标准,自动复投");

                    }elseif($investmentAmounts > $v['jyprice']){
        	            #第二种情况：持币量（资产钱包+释放钱包）> 交易钱包余额
        				#判断条件（复投轮数）：如果当前是第一轮复投，再判断持币量
                    	#统计该会员在复投备份表(redeliverylog)中的记录
                    	
                    	if($row['c'] == 1){
                    		#若持币量大于1万（自动复投）
                    		if ($investmentAmounts >=10000) {
                                #且 交易钱包不小于1万，则从交易钱包中扣除1万，作为复投
                                if($v['jyprice'] >=10000){
                                    $ft_money = 10000;
                                    $sqlclear =$db->query("update wd_users set jyprice=jyprice-{$ft_money},fh_money=0,ljslprice=0,ft_isend=0 where id={$userid}");
                                    #记录交易钱包复投记录
                                    hyepricejl($userid,-$ft_money,15,5,$curtime,"累计收入达到2倍复投标准,自动复投");
                                }elseif($v['jyprice'] <10000){
                                    #则从两个钱包中扣除1万，优先扣除资产钱包，不足1万的从释放钱包扣除，同时清空累计收入
                                    #资产钱包余额
                                    if($v['price'] >=10000){
                                        $sqlclear =$db->query("update wd_users set price=price-10000,fh_money=0,ljslprice=0,ft_isend=0 where id={$userid}");
                                        #记录资产钱包复投记录
                                        hyepricejl($userid,-10000,6,1,$curtime,"累计收入达到2倍复投标准,自动复投");
                                    }elseif ($v['price'] <10000) {
                                        #计算需要扣释放钱包的金额
                                        $ft_sfprice = 10000-$v['price'];

                                        $sqlclear =$db->query("update wd_users set price=0,sfprice=sfprice-{$ft_sfprice},fh_money=0,ljslprice=0,ft_isend=0 where id={$userid}");
                                        #记录资产钱包复投记录
                                        hyepricejl($userid,-$v['price'],6,1,$curtime,"累计收入达到2倍复投标准,自动复投");
                                        #记录释放钱包复投记录
                                        hyepricejl($userid,-$ft_sfprice,5,2,$curtime,"累计收入达到2倍复投标准,自动复投");
                                    }
                                }
                    			
                    		}elseif ($investmentAmounts <10000) {
                    			#若持币量小于1万，直接清空资产钱包和释放钱包，同时清空累计收入
                    			$sqlclear =$db->query("update wd_users set price=0,sfprice=0,fh_money=0,ljslprice=0 where id={$userid}");
                    			#记录资产钱包复投记录
    		                	hyepricejl($userid,-$v['price'],6,1,$curtime,"累计收入达到2倍复投标准,复投");
    		                	#记录释放钱包复投记录
    		                	hyepricejl($userid,-$v['sfprice'],5,2,$curtime,"累计收入达到2倍复投标准,复投");
                    		}
                    	}elseif ($row['c'] >= 2) {
    		                #若当前复投不是第一轮，则直接清空资产钱包和释放钱包，同时清空累计收入
    		                $sqlclear =$db->query("update wd_users set price=0,sfprice=0,fh_money=0,ljslprice=0 where id={$userid}");
                    		#记录资产钱包复投记录
    	                	hyepricejl($userid,-$v['price'],6,1,$curtime,"累计收入达到2倍复投标准,复投");
    	                	#记录释放钱包复投记录
    	                	hyepricejl($userid,-$v['sfprice'],5,2,$curtime,"累计收入达到2倍复投标准,复投");
                    	}
    	            }
                }
                $db->query('commit');#提交
            }catch(Exception $e){
                $db->query('rollback');#回滚
            }
        }
        $db->free_result();
}

