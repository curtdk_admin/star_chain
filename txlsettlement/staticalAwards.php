<?php 


require_once("../config/dbconn.php");

staticalAwards();
//静态奖
function staticalAwards(){
    global $db,$db_prefix,$curtime,$glo_returnStaticTxl_1,$glo_returnStaticTxl_2,$glo_returnStaticTxl_3,$glo_returnStaticTxl_4,$glo_returnStaticTxl_5,$glo_returnStaticTxl_6;
    //    今天的最后一秒
    $time1 = strtotime(date('Y-m-d 23:59:59'));
    $time2 =  strtotime(date('Y-m-d 00:00:00'));

    //   查询所有可以允许结算静态奖的会员
    $sql = "select id,username,price,sfprice,znprice,fh_money,fh_nexttime from {$db_prefix}users where regtime<{$time2} and fh_nexttime < {$time1}";
    $static_all = $db->get_all($sql);
    foreach ($static_all as $k=>$v){
        $userid= $v['id'];
        $username= $v['username'];

        //计算当前会员所处的等级
        $rank = calculationLevel($v['price'],$v['sfprice']);
        if($rank == false){
            $db->query("update wd_users set dynamic_nexttime={$time1} where username='{$username}'");
            continue;
        }
        //获取当前会员静态奖返现比例
        $glo_returnStaticTxl = 'glo_returnStaticTxl_'.$rank;

        //计算当前会员总资产
        $temporaryPriceall = $v['price'] + $v['sfprice'] +$v['znprice'];

        //今日静态奖收益数量
        $todayStaticEarnings = $temporaryPriceall * ($$glo_returnStaticTxl/100);

        $db->query('start transaction');#开启事物
        try{
            if($todayStaticEarnings>0){
            //将静态奖加回去
            $sqlkk="update {$db_prefix}users set rank= {$rank},znprice=znprice+'".floatval($todayStaticEarnings)."',fh_money = fh_money+'".floatval($todayStaticEarnings)."',priceall = priceall+'".floatval($todayStaticEarnings)."',fh_nexttime =$time1 where id={$userid}";
            file_put_contents('staticalAwards.txt',$sqlkk.PHP_EOL, FILE_APPEND);
            $row =  $db->query($sqlkk);
            if(!empty($row)){
                //记录财务信息
                //智能钱包财务记录
                $e_userid=$userid;$e_price=floatval($todayStaticEarnings);$e_type=1;$e_ptype=3;$e_addtime=$curtime;$e_memo="每日静态奖结算，新增智能钱包金额：".$todayStaticEarnings;
                hyepricejl($e_userid,$e_price,$e_type,$e_ptype,$e_addtime,$e_memo);

                unset($txldata);
                $txldata['userid']=$userid;
                $txldata['username']=$username;
                $txldata['statiPriceall']=$temporaryPriceall;
                $txldata['rank']=$rank;
                $txldata['staticEarnings']=floatval($todayStaticEarnings);
                $txldata['add_time']=$curtime;
                $res2 =$db->insert("{$db_prefix}statiPrice_log",$txldata);
             if(!$res2){
                    throw new Exception('插入失败');
                }
            }
                $db->query('commit');#提交
            }
        }catch(Exception $e){
            $db->query('rollback');#回滚
        }
    }
    $db->free_result();
    
    return true;
}
