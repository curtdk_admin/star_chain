<?
//财务记录
function hyepricejl($userid,$price,$type,$ptype,$addtime,$memo=''){
    global $db,$db_prefix;
    unset($dataArray);
    $dataArray['userid']=$userid;
    $dataArray['price']=$price;
    $dataArray['type']=$type;
    $dataArray['ptype']=$ptype;
    $dataArray['memo']=$memo;
    $dataArray['addtime']=$addtime;
    $db->insert("{$db_prefix}caiwu",$dataArray);
    return true;
}

//计算会员等级
function calculationLevel($price='',$sfprice=''){
    global $hyrankary,$glo_mostmin_1,$glo_mostmin_2,$glo_mostmin_3,$glo_mostmin_4,$glo_mostmin_5,$glo_mostmin_6;
    global $glo_mostmax_1,$glo_mostmax_2,$glo_mostmax_3,$glo_mostmax_4,$glo_mostmax_5;
    $leiji_price = $price+$sfprice;
    if($leiji_price>0){
        foreach ($hyrankary as $k=>$v){
            $glo_mostmin = "glo_mostmin_".$k;
            $glo_mostmax = "glo_mostmax_".$k;
            if($k>1){
                $glo_most = "glo_mostmax_".($k-1);
            }
            if($k == 1){
                if($leiji_price >=$$glo_mostmin && $leiji_price<=$$glo_mostmax){
                    return $k;
                }
            }else if($k>=2 && $k<6){
                if($leiji_price >$$glo_most && $leiji_price<=$$glo_mostmax){
                    return $k;
                }
            }else if($k==6){
                if($leiji_price >$$glo_most){
                    return $k;
                }
            }
        }
    }else{
        return false;
    }
}

/**
 * 发送手机验证码
 */
function mobile_code($mobile = null){
    global $db,$db_prefix;
//    if(empty($username)) returnJson(0,'用户名不能为空');
    if(empty($mobile)) returnJson(0,'手机号码不能为空');
    if(!preg_match('/^1[3456789]{1}\d{9}$/',$mobile)) returnJson(0,'手机号码格式不正确');
//    $sql="select id,islock,username,mobile from {$db_prefix}users where username='".trim($username)."'";
//    $userinfo = $db->get_one($sql);
//    if(!$userinfo) returnJson(0,'用户不存在');
//    if($userinfo['mobile'] != $mobile) returnJson(0,'手机号码不匹配');
    $code = rand(1000,9999);
    // $content = '您的验证码是：【'.$code.'】。请不要把验证码泄露给其他人。如非本人操作，可不用理会！';
    $content = "您的验证码是：{$code}。请不要把验证码泄露给其他人。";
    $now_time = time();
    $expire_time = $now_time+120;
    // $info = $db->get_one("select * from {$db_prefix}mobile_code where mobile = '".$mobile."' and expire_time > {$now_time} order by expire_time desc");
    $info = $db->get_one("select * from {$db_prefix}mobile_code where mobile = '{$mobile}'");
    if(empty($info)){
        $url = TXL_TRADE.'/home/api/sendSMS';
        $res = request_post($url,array('type'=>1,'mobile'=>$mobile,'content'=>$content,'status'=>1));
        $res = json_decode($res);
        if($res->code !=1){
            returnJson(0,$res->msg);
        }
        unset($txldata);
        $txldata['mobile']=$mobile;
        $txldata['code']=$code;
        $txldata['expire_time']=$expire_time;
        $is_ok =$db->insert("{$db_prefix}mobile_code",$txldata);
        if(!$is_ok) returnJson(0,'验证码发送失败');
        returnJson(1,'验证码发送成功');
    }else{
        if($info['expire_time'] > $now_time){

            returnJson(1,'2分钟之内不能重复发送',array('str'=>'nosend'));
        }else{
            $url = TXL_TRADE.'/home/api/sendSMS';
            $res = request_post($url,array('type'=>1,'mobile'=>$mobile,'content'=>$content,'status'=>1));
            $res = json_decode($res);
            if($res->code !=1){
                returnJson(0,$res->msg);
            }
            unset($txldata);
            $txldata['mobile']=$mobile;
            $txldata['code']=$code;
            $txldata['expire_time']=$expire_time;
            $is_ok =$db->update("{$db_prefix}mobile_code",$txldata,'id =' .$info['id']);
            if(!$is_ok) returnJson(0,'验证码发送失败');
            returnJson(1,'验证码发送成功');
        }
        
    }

}


function checkcode($mobile,$code){
    global $db,$db_prefix;
    $info = $db->get_one("select * from {$db_prefix}mobile_code where mobile='".$mobile."' and code='".$code."' order by expire_time desc");
    if(!$info){
        returnJson(0,'验证码不正确',array('str'=>'nosend'));
    }else{
        if($info['expire_time'] < time()){
            returnJson(0,'验证码已失效，请重新获取');
        }
        $db->query("delete from {$db_prefix}mobile_code where id = {$info['id']}");
    }
}

/**
 * 返回json信息
 * @param  integer $code [description]
 * @param  string  $msg  [description]
 * @param  array   $data [description]
 * @return [type]        [description]
 */
function returnJson($code = 0,$msg = '',$data = array()){
    echo json_encode(array("code"=>$code,"msg"=>$msg,"data"=>$data));
    exit();
}

// /**
//  * 检查用户登录
//  * @return [type] [description]
//  */
// function checkLogin($username = null ,$password = null){
//     global $db,$db_prefix;

//     if(empty($username)) returnJson(0,'请输入用户名');
//     if(empty($password)) returnJson(0,'请输入密码');

//     $sql="select id,islock,username,nickname,pwd,regtime,price,gwprice,sfprice,znprice,slprice,jyprice,djprice,mobile,fhstata from {$db_prefix}users where username='".trim($username)."'";
//     $rs=$db->get_one($sql);
//     if (empty($rs)){
//         returnJson(0,'用户名或密码错误');
//     }else{
//         if(authcode($rs['pwd'],"DECODE")!=$password) returnJson(0,'用户名或密码错误');
//         if($rs['islock'] == 1) returnJson(0,'用户已被冻结');
//         unset($rs['pwd']);
//         if(!isset($_SESSION)){
//             session_start();
//         }
//         $_SESSION['user'] = $rs;
//         returnJson(1,'登录成功',$rs);
//     }
// }



/**
 * 修改密码
 * @return [type] [description]
 */
function editPassword($username = null ,$mobile = null,$newpwd = null ,$confirmpwd = null ,$code = null,$type = 1){
    global $db,$db_prefix;
    if(empty($username)) returnJson(0,'请输入用户名');
    if(empty($mobile)) returnJson(0,'手机号码不能为空');
    if(empty($newpwd)) returnJson(0,'新密码不能为空');
    
    if(empty($confirmpwd)) returnJson(0,'确认密码不能为空');
    if($newpwd != $confirmpwd) returnJson(0,'密码不一致');
    if(empty($code)) returnJson(0,'手机验证码不能为空');
    $userinfo = $db->get_one("select * from {$db_prefix}users where username = '{$username}' and mobile = '{$mobile}'");

    if(!$userinfo){
        returnJson(0,'用户与手机号不匹配');
    }
       checkcode($mobile,$code);
    $update_arr = array();
    if($type == 1){
        $newpwd1 = authcode($newpwd,'ENCODE');
        $res = $db->update("{$db_prefix}users",array('pwd'=>$newpwd1),"id = {$userinfo['id']}");
        if($res){
            returnJson(1,'密码修改成功');
        }else{
            returnJson(0,'密码修改失败');
        }

    }else{
        $newpwd1 = authcode($newpwd,'ENCODE');
        $res = $db->update("{$db_prefix}users",array('pwd1'=>$newpwd1),"id = {$userinfo['id']}");
        if($res){
            returnJson(1,'密码修改成功');
        }else{
            returnJson(0,'密码修改失败');
        }
    }
}


/**
 * 获取用户账户资金信息
 * @return [type] [description]
 */
function getAccount($username = null,$type =''){
    global $db,$db_prefix,$glo_sellline_limit;
    if(empty($username)) returnJson(0,'用户信息有误');
    $sql="select id,islock,username,nickname,regtime,price,gwprice,sfprice,znprice,slprice,jyprice,djprice,mobile,fhstata from {$db_prefix}users where username='".trim($username)."'";
    $userinfo = $db->get_one($sql);
    if(!$userinfo) returnJson(0,'用户不存在');
    //该会员左区
    $gl_left = $db->get_one("select id,username from {$db_prefix}users where gluser='".$username."' and pos =1");
    //        //该会员右区
    $gl_right = $db->get_one("select id,username from {$db_prefix}users where gluser='".$username."' and pos =2");
    //左区
    if(!empty($gl_left)){
        $left_id = $gl_left['id'];
        //左区业绩
        $left_yeji = $db->get_one("select sum(price) as c1, sum(sfprice) as c2 from {$db_prefix}users where FIND_IN_SET({$left_id},glstr)or id={$left_id}");
        if(!empty($left_yeji)){
            $leftall_yeji = $left_yeji['c1']+$left_yeji['c2'];
        }
    }else{
        $leftall_yeji = 0;
    }
    //右区
    if(!empty($gl_right)){
        $right_id = $gl_right['id'];
        //右区业绩
        $right_yeji = $db->get_one("select sum(price) as c1, sum(sfprice) as c2 from {$db_prefix}users where FIND_IN_SET({$right_id},glstr)or id={$right_id}");
        if(!empty($right_yeji)){
            $rightall_yeji = $right_yeji['c1']+$right_yeji['c2'];
        }
    }else{
        $rightall_yeji = 0;
    }
    $userinfo['left_yeji']=$leftall_yeji;
    $userinfo['right_yeji']=$rightall_yeji;
    $userinfo['z_yeji']=$leftall_yeji+$rightall_yeji;
    if($type == 'trade_out'){
        //确定小区
        if($leftall_yeji <= $rightall_yeji){
            $smaller_yeji = $leftall_yeji;
        }else if($leftall_yeji > $rightall_yeji){
            $smaller_yeji = $rightall_yeji;
        }
        //小区业绩出售限制
        $sellline_limit = $glo_sellline_limit * 10000;
        if($smaller_yeji >= $sellline_limit){
            returnJson(1,'NO_TRADE',$userinfo);
        }
    }
    returnJson(1,'获取成功',$userinfo);
}
/**
 * 发送post请求
 * @param string $url
 * @param string $param
 * @return bool|mixed
 */
function request_post($url = '', $param = '') {
    if (empty($url) || empty($param)) {
        return false;
    }

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
    //    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $data = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Errno' . curl_error($ch);
    }
    curl_close($ch);
    return $data;
}

//会员注册
/*老注册*/

/***
 * @param $username             [新注册会员编号]
 * @param string $nickname      [会员姓名]
 * @param $tjuser               [推荐人]
 * @param string $gluser        [管理人]
 * @param string $pos           [区位]
 * @param string $mobile        [注册手机号]
 * @param string $code          [手机验证码]
 */

function register($username,$nickname='',$tjuser,$gluser='',$pos='',$mobile='',$code=''){
   // file_put_contents('session_info.txt',var_export($_SESSION,true),FILE_APPEND);
    global $db,$db_prefix,$sysposnum,$curtime;
    //检测用户输入信息的开始
    $pwd="123456";
    $pay_pwd="123456";
    $username = txl_trim($username);
    $tjuser = txl_trim($tjuser);
    $gluser = txl_trim($gluser);
    $pos = intval($pos);
//    returnJson(0,'系统维护中，注册功能暂时关闭');
    if(empty($username))  returnJson(0,'请输入用户名');
    if(empty($nickname)) returnJson(0,'请输入姓名');
    if(empty($tjuser)) returnJson(0,'分享王不能为空');
    if(!in_array($pos, array(1,2))) returnJson(0,'请选择区位');
    if(empty($mobile)) returnJson(0,'注册手机号不能为空');
    if(!preg_match('/^1[3456789]{1}\d{9}$/',$mobile)) returnJson(0,'手机号码格式不正确');
    if(empty($code)) returnJson(0,'验证码不能为空');
    if(empty($gluser)) returnJson(0,'部落王不能为空');
    $gluser = strtoupper($gluser);
    $username = strtoupper($username);
    $tjuser = strtoupper($tjuser);
    //手机验证
    checkcode($mobile,$code);
   if(!isset($_SESSION['user']['id'])) returnJson(0,'用户登录状态失效，请重新登录');
   $nowloginusername = strtoupper($_SESSION['user']['username']);
   $nowloginid = $_SESSION['user']['id'];

   if($gluser != $nowloginusername){
        $new_posuser = $db->get_one("select id,glstr from wd_users where username='".$gluser."'");
        //验证所填写 gluser 区位是否正确
        if(!empty($new_posuser)){
            $nowselectglstr = $new_posuser['glstr'];
            $nowselectglarr = explode(',',$nowselectglstr);
            if(!in_array($nowloginid,$nowselectglarr)) returnJson(0,'当前选择的用户不在本部落'); 
        }
   }
    

    //检测用户名是否可以使用
    $sqlchk="select id from {$db_prefix}users where username='".$username."'";
    $rschk=$db->get_one($sqlchk);
    if(!empty($rschk)) returnJson(0,'用户名已经被占用');

    $sqlchk01="select id from {$db_prefix}users_query where username='".$username."'";
    $rschk01=$db->get_one($sqlchk01);
    if(!empty($rschk01)) returnJson(0,'用户名已经被占用');

    //检测推荐人是否存在

    $sqlchk1="select * from {$db_prefix}users where username='".$tjuser."' and state=1";
    $rschk1=$db->get_one($sqlchk1);
    if (empty($rschk1)) returnJson(0,'分享王不存在');
    $tjuserid=$rschk1['id'];

    //检测管理人是否存在
    $sqlchk2="select id,glstr,gldepth from {$db_prefix}users where username='".$gluser."'";
    $rschk2=$db->get_one($sqlchk2);
    if (empty($rschk2)) returnJson(0,'部落王不存在');

    $gluserid=$rschk2['id'];
    //检测区位
    $sqlchk3="select id from {$db_prefix}users where gluser='".$gluser."' and pos='$pos'";
    $rschk3=$db->get_one($sqlchk3);
    if (!empty($rschk3)) returnJson(0,'区位已经被占用');

    $gldepth=0;$tjdepth=0;$tjstr='';$glstr='';

    $gldepth=$rschk2['gldepth']+1;  //管理层级
    $tjdepth=$rschk1['tjdepth']+1;  //推荐层级
    //推荐关系
    if ($rschk1['tjstr']) {
        $tjstr=$rschk1['tjstr'].",".$rschk1['id'];
    }else {
        $tjstr=$rschk1['id'];
    }
    if($rschk2['glstr']){
        $glstr=$rschk2['glstr'].",".$rschk2['id'];
    }else{
        $glstr=$rschk2['id'];
    }

    //插入到数据库
    unset($dataArray);
    $dataArray['username']=trim($username);
    $dataArray['nickname']=trim($nickname);
    $dataArray['pwd']=authcode($pwd,'ENCODE');
    $dataArray['pwd1']=authcode($pay_pwd,'ENCODE');
    $dataArray['tjuser']=trim($tjuser);
    $dataArray['gluser']=trim($gluser);
    $dataArray['pos']=$pos;
    $dataArray['posnum']=$sysposnum;
    $dataArray['regtime']=$curtime;
    $dataArray['confirmtime']=$curtime;
    $dataArray['mobile']=trim($mobile);
    $dataArray['tjdepth']=$tjdepth;
    $dataArray['gldepth']=$gldepth;
    $dataArray['state']=1;
    $dataArray['glstr']=$glstr;
    $dataArray['tjstr']=$tjstr;
    $dataArray['ft_times']=trim($curtime);
    $dataArray['ft_backnumber']=0;
    $dataArray['ft_startdate']=$curtime;
    $dataArray['ft_isend']=0;
    $dataArray['islock']=0;

    $db->query('start transaction');
    #开启事务
    try{
        $db->insert("{$db_prefix}users",$dataArray);
        $newuserid=$db->insert_id();
        if(!$newuserid){
            throw new Exception("会员注册失败");            
        }   

        if(!empty($glstr)){
            $res = $db->insert("{$db_prefix}glgx",array('userid'=>$newuserid,'glstr'=>$glstr));
            if(!$res){
                throw new Exception("会员注册失败");            
            }
        }
        if(!empty($tjstr)){
            $res = $db->insert("{$db_prefix}tjgx",array('userid'=>$newuserid,'tjstr'=>$tjstr));
            if(!$res){
                throw new Exception("会员注册失败");            
            }
        }
        $db->query('commit');#提交
        returnJson(1,'会员注册成功，请及时修改密码。');
    }catch(Exception $e){
        $db->query('rollback');#回滚
        returnJson(0,$e->getMessage());
    }
}



/**************************************新注册方法****************************************************/

/**
 * 用户注册---用于6-01~至今新注册
 * @param  [type] $username [description]
 * @param  string $nickname [description]
 * @param  [type] $tjuser   [description]
 * @param  string $gluser   [description]
 * @param  string $pos      [description]
 * @param  string $mobile   [description]
 * @param  string $code     [description]
 * @return [type]           [description]
 */
// function register($username,$nickname='',$tjuser,$gluser='',$pos='',$mobile='',$code=''){
//     global $db,$db_prefix,$sysposnum,$curtime;
//     //returnJson(0,'注册功能正在维护,请于12点以后再试');
//     //检测用户输入信息的开始
//     $pwd="123456";
//     $pay_pwd="123456";
//     $username = txl_trim($username);
//     $tjuser = txl_trim($tjuser);
//     $gluser = txl_trim($gluser);
//     $pos = intval($pos);
//     #returnJson(0,'系统维护中，注册功能暂时关闭');
//     if(empty($username))  returnJson(0,'请输入用户名');
//     if(empty($nickname)) returnJson(0,'请输入姓名');
//     if(empty($tjuser)) returnJson(0,'分享王不能为空');
//     if(!in_array($pos, array(1,2))) returnJson(0,'请选择区位');
//     // if(empty($mobile)) returnJson(0,'注册手机号不能为空');
//     // if(empty($code)) returnJson(0,'验证码不能为空');
//     if(empty($gluser)) returnJson(0,'部落王不能为空');
//     $gluser = strtoupper($gluser);
//     $username = strtoupper($username);
//     $tjuser = strtoupper($tjuser);
//     //手机验证
//     // checkcode($mobile,$code);
//    if(!(isset($_SESSION['user']['id']) && $_SESSION['user']['id'] > 0)) returnJson(0,'用户登录状态失效，请重新登录');
//    #1.验证新注册用户所选部落王，是否在当前登录用户的管理网之下
//    $nowloginusername = strtoupper($_SESSION['user']['username']);
//    $nowloginid = $_SESSION['user']['id'];
//    if($gluser != $nowloginusername){
//         $new_posuser = $db->get_one("select id,glstr from wd_users where username='".$gluser."'");
//         //验证所填写 gluser 区位是否正确
//         if(!empty($new_posuser)){
//             $nowselectglstr = $new_posuser['glstr'];
//             $nowselectglarr = explode(',',$nowselectglstr);
//             if(!in_array($nowloginid,$nowselectglarr)) returnJson(0,'当前选择的用户不在本部落'); 
//         }
//    }

//     #2.验证用户名是否已被占用
//     $start_time = strtotime('2018-06-01 00:00:00');
//     $sqlchk="select id,username from {$db_prefix}users where username='".$username."'";
//     $rschk=$db->get_one($sqlchk);
//     if (!empty($rschk)) returnJson(0,'用户名已经被占用');

//     #3.检测用户--查询老users表验证当前用户名是否在6-01~至今的区间内,并且用户数据未被同步过
//     $sql_3="select id,username,is_register,mobile,tjuser from {$db_prefix}users_query where regtime >={$start_time} and username='".$username."'";
//     $sql_3_res = $db->get_one($sql_3);
//     if(empty($sql_3_res)) returnJson(0,'当前注册只为部分用户开放');
//     if($sql_3_res['is_register'] == 2) returnJson(0,'用户已被抢先注册');
//     if($sql_3_res['mobile'] !=$mobile) returnJson(0,'手机号与原账户不匹配');

//     // $new_tjinfo=$db->get_one("select id,tjuser from wd_users_query where username='{$username}'");
//     // if(empty($new_tjinfo)) returnJson(0,'您今天不能注册');

//     // if(strtoupper($new_tjinfo['tjuser']) != $tjuser){
//     //     returnJson(0,"分享王验证失败，您不是该会员的实际推荐人");
//     // }
//     #4.检测推荐人是否存在

//     $sqlchk1="select * from {$db_prefix}users where username='".$tjuser."' and state=1";
//     $rschk1=$db->get_one($sqlchk1);
//     if (empty($rschk1)) returnJson(0,'分享王不存在');
//     $tjuserid=$rschk1['id'];
//     //这是会员推荐的第几个会员
//     // $sqltjs="select count(id) as c from {$db_prefix}users where tjuser='".$tjuser."' and state=1";
//     // $rstjs=$db->get_one($sqltjs);
//     // $tjsnum=intval($rstjs['c'])+2;

//     #5.检测管理人是否存在
//     $sqlchk2="select id,glstr,gldepth from {$db_prefix}users where username='".$gluser."'";
//     $rschk2=$db->get_one($sqlchk2);
//     if (empty($rschk2)) returnJson(0,'部落王不存在');

//     $gluserid=$rschk2['id'];
//     #6.检测区位
//     $sqlchk3="select id from {$db_prefix}users where gluser='".$gluser."' and pos={$pos}";
//     $rschk3=$db->get_one($sqlchk3);
//     if (!empty($rschk3)) returnJson(0,'区位已经被占用');

//     #7.计算新注册管理层深及推荐层深
//     $gldepth=0;$tjdepth=0;$tjstr='';$glstr='';

//     $gldepth=$rschk2['gldepth']+1;  //管理层级
//     $tjdepth=$rschk1['tjdepth']+1;  //推荐层级
//     //推荐关系
//     if ($rschk1['tjstr']) {
//         $tjstr=$rschk1['tjstr'].",".$rschk1['id'];
//     }else {
//         $tjstr=$rschk1['id'];
//     }
//     if($rschk2['glstr']){
//         $glstr=$rschk2['glstr'].",".$rschk2['id'];
//     }else{
//         $glstr=$rschk2['id'];
//     }

    
//     #8.开启事务 ---新注册会员信息验证成功
//     #从老users表 wd_users_query 获取信息 插入到新表wd_users 中 完成后
//     #更新老表is_register字段 表示此用户已经被注册
//     $db->query('start transaction');
//     try{
//         #获取要同步的用户信息 ---限制
//         $sql = "select * from wd_users_query where username = '{$username}'";
//         $new_user = $db->get_one($sql);
//         if(empty($new_user)){
//             throw new Exception('注册失败,用户不在今天的注册列表中');            
//         }
//         $new_user['username'] = $username;
//         $new_user['tjuser'] = $tjuser;
//         $new_user['tjstr'] = $tjstr;
//         $new_user['gluser'] = $gluser;
//         $new_user['glstr'] = $glstr;

//         $new_user['tjdepth']=$tjdepth;
//         $new_user['gldepth']=$gldepth;
        
//         $new_user['pos'] = $pos;
//         $new_user['regtime']=$curtime;
//         $new_user['confirmtime']=$curtime;
//         $new_user['old_userid']=$new_user['id'];
//         #删除标识is_register
//         unset($new_user['is_register']);
//         $db->insert("{$db_prefix}users",$new_user);
//         $newuserid=$db->insert_id();
//         if(!$newuserid){
//             throw new Exception("会员注册失败");            
//         }   

//         if(!empty($glstr)){
//             $res = $db->insert("{$db_prefix}glgx",array('userid'=>$newuserid,'glstr'=>$glstr));
//             if(!$res){
//                 throw new Exception("会员注册失败");            
//             }
//         }
//         if(!empty($tjstr)){
//             $res = $db->insert("{$db_prefix}tjgx",array('userid'=>$newuserid,'tjstr'=>$tjstr));
//             if(!$res){
//                 throw new Exception("会员注册失败");            
//             }
//         }

//         #更新is_register 标识
//         $update_res = $db->query("update wd_users_query set is_register = 2 where id = {$new_user['id']}");
//         if(!$update_res){
//             throw new Exception("会员注册失败");            
//         }
//         // 
//         // //插入到数据库
//         // unset($dataArray);
//         // $dataArray['username']=trim($username);
//         // $dataArray['nickname']=trim($nickname);
//         // $dataArray['pwd']=authcode($pwd,'ENCODE');
//         // $dataArray['pwd1']=authcode($pay_pwd,'ENCODE');
//         // $dataArray['tjuser']=trim($tjuser);
//         // $dataArray['gluser']=trim($gluser);
//         // $dataArray['pos']=$pos;
//         // $dataArray['posnum']=$sysposnum;
//         // $dataArray['regtime']=$curtime;
//         // $dataArray['confirmtime']=$curtime;
//         // $dataArray['mobile']=trim($mobile);
//         // $dataArray['tjdepth']=$tjdepth;
//         // $dataArray['gldepth']=$gldepth;
//         // $dataArray['state']=1;
//         // $dataArray['glstr']=$glstr;
//         // $dataArray['tjstr']=$tjstr;
//         // $dataArray['ft_times']=trim($curtime);
//         // $dataArray['ft_backnumber']=0;
//         // $dataArray['ft_startdate']=$curtime;
//         // $dataArray['ft_isend']=0;
//         // $dataArray['islock']=0;
//         // $db->insert("{$db_prefix}users",$dataArray);
//         // $newuserid=$db->insert_id();
//         // if(!$newuserid){
//         //     throw new Exception("会员注册失败");            
//         // }   

//         // if(!empty($glstr)){
//         //     $res = $db->insert("{$db_prefix}glgx",array('userid'=>$newuserid,'glstr'=>$glstr));
//         //     if(!$res){
//         //         throw new Exception("会员注册失败");            
//         //     }
//         // }
//         // if(!empty($tjstr)){
//         //     $res = $db->insert("{$db_prefix}tjgx",array('userid'=>$newuserid,'tjstr'=>$tjstr));
//         //     if(!$res){
//         //         throw new Exception("会员注册失败");            
//         //     }
//         // }
//         $db->query('commit');#提交
//         returnJson(1,'会员注册成功，请及时修改密码。');
//     }catch(Exception $e){
//         $db->query('rollback');#回滚
//         returnJson(0,$e->getMessage());
//     }
// }


/**
 * 检测登录---用于6-01~至今新注册会员的推荐人登录注册用户
 * @param  [type] $username [description]
 * @param  [type] $password [description]
 * @return [type]           [description]
 */
function checkLogin($username = null ,$mobile = null,$code = null){
    global $db,$db_prefix;
    $username = txl_trim($username);
    $mobile = txl_trim($mobile);
    $code = txl_trim($code);
    if(empty($username)) returnJson(0,'请输入用户名');
    // if(empty($password)) returnJson(0,'请输入密码');
    if(empty($mobile)) returnJson(0,'请输入手机号');
    if(empty($code)) returnJson(0,'请输入短信验证码');

    // $start_time = strtotime('2018-06-01 00:00:00');
    #1.检测当前用户是否存在
    $sql="select id,islock,username,nickname,pwd,regtime,price,gwprice,sfprice,znprice,slprice,jyprice,djprice,mobile,fhstata from {$db_prefix}users where username='".$username."'";
    $rs=$db->get_one($sql);
    if (empty($rs)){
        returnJson(0,'用户名或密码错误');
    }else{
        if($rs['mobile'] != $mobile) returnJson(0,'手机号码与账户不匹配');
        checkcode($mobile,$code);

        // if(authcode($rs['pwd'],"DECODE")!=$password) returnJson(0,'用户名或密码错误');
        if($rs['islock'] == 1) returnJson(0,'用户已被冻结');
        unset($rs['pwd']);
        if(!isset($_SESSION)){
            session_start();
        }
        $_SESSION['user'] = $rs;
        returnJson(1,'登录成功',$rs);
    }
}

/**
 * 去除空格
 * @param  string $str [description]
 * @return [type]      [description]
 */
function txl_trim($str=''){
    $data = '';
    if(empty($str)) return $data;

    $str = str_replace(' ', '', $str);
    $data = str_replace('   ', '', $str);
    return $data;
}


function checkpwd($pwd){
    $length = strlen($pwd);
    if($length<6 || $length >12) returnJson(0,'密码长度应在6~12个字符之间');
    
}
