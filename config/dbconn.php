<?
// error_reporting(E_ALL);
error_reporting(0);
// ini_set('display_errors',1);
ini_set('display_errors',0);
header("Content-type:text/html;charset=utf-8");
import_request_variables('pg');
ini_set('date.timezone','Asia/Shanghai');

session_start(); 
$curtime=time();//设定现在时间$curtime
define("APP_ROOT",dirname(__FILE__));
define("TXL_TRADE",'http://www.hscep.top');
require_once("dbconfig.php");
require_once("mysqlcls.php");
// require_once("xssattack.php");//防攻击
$ip = isset($_SERVER['HTTP_ORIGIN']) ? $_SERVER['HTTP_ORIGIN'] : '';
header("Access-Control-Allow-Origin: ".$ip);
header('Access-Control-Allow-Credentials:true');
$db=new DB;

$db->connect($mysql_host.":".$mysql_port, $mysql_user, $mysql_password, $mysql_dbname,0,'utf8');
require_once("sysset.php");
require_once("pwdcls.php");//加密函数
require_once("getsalaryset.php");//奖金设定
require_once("wangyincls.php");//网银接口
require_once("areacls.php");//获取所在区域
require_once("yjcls.php");//会员业绩累计函数
require_once("ecls.php");//会员财务记录函数
require_once("logcls.php");//日志记录函数
require_once("cartcls.php");//购物车函数
require_once("regcartcls.php");//注册购物车函数
require_once("gpset.php");//股票设定参数

//获取推荐的上级
function gettjupstrfunc($tjuser,$dai,$dai1){
	global $db,$db_prefix,$hytjupstr;
	if ($dai1>=$dai){
		$sql1="select id,username,tjuser from {$db_prefix}users where username='$tjuser'";
		$rs1=$db->get_one($sql1);
		if ($rs1){
			if ($hytjupstr!='') $hytjupstr.=",";$hytjupstr.=$rs1['id'];
			if ($rs1['tjuser']){
				gettjupstrfunc($rs1['tjuser'],$dai+1,$dai1);
			}
		}
	}
}

function gettjupstringfunc($tjuser){
	global $db,$db_prefix,$hytjupstring;
	$sql1="select id,username,tjuser from {$db_prefix}users where username='$tjuser'";
	$rs1=$db->get_one($sql1);
	if ($rs1){
		if ($hytjupstring!='') $hytjupstring.=",";$hytjupstring.=$rs1['id'];
		if ($rs1['tjuser']){
			gettjupstringfunc($rs1['tjuser']);
		}
	}
}
//获取管理的上级
function getglupstrfunc($gluser,$ceng,$ceng1){
	global $db,$db_prefix,$hyglupstr;
	if ($ceng1>=$ceng){
		$sql1="select id,username,gluser from {$db_prefix}users where username='$gluser'";
		$rs1=$db->get_one($sql1);
		if ($rs1){
			if ($hyglupstr!='') $hyglupstr.=",";$hyglupstr.=$rs1['id'];
			if ($rs1['gluser']){
				getglupstrfunc($rs1['gluser'],$ceng+1,$ceng1);
			}
		}
	}
}

function getglupstringfunc($gluser){
	global $db,$db_prefix,$hyglupstring;
	$sql1="select id,username,gluser from {$db_prefix}users where username='$gluser'";
	$rs1=$db->get_one($sql1);
	if ($rs1){
		if ($hyglupstring!='') $hyglupstring.=",";$hyglupstring.=$rs1['id'];
		if ($rs1['gluser']){
			getglupstringfunc($rs1['gluser']);
		}
	}
}
//最短距离叶子，左区优先
function getglmin($gluser,$ceng){
	global $db,$db_prefix,$glmin,$glminpos,$glminceng;
	$sql1="select id,username,gluser from {$db_prefix}users where gluser='$gluser' and pos=1";
	$rs1=$db->get_one($sql1);
	if ($rs1){
			
			getglmin($rs1['username'],$ceng+1);
	
	}else{
		if($minceng>$ceng){
			$glmin=$gluser;$glminpos=1;$glminceng=$ceng;
		}
	}
	$sql1="select id,username,gluser from {$db_prefix}users where gluser='$gluser' and pos=2";
	$rs1=$db->get_one($sql1);
	if ($rs1){
			getglmin($rs1['username'],$ceng+1);

	}else{
		if($minceng>$ceng){
			$glmin=$gluser;$glminpos=2;$glminceng=$ceng;
		}
	}
}

function dump($data){
    echo "<pre/>";
    print_r($data);
    exit();
}

//不同环境下获取真实的IP
function get_ip(){
    //判断服务器是否允许$_SERVER
    if(isset($_SERVER)){    
        if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $realip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }elseif(isset($_SERVER['HTTP_CLIENT_IP'])) {
            $realip = $_SERVER['HTTP_CLIENT_IP'];
        }else{
            $realip = $_SERVER['REMOTE_ADDR'];
        }
    }else{
        //不允许就使用getenv获取  
        if(getenv("HTTP_X_FORWARDED_FOR")){
              $realip = getenv( "HTTP_X_FORWARDED_FOR");
        }elseif(getenv("HTTP_CLIENT_IP")) {
              $realip = getenv("HTTP_CLIENT_IP");
        }else{
              $realip = getenv("REMOTE_ADDR");
        }
    }

    return $realip;
}

