<?
class pagecls{
	var $pageno;
	var $pagesize;
	var $pagenum;
	var $pastnum;
	var $recnum;
	var $url;
	
	function pagecls($pagesize,$recnum){
		if (!$_REQUEST['pageno']) $this->pageno=1;else $this->pageno=$_REQUEST['pageno'];
		if ($this->pageno<1) $this->pageno=1;
		$this->pagesize=$pagesize;
		$this->recnum=$recnum;
		$this->pagenum=ceil($this->recnum/$this->pagesize);
		if ($this->pageno>$this->pagenum) $this->pageno=$this->pagenum;
		$this->pastnum=($this->pageno-1)*$this->pagesize;
		if ($this->pastnum<0) $this->pastnum=0;
		$this->url='';
		foreach($_REQUEST as $key=>$value){
			if ($key!='pageno'){
				if ($this->url!='') $this->url.='&';
				$this->url.=$key."=".$value;
			}
		}
		if ($this->url!='') $this->url.='&';
		$this->url.="pageno=";
	}
}
?>