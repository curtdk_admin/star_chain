<?php 
	require_once("dbconn.php");

	//获取昨天第一秒时间
	 $time = strtotime(date('Y-m-d 00:00:00'))-86400;
    //获取今天第一秒时间
	 $time1 = strtotime(date('Y-m-d 00:00:00'));

	//统计出昨日交易会员总数
	$sql = "select count(userid) as num,userid from wd_caiwu where addtime>{$time} and ptype=5 and type=6 and price>1050 GROUP BY userid ORDER BY num desc";
	$info =$db->get_all($sql);
	foreach ($info as $key => $value) {
	    //查询今日是否有该会员交易税返现记录
        $sqlquery = $db->get_one("select * from wd_caiwu where userid={$value['userid']} and addtime>{$time1} and ptype=5 and type=12");
        if(!empty($sqlquery)){
            continue;
        }
		//统计该会员今日买入交易钱包的TXL数量
		$sumsql = "SELECT sum(price) as c from wd_caiwu where userid={$value['userid']} and addtime>{$time} and ptype=5 and type=6 and price>1050";
		$rowsum =$db->get_one($sumsql);

		if($rowsum['c'] <$glo_restitutionCommission){
			continue;
		}

		//计算该会员今日交易扣的钱
		$trade_tax = $rowsum['c'] *0.05/0.95;
		//开启事务
		$db->query('start transaction');#开启事物
        try{
			//将这笔钱加入到该会员的交易钱包中
			$rs1 = $db->query("update wd_users set jyprice=jyprice +{$trade_tax},ljjyprice = ljjyprice+{$trade_tax} where id={$value['userid']}");
			if(!$rs1){
                throw new Exception('资金更新失败');
            }
            //记录财务日志
            $e_userid=$value['userid'];$e_price=floatval($trade_tax);$e_type=12;$e_ptype=5;$e_addtime=$curtime;$e_memo="买入总金额：".$rowsum['c'].",返还交易税：".$trade_tax;
            hyepricejl($e_userid,$e_price,$e_type,$e_ptype,$e_addtime,$e_memo);
            //从txl0001账户中扣除这部分钱
            $rs2 = $db->query("update wd_users set jyprice=jyprice -{$trade_tax},ljjyprice = ljjyprice-{$trade_tax} where username='TXL0001'");
			if(!$rs2){
                throw new Exception('资金更新失败');
            }
              $e_userid=2235;$e_price=-$trade_tax;$e_type=12;$e_ptype=5;$e_addtime=$curtime;$e_memo="会员编号:".$value['userid'].",返还交易税：".$trade_tax;
            hyepricejl($e_userid,$e_price,$e_type,$e_ptype,$e_addtime,$e_memo);

            //
      
		 	$db->query('commit');#提交
        }catch(Exception $e){
            $db->query('rollback');#回滚
        }	
		var_dump($value,$rowsum,$trade_tax);
		echo "<hr/>";
	}
