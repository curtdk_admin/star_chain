<?php
/*
 * 会员注册校验
*/
require_once("../config/dbconn.php");

$act = isset($_POST['act']) ? $_POST['act'] : '';#方法
/*用户名校验*/
if ($act=='chkusername'){
    $username=isset($_POST['username']) ? $_POST['username'] : '';
    if(empty($username)) returnJson(0,'注册的用户不能为空');
    $sqlc="select id from {$db_prefix}users where username='$username'";
    $rsc=$db->get_one($sqlc);
    if ($rsc['id']){
        returnJson(0,'注册的用户已存在');
    }else{
        returnJson(1,'用户名可以使用');
    }
}
/*推荐人校验*/
if ($act=='chktjuser'){
    $tjuser = isset($_POST['tjuser']) ? $_POST['tjuser'] : '';
    if(empty($tjuser)) returnJson(0,'推荐人不能为空');
    $sqlc="select id,nickname from {$db_prefix}users where username='$tjuser'";
    $rsc=$db->get_one($sqlc);
    if ($rsc['id']){
        returnJson(1,'推荐人可以使用',$rsc['nickname']);
    }else{
        returnJson(0,'推荐人不存在');
    }
}
/*管理人校验*/
if ($act=='chkgluser'){
    $gluser = isset($_POST['gluser']) ? $_POST['gluser'] : '';
    $tjuser = isset($_POST['tjuser']) ? $_POST['tjuser'] : '';
    if(empty($tjuser)) returnJson(0,'请先选择推荐人');
    if(empty($gluser)) returnJson(0,'管理人不能为空');
    $sqlgl="select id,nickname,glstr from {$db_prefix}users where username='$gluser'";
    $rsc=$db->get_one($sqlgl);
    if ($rsc['id']){
    $sqltj="select id,nickname,glstr from {$db_prefix}users where username='$tjuser'";
        $rstj = $db->get_one($sqltj);
        $nowloginid = $rstj['id'];
        if($nowloginid != $rsc['id']){
            $nowselectglstr = $rsc['glstr'];
            $nowselectglarr = explode(',',$nowselectglstr);
            if(!in_array($nowloginid,$nowselectglarr)) returnJson(0,'当前选择的用户不在本部落');
            $glusernickname = $rsc['nickname'];
        }else{
            $glusernickname = $rstj['nickname'];
        }
        returnJson(1,'管理人可以使用',$glusernickname);
    }else{
        returnJson(0,'管理人不存在');
    }
}
/*区位校验*/
if ($act=='chkpos'){
    $gluser = isset($_POST['gluser']) ? $_POST['gluser'] : '';
    $tjuser = isset($_POST['tjuser']) ? $_POST['tjuser'] : '';
    if(empty($tjuser)) returnJson(0,'请先选择推荐人');
    if(empty($gluser)) returnJson(0,'管理人不能为空');
    $pos = isset($_POST['pos']) ? $_POST['pos'] : '';
    if(empty($pos)) returnJson(0,'请先选择区位');
    $return=0;
    $sqlc="select id from {$db_prefix}users where username='$gluser'";
    $rsc=$db->get_one($sqlc);
    if ($rsc['id']){
        $sqlp="select id from {$db_prefix}users where gluser='$gluser' and pos='$pos'";
        $rsp=$db->get_one($sqlp);
        if (!$rsp['id']){
            returnJson(1,'区位可以使用');
        }else{
            returnJson(0,'当前位置不可用');
        }
    }
}
