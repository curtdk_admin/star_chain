<?php
require_once("../config/dbconn.php");

/** 检测今天未结算动态奖金的会员 **/
// dtj();
function dtj(){
    global $db,$db_prefix;
    $dtj_insert_sql = '';
    $user_list = '';
    $zc_user_list = '';
    #1.获取6.24日结算动态奖的用户列表
    $sql = "select * from wd_caiwu where type = 1 and ptype =4 and addtime >= 1529769600 and addtime <1529856000";
    $lists = $db->get_all($sql);

    #2.循环列表，检测用户于6.25日没有动态奖的会员并记录信息与上一次动态奖结算金额
    foreach ($lists as $k => $v) {
        #
        $caiwu_sql = "select * from wd_caiwu where type = 1 and ptype =4 and addtime >= 1529856000 and addtime <1529942400 and userid = {$v['userid']}";
        $caiwu_res = $db->get_one($caiwu_sql);
        $user_sql = "select id,islock,username,jyprice,slprice from wd_users where id = {$v['userid']}";
        $user = $db->get_one($user_sql);
        if(!$caiwu_res){
            $dtj_insert_sql .= "insert into wd_caiwu (userid,price,type,ptype,memo,addtime,isff,isok) values({$v['userid']},{$v['price']},1,4,'每日动态奖结算，新增算力钱包金额:'.{$v['price']},1529857800,0,0);".PHP_EOL;
            #记录用户信息
            if($user){
                $user_list .= "6月25日未结算动态奖金用户:{$user['username']},用户id:{$user['id']},昨日结算动态奖金额：{$v['price']};".PHP_EOL.PHP_EOL;
            }
        }else{
            $zc_user_list .= "正常结算的用户:{$user['username']},用户id:{$user['id']},24日动态奖结算金额:{$v['price']},25日动态奖结算金额:{$caiwu_res['price']};".PHP_EOL.PHP_EOL;
        }
    }

    file_put_contents('./dtj_insert_sql.sql', $dtj_insert_sql);
    file_put_contents('./user_list.log', $user_list);
    file_put_contents('./zc_user_list.log', $zc_user_list);
}

check_fxj();
/**
 * 检测分享奖有差异的会员
 * @return [type] [description]
 */
function check_fxj(){
    global $db,$db_prefix;
    $fxj_error_list = '';
    $no_fxj_list = '';
    $fxj_ok_list = '';
    $ids_str = '';
    #1.获取6.24日有分享奖会员的财务记录条数
    $sql = "SELECT  count(*) as num , userid FROM wd_caiwu WHERE  type = 5 AND ptype = 4 AND addtime >= 1529769600 AND addtime < 1529856000 GROUP BY  userid ORDER BY num DESC";
    $lists = $db->get_all($sql);
    #2.循环遍历每个用户的分享奖结算记录，6.25日分享奖会员财务记录条数
    foreach ($lists as $k => $v) {
        
        $sql = "SELECT  count(*) as num FROM   wd_caiwu WHERE  type = 5 AND ptype = 4 AND addtime >= 1529856000 AND addtime < 1529942400 and userid = {$v['userid']}";
        $res = $db->get_one($sql);
        #3.比对当前用户25号分享奖条数与24号是否一致，或者说大于等于24号
        $user_sql = "select id,islock,username,jyprice,slprice from wd_users where id = {$v['userid']}";
        $user = $db->get_one($user_sql);
        if($res && $res['num'] > 0 ){
            if($v['num'] <= $res['num']){
                #正常
                $fxj_ok_list .= "6月25日分享奖正常的用户:{$user['username']},用户id:{$user['id']},昨日分享奖总条数：{$v['num']},今日分享奖总条数:{$res['num']};".PHP_EOL;
            }else{
                #记录分享奖，有问题的会员
                $fxj_error_list .= "6月25日分享奖有问题的用户:{$user['username']},用户id:{$user['id']},昨日分享奖总条数：{$v['num']},今日分享奖总条数:{$res['num']};".PHP_EOL;
                $ids_str .= $user['id'].',';
            }
        }else{
            #记录25号没有分享奖的会员
            $no_fxj_list .= "6月25日没有分享奖的用户:{$user['username']},用户id:{$user['id']},昨日分享奖总条数：{$v['num']};".PHP_EOL;
            $ids_str .= $user['id'].',';
        } 
    }


    file_put_contents('./fxj_ok_list002.log', $fxj_ok_list);
    file_put_contents('./fxj_error_list002.log', $fxj_error_list);
    file_put_contents('./no_fxj_list002.log', $no_fxj_list);
    file_put_contents('./ids002.log', $ids_str);
}

// fxj();

function fxj(){
    global $db,$db_prefix;
    $curtime = strtotime('2018-06-25 21:00:00');
    echo 'fxj';
    #1.获取分享奖有遗漏的用户列表
    // $sql = "SELECT  id,username,tjuser from wd_users where find_in_set (id,'11134,10187,4211,1655,5658,7201,1325,16334,2482,6075,5681,3805,10164,11843,13165,442,16331,4243,11732,3948,3497,9226,10337,14139,15727,16480,669,11634,1395,1501,13956,3825,9135,5817,8293,562,14675,1416,8840,16422,10680,2311,7542,9048,7852,8905,12447,11359,9540,10682,13355,10482,13019,15076,5653,11992,12852,14898,9217,8125,13158,15208,14095,11125,12895,10207,747,8377,1575,12199,5923,9842,1753,7901,15847,2051,9697,11186,2662,10394,13123,2176,384,3242,2641,4735,9468,10215,7445,12742,6857,6842,12471,558,16240,12818,15170,9123,14528,7514,1862,5607,10248,6891,5492,7929,3345,3876,4828,10466,16572,16136,12064,15954,4447,7867,5200,3607,6288,4520,1932,10197,2869,9407,5415,2035,4225,5404,4149,7146,10358,13508,1868,2477,3652,15066,10379,2629,9221,13694,14464,4020,8658,10398,12553,12798,6596,12720,8242,4247,7982,2095,3745,6336,1876,15352,4931,3845,3911,2979,3202,3573,2740,12165,12901,15767,2452,10455,9375,2564,12005,9688,9547,7685,3304,9346,10445,15995,15559,2350,1687,10126,5130,15515,5393')";
    // $sql = "SELECT  id,username,tjuser from wd_users where find_in_set (id,'11134,11634,7852,10682,13355,15076,14095,7445,6857,12818,6891,3607,6288,9567,14464,12553,8242,2740,9688,3976,1841,18,11458,2350,13828,15004,16823,15045,10235,8491,14358,260,787,16974,12512,7520,11610')";
    $sql = "SELECT  id,username,tjuser from wd_users where find_in_set (id,'11134,11634,7852,10682,13355,15076,14095,7445,6857,12818,6891,3607,6288,9567,14464,12553,8242,2740,9688,1841,2350,260')";
    // $sql = "SELECT  id,username,tjuser from wd_users where find_in_set (id,'9567')";
    $user_lists = $db->get_all($sql);

    #2.遍历列表计算每个会员，应得的分享奖
    $yiji_insert_sql = '';
    $erji_insert_sql = '';
    $update_sql = "";
    $query_yiji_sql = '';#一级推荐人
    $query_yijidt_sql = '';#一级动态奖
    $query_yijifxj_sql = '';#一级分享奖
    $query_erji_sql = '';#二级推荐人
    $query_erjidt_sql = '';#二级动态奖
    $query_erjifxj_sql = '';#二级分享奖
    foreach ($user_lists as $k => $v) {
        echo $k;
        #查询当前会员发展的一级推荐人
        $one_tjusers_sql = "SELECT  id,username,tjuser from wd_users where tjuser = '{$v["username"]}'";
        $query_yiji_sql .= $one_tjusers_sql.';'.PHP_EOL;

        $one_tjusers_res = $db->get_all($one_tjusers_sql);
        foreach ($one_tjusers_res as $k1 => $v1) {
            #查看当前会员发展的一级推荐人 查看25号是否有动态奖记录
            $dtj_sql = "select c.id,c.userid,c.price,c.memo,c.addtime,u.username from wd_caiwu as c inner join wd_users  as u on u.id=c.userid  where c.type = 1 and c.ptype =4 and c.addtime >= 1529856000 and c.addtime <1529942400 and c.userid = {$v1['id']}";
            $query_yijidt_sql .= $dtj_sql.';'.PHP_EOL;
            $dtj_res = $db->get_one($dtj_sql);
            if($dtj_res){
                #获取会员当天分享奖记录 --- 一级
                $user_fxj_sql = "SELECT  c.id,c.userid,c.price,c.memo,c.addtime,u.username FROM   wd_caiwu as c inner join wd_users as u on u.id = c.userid WHERE  c.type = 5 AND c.ptype = 4 AND c.addtime >= 1529856000 AND c.addtime < 1529942400 and c.userid = {$v['id']} and c.memo like '%1级%' and c.memo like '%".$dtj_res["username"]."%'";
                $query_yijifxj_sql .= $user_fxj_sql.';'.PHP_EOL;
                $fxj_res = $db->get_one($user_fxj_sql);
                if(!$fxj_res){
                    $price = $dtj_res['price']*0.01;
                    $yiji_insert_sql .= "INSERT into wd_caiwu (userid,price,type,ptype,memo,addtime) values({$v['id']},{$price},5,4,'算力分享奖（1级），新增算力钱包金额：{$price}，奖金来自：".$dtj_res['username']."',{$curtime});".PHP_EOL;
                    $update_sql .= "update wd_users set slprice = slprice+{$price} where id = {$v['id']};".PHP_EOL;
                }
                
            }

            #查询当前会员发展的二级推荐人
            $two_tjusers_sql = "SELECT  id,username,tjuser from wd_users where tjuser = '{$v1["username"]}'";
            $query_erji_sql .= $two_tjusers_sql.';'.PHP_EOL;
            $two_tjusers_res = $db->get_all($two_tjusers_sql);
            foreach ($two_tjusers_res as $k2 => $v2) {
                #查看当前会员发展的一级推荐人 查看25号是否有动态奖记录
                $erji_dtj_sql = "select c.id,c.userid,c.price,c.memo,c.addtime,u.username from wd_caiwu as c inner join wd_users  as u on u.id=c.userid  where c.type = 1 and c.ptype =4 and c.addtime >= 1529856000 and c.addtime <1529942400 and c.userid = {$v2['id']}";
                $query_erjidt_sql .= $erji_dtj_sql.';'.PHP_EOL;
                $erji_dtj_res = $db->get_one($erji_dtj_sql);
                if($erji_dtj_res){
                    #获取会员当天分享奖记录 --- 二级
                    $erji_fxj_sql = "SELECT  c.id,c.userid,c.price,c.memo,c.addtime,u.username FROM   wd_caiwu as c inner join wd_users as u on u.id = c.userid WHERE  c.type = 5 AND c.ptype = 4 AND c.addtime >= 1529856000 AND c.addtime < 1529942400 and c.userid = {$v['id']} and c.memo like '%2级%' and c.memo like '%".$erji_dtj_res["username"]."%'";
                    $query_yijifxj_sql .= $erji_fxj_sql.';'.PHP_EOL;
                    $erji_fxj_res = $db->get_one($erji_fxj_sql);
                    if(!$erji_fxj_res){
                        $erji_price = $erji_dtj_res['price']*0.02;
                        $erji_insert_sql .= "INSERT into wd_caiwu (userid,price,type,ptype,memo,addtime) values({$v['id']},{$erji_price},5,4,'算力分享奖（2级），新增算力钱包金额：{$erji_price}，奖金来自：".$erji_dtj_res['username']."',{$curtime});".PHP_EOL;
                        $update_sql .= "update wd_users set slprice = slprice+{$erji_price} where id = {$v['id']};".PHP_EOL;
                    }
                }
            }
            
        }
    }

    file_put_contents('./yiji_insert_sql001.sql', $yiji_insert_sql);
    file_put_contents('./erji_insert_sql001.sql', $erji_insert_sql);
    file_put_contents('./update_sql.sql001', $update_sql);
    // 
    // file_put_contents('./query_yiji_sql.sql', $query_yiji_sql);
    // file_put_contents('./query_yijidt_sql.sql', $query_yijidt_sql);
    // file_put_contents('./query_yijifxj_sql.sql', $query_yijifxj_sql);

    // file_put_contents('./query_erji_sql.sql', $query_erji_sql);
    // file_put_contents('./query_erjidt_sql.sql', $query_erjidt_sql);
    // file_put_contents('./query_erjifxj_sql.sql', $query_erjifxj_sql);
}