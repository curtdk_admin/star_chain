<?php 
require_once("../config/dbconn.php");
//找到从6月1号零点到6月3号下午5点之前注册的会员
$sql = "select id,username,tjuser,glstr,pos from wd_users where  regtime > 1527782400 AND regtime < 1528016400 and isqian=0 order by regtime asc limit 0,300";
// echo "遍历所有会员：".$sql."<br/>";
$res = $db->get_all($sql);
// dump($res);
foreach($res as $k=>$v){
    //新注册会员id
    $new_id = $v['id'];
    //新注册会员名
    $new_username = $v['username'];
    //新注册会员推荐人名称
    $tjuser = $v['tjuser'];
    //新注册会员区位
    $pos = $v['pos'];
    //新注册会员的glstr
    $new_glstr = $v['glstr'];

    //找到实际管理人
     echo "<hr>新注册会员名".$new_username.",新注册会员推荐名:".$tjuser.",新注册会员区位：".$pos;
    // die;
    $real_gluserinfo = tjstr($new_username,$tjuser,$pos);
    $tjuserinfo = $db->get_one("select id from wd_users where username='{$tjuser}'");
    if($real_gluserinfo['id'] != $tjuserinfo['id']){
        $pos = 1;
    }
    echo "新注册会员名".$new_username.",真实的管理人：".$real_gluserinfo['username']."<br/>";
   
    //更新会员管理关系
    executionUpdates($new_username,$real_gluserinfo,$new_id,$pos);
    echo $k."所有会员信息更新成功<br/>";
}

/**
 * [tjstr 找到实际管理人]
 * @param  string $username  [description]
 * @param  string $tjuser    [description]
 * @param  string $pos       [description]
 * @return [type]            实际管理人会员信息
 */
function tjstr($username='',$tjuser='',$pos=''){
    global $db;
    //根据区位和推荐人找到该会员在这个时间内所对应的区位 注册时间限制在6月1日0点之前
    $sql = "select id,username,glstr,gldepth,pos from wd_users where gluser='{$tjuser}' and pos={$pos} and (regtime<1527782400 or isqian=1)";
    echo "对应区位负责人：".$sql."<br/>";
    $pos_userinfo = $db->get_one($sql);
    // var_dump($pos_userinfo);
    // echo "_________<br/>";
    if(empty($pos_userinfo)){
        $gluserinfo = $db->get_one("select id,username,glstr,gldepth,pos from wd_users where username='".$tjuser."'");
        $real_gluserinfo= $gluserinfo;
    }else{
        //查询获得实际管理人的区位信息、管理网信息
        $real_gluserinfo =recursion($pos_userinfo);
    }
    return $real_gluserinfo;
   
}

/**
 * [recursion 递归查找左区会员]
 * @param  [type] $pos_userinfo [description]
 * @return [type]               管理人所有信息
 */
function recursion($pos_userinfo){
    global $db;
    $sql = "select id,username,glstr,gldepth,pos from wd_users where gluser='".$pos_userinfo['username']."' and pos=1 and (regtime<1527782400 or isqian=1)";
    // echo"递归查找左区会员：".$sql."<br/>";
    $userinfo =$db->get_one($sql);
    if(!empty($userinfo)){
       return recursion($userinfo);
    }else{
        return $pos_userinfo;
    }
}

//确定执行 新注册会员，实际推荐人，管理关系
function executionUpdates($username='',$real_gluserinfo='',$new_id='',$pos=''){
    global $db;
    echo "新注册会员".$username."注册区位:".$pos."<br/>";
    // dump($real_gluserinfo);
    $new_gluser = $real_gluserinfo['username'];
    $new_glstr = $real_gluserinfo['glstr'].",".$real_gluserinfo['id'];
    $new_gldepth = $real_gluserinfo['gldepth']+1;

    #检测区位是否已被占用
    $is_ok = $db->get_one("select id,username,isqian from wd_users where gluser = '{$new_gluser}' and pos ={$pos}");
    if(!empty($is_ok)){
        echo "区位被占用,占用人：".$is_ok['username']."<br/>";
        //检测现在已经占位的会员是不是自己
        if($is_ok['username'] == $username){
            $db->query("update wd_users set isqian=1 where username='{$username}'");
            return;
        }
        //如果是已处理会员占位
        if($is_ok['isqian'] == 1){

        echo "已处理会员占位,该会员帐号为:".$is_ok['username']."<br/>";  
        }
        file_put_contents('./abnormalMember.txt', "被占位新注册会员:".$username."实际推荐人:".$new_gluser.PHP_EOL,FILE_APPEND);
    }
    //先更新自己的管理网信息
    $db->query("update wd_users set gluser='".$new_gluser."',glstr='{$new_glstr}',gldepth='{$new_gldepth}',pos={$pos},isqian=1 where username='{$username}'");
    #更新管理关系数据
    $db->query("update wd_glgx set glstr='{$new_glstr}' where userid ={$new_id}");
    // echo "update wd_users set gluser='".$new_gluser."',glstr='{$new_glstr}',gldepth={$new_gldepth},isqian=1 where username='{$username}'".'<br />';
    file_put_contents('./changeGluser.sql', "update wd_users set gluser='".$new_gluser."',glstr='{$new_glstr}',pos=1,gldepth={$new_gldepth},isqian=1 where username='{$username}';".PHP_EOL,FILE_APPEND);
    //再更新下级所有人的管理网信息
    $old=$db->get_all("select id,username,gluser from wd_users where find_in_set($new_id,glstr) order by gldepth asc");
    
    foreach ($old as $key => $value) {
        //下级会员
        $next_username = $value['username'];
        echo "下级会员：".$next_username."<br/>";
        //下级会员的管理人 网络信息
        $next_gluserinfo =$db->get_one("select id,glstr,gldepth from wd_users where username='{$value['gluser']}'");
        $now_glstr = $next_gluserinfo['glstr'].",".$next_gluserinfo['id'];
        $now_gldepth = $next_gluserinfo['gldepth']+1;
        $now_pos = $next_gluserinfo['pos'];


        $db->query("update wd_users set glstr='{$now_glstr}',gldepth='{$now_gldepth}' where username='{$next_username}'");
        #更新管理关系数据
        $db->query("update wd_glgx set glstr='{$now_glstr}' where userid ={$value['id']}");
        // echo "update wd_users set glstr='{$now_glstr}',gldepth={$now_gldepth} where username='{$next_username}'".'<br />';
        file_put_contents('./changeGluserNext.sql', "update wd_users set glstr='{$now_glstr}',gldepth={$now_gldepth} where username='{$next_username}';".PHP_EOL,FILE_APPEND);
    }
}