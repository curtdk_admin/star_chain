<?
require_once("../config/dbconn.php");require_once("../config/powercls.php");require_once("../config/pagecls.php");

if($action=='del'){
	$msg='';
	$sql="select * from {$db_prefix}salary where id='$id'";
	$rs=$db->get_one($sql);
	if ($rs['state']==2){
		$msg.="您已经发放本期奖金\\n\\n不能删除";
	}
	
	if ($msg!=''){
		echo "<script>alert('$msg');history.back();</script>";exit();
	}
	$periods=$rs['periods'];
	
	
	$db->delete("{$db_prefix}salary","id='$id'");
	$db->delete("{$db_prefix}salary1","periods='$periods'");
	
	echo "<script>location.href='salary_lst.php?pageno={$pageno}';</script>";exit();
}
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	
}
td{
font-size:12px;
}
-->
</style>

<link href="images/skin.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" height="29" valign="top" background="images/mail_leftbg.gif"><img src="images/left-top-right.gif" width="17" height="29" /></td>
    <td width="935" height="29" valign="top" background="images/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">奖金结算列表</div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="images/mail_rightbg.gif"><img src="images/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td height="71" valign="middle" background="images/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9"><table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td height="13" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="nowtable">
                <tr>
                  <td class="left_bt2">&nbsp;</td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td height="30"><table width="100%" height="60" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td height="30" align="center" bgcolor="#f2f2f2" >期数</td>
                  <td align="center" bgcolor="#f2f2f2" >结算日期</td>
                  <td align="center" bgcolor="#f2f2f2" >状态</td>
                  <td align="center" bgcolor="#f2f2f2" >添加时间</td>
                  <td align="center" bgcolor="#f2f2f2" >发放时间</td>
                  <td height="30" align="center" bgcolor="#f2f2f2" >操作</td>
                  </tr>
  <?
  $query='';
  if ($action=='query'){
  	
  }
  $sql="select count(id) as c  from {$db_prefix}salary where 1";
  if ($query!='') $sql.=$query;
  $rs=$db->get_one($sql);
  $page=new pagecls(20,intval($rs['c']));
  $sql="select * from {$db_prefix}salary where 1";
  if ($query!='') $sql.=$query;
  $sql.=" order by id desc limit {$page->pastnum},{$page->pagesize}";
  $result=$db->query($sql);
  while($rs=$db->fetch_array($result)){
  ?>
                <tr>
                  <td align="center"><?=$rs['periods']?></td>
                  <td align="center"><?=$rs['beginday'].'-'.$rs['endday']?></td>
                  <td align="center" ><? if ($rs['state']==0) echo "未发放";else echo "已发放";?></td>
                  <td align="center" ><?=date("Y-m-d H:i:s",$rs['addtime'])?></td>
                  <td align="center" ><? if ($rs['fftime']) echo date("Y-m-d H:i:s",$rs['fftime'])?></td>
                  <td height="30" align="center" >
				   <?
				   if ($rs['state']==0){
				  ?>
				 <a href="salary_do.php?periods=<?=$rs['periods']?>?>" onClick="return confirm('确定要结算本期奖金吗')">结算</a>
				  <?
				  }
				  ?>
				  <a href="salary_rec.php?periods=<?=$rs['periods']?>&pageno=<?=$page->pageno?>">奖金列表</a>
				  <?
				  if ($rs['state']==1){
				  ?>
				 <a href="salary_ff.php?periods=<?=$rs['periods']?>&pageno=<?=$page->pageno?>" onClick="return confirm('确定要发放本期奖金吗?\r\r一旦发放会员的奖金币会自动增加')">发放</a>
				  <?
				  }
				  ?>
				  |<a href="?action=del&id=<?=$rs['id']?>&pageno=<?=$page->pageno?>">删除</a></td>
                  </tr>
	<?
  }
  $db->free_result($result);
  ?>
              </table></td>
            </tr>
            
            <tr>
              <td height="30">&nbsp;</td>
            </tr>
            <tr>
              <td height="30" align="right">每页<?=$page->pagesize?>条 第<?=$page->pageno?>/<?=$page->pagenum?>页 共<?=$page->recnum?>条 <a href='?<?=$page->url?>1'><img src="image1/first.gif" border="0"></a>   <a href='?<?=$page->url.($page->pageno-1)?>'><img src="image1/back.gif" border="0"></a>  <a href='?<?=$page->url.($page->pageno+1)?>'><img src="image1/next.gif" border="0"></a>   <a href='?<?=$page->url.$page->pagenum?>'><img src="image1/last.gif" border="0"></a></td>
              </tr>
            <tr>
              <td height="30">&nbsp;</td>
            </tr>
          </table></td>
      </tr>
    </table></td>
    <td background="images/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="middle" background="images/mail_leftbg.gif"><img src="images/buttom_left2.gif" width="17" height="17" /></td>
      <td height="17" valign="top" background="images/buttom_bgs.gif"><img src="images/buttom_bgs.gif" width="17" height="17" /></td>
    <td background="images/mail_rightbg.gif"><img src="images/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>

</body>
</html>
