<?
require_once("../config/dbconn.php");require_once("../config/powercls.php");
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	
}
.red{
	color:red
}
-->
</style>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/utils.js"></script>
<script type="text/javascript" src="js/user_edit.js"></script>
<script language="javascript" type="text/javascript" src="../calendar/WdatePicker.js"></script>
<link href="images/skin.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" height="29" valign="top" background="images/mail_leftbg.gif"><img src="images/left-top-right.gif" width="17" height="29" /></td>
    <td width="935" height="29" valign="top" background="images/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">会员查询</div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="images/mail_rightbg.gif"><img src="images/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td height="71" valign="middle" background="images/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9"><table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td height="13" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td valign="top">
		<form id="form1" name="form1" method="post" action="user_lst.php?act=query">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td colspan="3"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="nowtable">
                <tr>
                  <td class="left_bt2">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td height="30" colspan="3">
			  
			  <table width="100%" height="570" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td height="30" align="center" class="left_txt">会员名</td>
                  <td height="30" class="left_txt"><label>
                    <input name="username" type="text" id="username">
                  </label></td>
                </tr>
                <tr>
                  <td height="30" align="center" class="left_txt">昵称</td>
                  <td height="30" class="left_txt"><label>
                    <input name="nickname" type="text" id="nickname">
                  </label></td>
                </tr>
                <tr>
                  <td height="30" align="center" class="left_txt">级别</td>
                  <td height="30" class="left_txt"><label>
                    <select name="rank" id="rank">
                      <option value="">请选择</option>
                      <?
				  foreach($hyrankary as $k1=>$v1){
				  	echo "<option value='{$k1}'>{$v1}</option>";
				  }
				  ?>
                    </select>
                  </label></td>
                </tr>
                <tr>
                  <td height="30" align="center" class="left_txt">性别</td>
                  <td height="30" class="left_txt"><label>
                    <select name="sex" id="sex">
						<option value="">全部</option>
                      <option value="男">男</option>
                      <option value="女">女</option>
                    </select>
                  </label></td>
                </tr>
                <tr>
                  <td height="30" align="center" class="left_txt">注册类型</td>
                  <td height="30" class="left_txt"><select name="iskong" id="iskong">
				  <option value="">全部</option>
                    <option value="0">实单</option>
                    <option value="1">空单</option>
                  </select></td>
                </tr>
                <tr>
                  <td height="30" align="center" class="left_txt">推荐用户</td>
                  <td height="30" class="left_txt"><input name="tjuser" type="text" id="tjuser"></td>
                </tr>
                <tr style="display:">
                  <td height="30" align="center" class="left_txt">管理用户</td>
                  <td height="30" class="left_txt"><input name="gluser" type="text" id="gluser"></td>
                </tr>
                <tr style="display:">
                  <td height="30" align="center" class="left_txt">位置</td>
                  <td height="30" class="left_txt"><select name="pos" id="pos" >
                    <option value="">全部</option>
                    <?
					    foreach($sysposary as $k1=>$v1){
							echo "<option value='{$k1}'";
							if ($pos==$k1) echo " selected";
							echo ">{$v1}</option>";
						  }
					   ?>
                  </select></td>
                </tr>
                <tr>
                  <td height="30" align="center" class="left_txt">是否服务中心</td>
                  <td height="30" class="left_txt"><select name="isdp" id="isdp">
                    <option value="">全部</option>
                   <option value="1">是</option>
				   <option value="0">否</option>
                  </select></td>
                </tr>
                <tr>
                  <td height="30" align="center" class="left_txt">所属服务中心</td>
                  <td height="30" class="left_txt"><input name="bduser" type="text" id="bduser"></td>
                </tr>
                <tr>
                  <td height="30" align="center" class="left_txt">QQ</td>
                  <td height="30" class="left_txt"><input name="qq" type="text" id="qq"></td>
                </tr>
                <tr>
                  <td height="30" align="center" class="left_txt">EMAIL</td>
                  <td height="30" class="left_txt"><input name="email" type="text" id="email"></td>
                </tr>
                <tr>
                  <td height="30" align="center" class="left_txt">手机</td>
                  <td height="30" class="left_txt"><input name="mobile" type="text" id="mobile"></td>
                </tr>
                <tr>
                  <td height="30" align="center" class="left_txt">所在地区</td>
                  <td height="30" class="left_txt"><select name="province" id="province" onChange="provincechange(this.value)" style="width:140px;">
					   <option value="">请选择</option>
					   <?
					   $sqlsf="select * from {$db_prefix}province where 1";
					   $resultsf=$db->query($sqlsf);
					   while($rssf=$db->fetch_array($resultsf)){
					   	echo "<option value='{$rssf['provinceID']}'>{$rssf['province']}</option>";
					   }
					   $db->free_result($resultsf);
					   ?>
				         </select> 
						
				      城市： <select name="city" id="city" onChange="citychange(this.value)" style="width:160px;">
				         </select> 
						 
				      区县： <select name="area" id="area" style="width:160px;">
				         </select>
				    <br> <label id="province_notice" class="red"></label></td>
                </tr>
                <tr>
                  <td height="30" align="center" class="left_txt">开户银行</td>
                  <td height="30" class="left_txt"><select name="bank" id="bank">
                    <option value="">全部</option>
                    <?
					   $sqlyh="select * from {$db_prefix}banks where 1";
					   $resultyh=$db->query($sqlyh);
					   while($rsyh=$db->fetch_array($resultyh)){
					   	echo "<option value='{$rsyh['bank']}'>{$rsyh['bank']}</option>";
					   }
					   $db->free_result($resultyh);
					   ?>
                  </select></td>
                </tr>
                <tr style="display:none">
                  <td height="30" align="center" class="left_txt">购物金额</td>
                  <td height="30" class="left_txt"><label>
                    <input name="gwprice1" type="text" id="gwprice1" size="5">
                  -
                  <input name="gwprice2" type="text" id="gwprice2" size="5">
                  美金</label></td>
                </tr>
                <tr>
                  <td height="30" align="center" class="left_txt">报单币金额</td>
                  <td height="30" class="left_txt"><input name="price1" type="text" id="price1" size="5">
-
  <input name="price2" type="text" id="price2" size="5">
美金</td>
                </tr>
                <tr style="display:">
                  <td height="30" align="center" class="left_txt">奖金余额</td>
                  <td height="30" class="left_txt"><input name="jjprice1" type="text" id="jjprice1" size="5">
-
  <input name="jjprice2" type="text" id="jjprice2" size="5">
美金</td>
                </tr>
                <tr>
                  <td height="30" align="center" class="left_txt">注册日期</td>
                  <td height="30" class="left_txt"><label>
                    <input name="confirmtime1" type="text" id="confirmtime1" size="12" onClick="WdatePicker()" >
                  -
                  <input name="confirmtime2" type="text" id="confirmtime2" size="12" onClick="WdatePicker()" >
                  </label></td>
                  </tr>
              </table></td>
            </tr>
            
            <tr>
              <td height="30" colspan="3">&nbsp;</td>
            </tr>
            <tr>
              <td width="50%" height="30" align="right"><input type="submit" value="进行查询" name="B1" /></td>
              <td width="6%" height="30" align="right">&nbsp;</td>
              <td width="44%" height="30">&nbsp;</td>
            </tr>
            <tr>
              <td height="30" colspan="3">&nbsp;</td>
            </tr>
          </table>
		  </form>
		  </td>
      </tr>
    </table></td>
    <td background="images/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="middle" background="images/mail_leftbg.gif"><img src="images/buttom_left2.gif" width="17" height="17" /></td>
      <td height="17" valign="top" background="images/buttom_bgs.gif"><img src="images/buttom_bgs.gif" width="17" height="17" /></td>
    <td background="images/mail_rightbg.gif"><img src="images/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>

</body>
</html>