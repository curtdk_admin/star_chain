<?
require_once("../config/dbconn.php");require_once("../config/powercls.php");

//获取会员信息
$sqlhy="select * from {$db_prefix}users where id='$id'";
$rshy=$db->get_one($sqlhy);

if($act=='edit'){
	
	if(trim($pwd)=='') $msg="请输入登陆密码\\n";
	if(trim($pwd1)=='') $msg="请输入二级密码\\n";
	
	//二、	每个护照号码最多使用三次，多余无效
	//检测用户名是否可以使用
	$sqlhz="select count(id) as c from {$db_prefix}users where idcard='".trim($idcard)."' and id!='$id'";
	$rshz=$db->get_one($sqlhz);
	
	if ($isdp==1) $zmdrank=1;else $zmdrank=0;
	
	//插入到数据库
	unset($dataArray);
	$dataArray['nickname']=trim($nickname);
	$dataArray['pwd']=authcode($pwd,'ENCODE');
	$dataArray['pwd1']=authcode($pwd1,'ENCODE');
	$dataArray['qq']=trim($qq);
	$dataArray['mobile']=trim($mobile);
	$dataArray['email']=trim($email);
	$dataArray['province']=trim($province);
	$dataArray['city']=trim($city);
	$dataArray['area']=trim($area);
	$dataArray['sex']=trim($sex);
	$dataArray['bank']=trim($bank);
	$dataArray['bankaddress']=trim($bankaddress);
	$dataArray['bankname']=trim($bankname); 
	$dataArray['bankno']=trim($bankno);
	$dataArray['idcard']=trim($idcard);

	
	$db->update("{$db_prefix}users",$dataArray,"id='$id'");

	//更新成功
	
	echo "<script>alert('会员修改成功');location.href='user_lst.php?pageno={$pageno}';</script>";exit();
}


?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	
}
.red{
	color:red
}
td{
	font-size:12px;
}
-->
</style>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/utils.js"></script>
<script type="text/javascript" src="js/user_edit.js"></script>
<link href="images/skin.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" height="29" valign="top" background="images/mail_leftbg.gif"><img src="images/left-top-right.gif" width="17" height="29" /></td>
    <td width="935" height="29" valign="top" background="images/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">会员修改</div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="images/mail_rightbg.gif"><img src="images/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td height="71" valign="middle" background="images/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9"><table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td height="13" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td valign="top">
		<form id="form1" name="form1" method="post" action="?act=edit" onSubmit="return usereditdo(this);">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td colspan="3"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="nowtable">
                <tr>
                  <td class="left_bt2">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td height="30" colspan="3">
			  
			  <table width="100%" height="750" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td width="17%" height="30" align="center" bgcolor="#f2f2f2" class="left_txt">会员编号</td>
                  <td width="83%" height="30" bgcolor="#f2f2f2" class="left_txt"><?=$rshy['username']?></td>
                </tr>
                <tr>
                  <td height="30" align="center" class="left_txt">姓名</td>
                  <td height="30" class="left_txt"><label>
                    <input name="nickname" type="text" id="nickname" value="<?=$rshy['nickname']?>">
                  </label></td>
                </tr>
                <tr>
                  <td height="30" align="center" bgcolor="#f2f2f2" class="left_txt">性别</td>
                  <td height="30" bgcolor="#f2f2f2" class="left_txt"><label>
                    <select name="sex" id="sex">
					<option value="男" <? if ($rshy['sex']=='男') echo " selected"?>>男</option>
					<option value="女" <? if ($rshy['sex']=='女') echo " selected"?>>女</option>
                    </select>
                  </label></td>
                </tr>
                <tr>
                  <td height="30" align="center" class="left_txt">登陆密码</td>
                  <td height="30" class="left_txt"><label>
                    <input name="pwd" type="password" id="pwd" value="<?=authcode($rshy['pwd'],"DECODE")?>">
                    <span class="red">*</span></label></td>
                </tr>
                <tr>
                  <td height="30" align="center" bgcolor="#f2f2f2" class="left_txt">二级密码</td>
                  <td height="30" bgcolor="#f2f2f2" class="left_txt"><input name="pwd1" type="password" id="pwd1" value="<?=authcode($rshy['pwd1'],"DECODE")?>">
                    <span class="red">*</span></td>
                </tr>

				   <tr style="display:">
				     <td height="30" align="center" class="left_txt">身份证</td>
				     <td height="30" class="left_txt"><input name="idcard" type="text" id="idcard" value="<?=$rshy['idcard']?>">
				       <span class="red">*</span></td>
				     </tr>
				   <tr style="display:">
				     <td height="30" align="center" class="left_txt">QQ</td>
				     <td height="30" class="left_txt"><label>
				       <input name="qq" type="text" id="qq" value="<?=$rshy['qq']?>">
				     </label></td>
				     </tr>
				   <tr>
				     <td height="30" align="center" bgcolor="#f2f2f2" class="left_txt">E-mail</td>
				     <td height="30" bgcolor="#f2f2f2" class="left_txt"><label>
				       <input name="email" type="text" id="email" value="<?=$rshy['email']?>">
				     </label></td>
				     </tr>
				   <tr>
				     <td height="30" align="center" class="left_txt">手机</td>
				     <td height="30" class="left_txt"><input name="mobile" type="text" id="mobile" value="<?=$rshy['mobile']?>"></td>
				     </tr>

				   <tr style="display:">
				     <td height="30" align="center" class="left_txt">所在地区</td>
				     <td height="30" class="left_txt">
				     <select name="province" id="province" onChange="provincechange(this.value)" style="width:140px;">
					   <option value="">请选择</option>
					   <?
					   $sqlsf="select * from {$db_prefix}province where 1";
					   $resultsf=$db->query($sqlsf);
					   while($rssf=$db->fetch_array($resultsf)){
					   	echo "<option value='{$rssf['provinceID']}'";
						if($rshy['province']==$rssf['provinceID']) echo " selected";
						echo ">{$rssf['province']}</option>";
					   }
					   $db->free_result($resultsf);
					   ?>
				         </select> 
						
				      城市： <select name="city" id="city" onChange="citychange(this.value)" style="width:160px;">
					  <?
					  if ($rshy['province']){
					  	   $sqlsf="select * from {$db_prefix}city where father='".$rshy['province']."'";
						   $resultsf=$db->query($sqlsf);
						   while($rssf=$db->fetch_array($resultsf)){
							echo "<option value='{$rssf['cityID']}'";
							if($rshy['city']==$rssf['cityID']) echo " selected";
							echo ">{$rssf['city']}</option>";
						   }
						   $db->free_result($resultsf);
					  }
					  ?>
				         </select> 
						 
				      区县： <select name="area" id="area" style="width:160px;">
					  <?
					  if ($rshy['city']){
					  	   $sqlsf="select * from {$db_prefix}area where father='".$rshy['city']."'";
						   $resultsf=$db->query($sqlsf);
						   while($rssf=$db->fetch_array($resultsf)){
							echo "<option value='{$rssf['areaID']}'";
							if($rshy['area']==$rssf['areaID']) echo " selected";
							echo ">{$rssf['area']}</option>";
						   }
						   $db->free_result($resultsf);
					  }
					  ?>
				         </select>
				       <span class="red">*</span><br> <label id="province_notice" class="red"></label></td>
				     </tr>
				   <tr>
				     <td height="30" align="center" bgcolor="#f2f2f2" class="left_txt">开户银行</td>
				     <td height="30" bgcolor="#f2f2f2" class="left_txt"><label>
				       <select name="bank" id="bank">
					   <option value="">请选择</option>
					   <?
					   $sqlyh="select * from {$db_prefix}banks where 1";
					   $resultyh=$db->query($sqlyh);
					   while($rsyh=$db->fetch_array($resultyh)){
					   	echo "<option value='{$rsyh['bank']}'";
						if ($rshy['bank']==$rsyh['bank']) echo " selected";
						echo ">{$rsyh['bank']}</option>";
					   }
					   $db->free_result($resultyh);
					   ?>
				         </select>
				       <span class="red">*</span></label> <label class="red" id="bank_notice"></label></td>
				     </tr>
				   <tr>
				     <td height="30" align="center" class="left_txt">银行地址</td>
				     <td height="30" class="left_txt"><label>
				       <input name="bankaddress" type="text" id="bankaddress" size="40" value="<?=$rshy['bankaddress']?>">
				       <span class="red">*</span> </label></td>
				     </tr>
				   <tr>
				     <td height="30" align="center" bgcolor="#f2f2f2" class="left_txt">银行卡号</td>
				     <td height="30" bgcolor="#f2f2f2" class="left_txt"><label>
				       <input name="bankno" type="text" id="bankno" value="<?=$rshy['bankno']?>">
				       <span class="red">*</span> </label></td>
				     </tr>
				   <tr>
				     <td height="30" align="center" class="left_txt">卡号开户名</td>
				     <td height="30" class="left_txt"><input name="bankname" type="text" id="bankname" value="<?=$rshy['bankname']?>">
                       <span class="red">*</span> </td>
				     </tr>
              </table></td>
            </tr>
            
            <tr>
              <td height="30" colspan="3" align="center"><label id="reg_notice" class="red"></label></td>
            </tr>
            <tr>
              <td width="50%" height="30" align="right"><span class="left_txt">
                <input name="pageno" type="hidden" id="pageno" value="<?=$pageno?>">
                <input type="hidden" name="id" id="id" value="<?=$id?>">
                </span>
                <input type="submit" value="完成以上修改" name="B1" /></td><td width="6%" height="30" align="right">&nbsp;</td>
                <td width="44%" height="30">&nbsp;</td>
            </tr>
            <tr>
              <td height="30" colspan="3">&nbsp;</td>
            </tr>
          </table>
		  </form>
		  </td>
      </tr>
    </table></td>
    <td background="images/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="middle" background="images/mail_leftbg.gif"><img src="images/buttom_left2.gif" width="17" height="17" /></td>
      <td height="17" valign="top" background="images/buttom_bgs.gif"><img src="images/buttom_bgs.gif" width="17" height="17" /></td>
    <td background="images/mail_rightbg.gif"><img src="images/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>

</body>
</html>