<?
require_once("../config/dbconn.php");require_once("../config/powercls.php");

if($act=='add'){
	$msg='';
	if(trim($departid)=='') $msg="请输入部门\\n";
	if(trim($username)=='') $msg="请输入用户名\\n";
	if(trim($pwd)=='') $msg="请输入密码\\n";
	if(trim($pwd1)=='') $msg="请输入二级密码\\n";
	if ($msg!=''){
		echo "<script>alert('$msg');history.back();</script>";exit();
	}
	//插入数据库
	$dataArray['departid']=$departid;
	$dataArray['username']=$username;
	$dataArray['pwd']=authcode($pwd,"ENCODE");
	$dataArray['pwd1']=authcode($pwd1,"ENCODE");
	$dataArray['nickname']=$nickname;
	$dataArray['contact']=$contact;
	$dataArray['addtime']=$curtime;
	
	if ($id){
		$condition="id='$id'";
		$db->update("{$db_prefix}admin888",$dataArray,$condition);
		echo "<script>alert('管理员已修改');location.href='admin_lst.php?pageno={$pageno}';</script>";exit();
	}else{
		$db->insert("{$db_prefix}admin888",$dataArray);
		echo "<script>alert('管理员添加成功');location.href='admin_lst.php';</script>";exit();
	}
}

if ($id){
	$sql1="select * from {$db_prefix}admin888 where id='$id'";
	$rs1=$db->get_one($sql1);
}
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	
}
-->
</style>

<link href="images/skin.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" height="29" valign="top" background="images/mail_leftbg.gif"><img src="images/left-top-right.gif" width="17" height="29" /></td>
    <td width="935" height="29" valign="top" background="images/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">管理员<?=($id)?"修改":"添加"?></div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="images/mail_rightbg.gif"><img src="images/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td height="71" valign="middle" background="images/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9"><table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td height="13" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td valign="top">
		<form id="form1" name="form1" method="post" action="?act=add">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td colspan="3"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="nowtable">
                <tr>
                  <td class="left_bt2">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td height="30" colspan="3">
			  
			  <table width="100%" height="180" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td height="30" align="center" bgcolor="#f2f2f2" class="left_txt">所在部门</td>
                  <td height="30" bgcolor="#f2f2f2" class="left_txt"><select name="departid">
                    <option value="">请选择部门</option>
                    <?
		$sql="select * from {$db_prefix}departs where 1 order by orders asc";
		$result=$db->query($sql);
		while($rs=$db->fetch_array($result)){
			echo "<option value='".$rs['id']."'";
			if ($rs1['departid']==$rs['id']) echo " selected";
			echo ">".str_repeat("------",$rs['depth']-1).$rs['flname']."</option>";
		}
		$db->free_result($result);
		?>
                  </select></td>
                  </tr>
                <tr>
                  <td height="30" align="center" class="left_txt">管理员</td>
                  <td height="30" class="left_txt"><input type="text" name="username" value="<?=$rs1['username']?>" /></td>
                  </tr>
                <tr>
                  <td height="30" align="center" bgcolor="#f2f2f2" class="left_txt">密码</td>
                  <td height="30" bgcolor="#f2f2f2" class="left_txt"><label>
                    <input name="pwd" type="password" id="pwd" value="<?=authcode($rs1['pwd'],"DECODE")?>">
                  </label></td>
                </tr>
                <tr>
                  <td height="30" align="center" class="left_txt">二级密码</td>
                  <td height="30" class="left_txt"><input name="pwd1" type="password" id="pwd1" value="<?=authcode($rs1['pwd1'],"DECODE")?>"></td>
                </tr>
                <tr>
                  <td height="30" align="center" class="left_txt">昵称</td>
                  <td height="30" class="left_txt"><label>
                    <input name="nickname" type="text" id="nickname" value="<?=$rs1['nickname']?>">
                  </label></td>
                </tr>
				   <tr>
                  <td height="30" align="center" bgcolor="#f2f2f2" class="left_txt">联系方式</td>
                  <td height="30" bgcolor="#f2f2f2" class="left_txt"><label>
                    <input name="contact" type="text" id="contact" value="<?=$rs1['contact']?>">
                  </label></td>
                </tr>
              </table></td>
            </tr>
            
            <tr>
              <td height="30" colspan="3">&nbsp;</td>
            </tr>
            <tr>
              <td width="50%" height="30" align="right"><input name="id" type="hidden" id="id" value="<?=$id?>">
              <input name="pageno" type="hidden" id="pageno" value="<?=$pageno?>">
<input type="submit" value="完成以上修改" name="B1" /></td>
              <td width="6%" height="30" align="right">&nbsp;</td>
              <td width="44%" height="30"><input type="reset" value="取消设置" name="B12" /></td>
            </tr>
            <tr>
              <td height="30" colspan="3">&nbsp;</td>
            </tr>
          </table>
		  </form>
		  </td>
      </tr>
    </table></td>
    <td background="images/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="middle" background="images/mail_leftbg.gif"><img src="images/buttom_left2.gif" width="17" height="17" /></td>
      <td height="17" valign="top" background="images/buttom_bgs.gif"><img src="images/buttom_bgs.gif" width="17" height="17" /></td>
    <td background="images/mail_rightbg.gif"><img src="images/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>

</body>
</html>