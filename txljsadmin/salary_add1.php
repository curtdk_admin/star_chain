<?
require_once("../config/dbconn.php");require_once("../config/powercls.php");

if($act=='add'){
	$msg='';
	if(trim($jsyear)=='') $msg="请输入结算年份\\n";
	if(trim($jsmonth)=='') $msg="请输入结算月份\\n";
	if ($msg!=''){
		echo "<script>alert('$msg');history.back();</script>";exit();
	}
	/*$beginary=explode('-',trim($beginday));
	$endary=explode('-',trim($endday));
	
	$begintime=mktime(0,0,0,$beginary[1],$beginary[2],$beginary[0]);
	$endtime=mktime(0,0,0,$endary[1],$endary[2],$endary[0]);
	
	if ($endtime<$begintime) $msg.="结束日期不能小于开始日期\\n";*/
	
	if ($msg!=''){
		echo "<script>alert('$msg');history.back();</script>";exit();
	}
	
	//结算是否已经存在了
	/*$sqlchk="select periods from {$db_prefix}salarym where datediff(beginday,'".trim($beginday)."')<=0 and datediff(endday,'".trim($beginday)."')>=0";
	$rschk=$db->get_one($sqlchk);
	if ($rschk['periods']){
		$msg.="已经存在了该日期的结算\\n";
	}else{
		$sqlchk1="select periods from {$db_prefix}salarym where datediff(beginday,'".trim($endday)."')<=0 and datediff(endday,'".trim($endday)."')>=0";
		$rschk1=$db->get_one($sqlchk1);
		if ($rschk1['periods']){
			$msg.="已经存在了该日期的结算\\n";
		}
	}
	*/
	
	$sqlchk="select periods from {$db_prefix}salarym where jsyear='".trim($jsyear)."' and jsmonth='".trim($jsmonth)."'";
	$rschk=$db->get_one($sqlchk);
	if ($rschk['periods']){
		$msg.="已经存在了该月份的结算\\n";
	}
	if ($msg!=''){
		echo "<script>alert('$msg');history.back();</script>";exit();
	}
	
	$sqlqs="select max(periods) as c from {$db_prefix}salarym where 1";
	$rsqs=$db->get_one($sqlqs);
	$curperiods=$rsqs['c']+1;
	
	//更新到数据库中
	unset($dataArray);
	$dataArray['periods']=$curperiods;
	$dataArray['jsyear']=trim($jsyear);
	$dataArray['jsmonth']=trim($jsmonth);
	$dataArray['addtime']=$curtime;
	$dataArray['state']=0;
	$dataArray['fhprice']=$fhprice;
	
	$db->insert("{$db_prefix}salarym",$dataArray);
	
	echo "<script>alert('奖金结算已添加');location.href='salary_lst1.php';</script>";exit();
}
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	
}
-->
</style>
<script language="javascript" type="text/javascript" src="../calendar/WdatePicker.js"></script>

<link href="images/skin.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" height="29" valign="top" background="images/mail_leftbg.gif"><img src="images/left-top-right.gif" width="17" height="29" /></td>
    <td width="935" height="29" valign="top" background="images/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">月奖金添加</div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="images/mail_rightbg.gif"><img src="images/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td height="71" valign="middle" background="images/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9"><table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td height="13" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td valign="top">
		<form id="form1" name="form1" method="post" action="?act=add">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td colspan="3"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="nowtable">
                <tr>
                  <td class="left_bt2">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td height="30" colspan="3">
			  
			  <table width="100%" height="120" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td height="30" align="center" class="left_txt">奖金期数</td>
                  <td height="30" class="left_txt">
				  <?
				  //获取最大的奖金期数
				  $sqlqs="select max(periods) as c from {$db_prefix}salarym where 1";
				  $rsqs=$db->get_one($sqlqs);
				  echo ($rsqs['c']+1);
				  ?>
				  </td>
                </tr>
                <tr>
                  <td height="30" align="center" class="left_txt">年份</td>
                  <td height="30" class="left_txt"><input name="jsyear" type="text" id="jsyear"/>
                    <span class="left_ts">* 例如：2012</span></td>
                  </tr>
                <tr>
                  <td height="30" align="center" class="left_txt">月份</td>
                  <td height="30" class="left_txt"><label>
                    <input name="jsmonth" type="text" id="jsmonth" >
                    <span class="left_ts">* 例如：01</span></label></td>
                </tr>
				   <tr style="display:">
                  <td height="30" align="center" class="left_txt">公司总业绩</td>
                  <td height="30" class="left_txt"><label>
                    <input name="fhprice" type="text" id="fhprice" value="0">
                  美金</label></td>
                </tr>
              </table></td>
            </tr>
            
            <tr>
              <td height="30" colspan="3"></td>
            </tr>
            <tr>
              <td width="50%" height="30" align="right"><input type="submit" value="添加奖金结算" name="B1" /></td>
              <td width="6%" height="30" align="right">&nbsp;</td>
              <td width="44%" height="30">&nbsp;</td>
            </tr>
            <tr>
              <td height="30" colspan="3">&nbsp;</td>
            </tr>
          </table>
		  </form>
		  </td>
      </tr>
    </table></td>
    <td background="images/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="middle" background="images/mail_leftbg.gif"><img src="images/buttom_left2.gif" width="17" height="17" /></td>
      <td height="17" valign="top" background="images/buttom_bgs.gif"><img src="images/buttom_bgs.gif" width="17" height="17" /></td>
    <td background="images/mail_rightbg.gif"><img src="images/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>

</body>
</html>