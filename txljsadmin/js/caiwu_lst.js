function ptypechange()
{
  var ptype  = Utils.trim($("#ptype").val());
  document.getElementById('type').innerHTML='';
  var opt=document.createElement("option");
	opt.value="";
	opt.innerText="全部类型";
	document.getElementById('type').appendChild(opt);
  var msg = '';
  if (ptype == "")
  {
	$("#ptype_notice").text("请选择账户类型");
	msg = 'ptype';
  }
  
  if(msg != ""){
	$("#reg_notice").text("");
	return false;
  }
  
  $("#ptype_notice").text("");
  
  $.ajax({
	    type: "POST",
	    url: "./ajax_caiwutype.php",
		data: "act=gettype&ptype="+ptype,
		dataType: "text",
		beforeSend: function(){
		    $("#reg_notice").text("稍等...");
		},
		success: function(data){
			var dataary=data.split(':');
			if(dataary[0].indexOf('send_success')!=-1){
					var dataary1=dataary[1].split('|');
					var dataary2=dataary1[0].split(',');
					var dataary3=dataary1[1].split(',');
					for (var i=0;i<dataary2.length;i++){
						var opt=document.createElement("option");
						opt.value=dataary2[i];
						opt.innerText=dataary3[i];
						document.getElementById('type').appendChild(opt);
					}
				    $("#reg_notice").text("获取成功");
				}else{
				    $("#reg_notice").text("出错了！");
			}
		}
	});
}

function citychange()
{
  var city  = Utils.trim($("#city").val());
  document.getElementById('area').innerHTML='';
  var opt=document.createElement("option");
	opt.value="";
	opt.innerText="请选择区县";
	document.getElementById('area').appendChild(opt);
  var msg = '';
  if (city == "")
  {
	$("#province_notice").text("请输入选择城市");
	msg = 'city';
  }
  
  if(msg != ""){
	$("#reg_notice").text("");
	return false;
  }
  
  $("#province_notice").text("");
  
  $.ajax({
	    type: "POST",
	    url: "./ajax_getarea.php",
		data: "act=getarea&father="+city,
		dataType: "text",
		beforeSend: function(){
		    $("#reg_notice").text("稍等...");
		},
		success: function(data){
			var dataary=data.split(':');
			if(dataary[0].indexOf('send_success')!=-1){
					var dataary1=dataary[1].split('|');
					var dataary2=dataary1[0].split(',');
					var dataary3=dataary1[1].split(',');
					for (var i=0;i<dataary2.length;i++){
						var opt=document.createElement("option");
						opt.value=dataary2[i];
						opt.innerText=dataary3[i];
						document.getElementById('area').appendChild(opt);
					}
				    $("#reg_notice").text("县区获取成功");
				    }else{
				    $("#reg_notice").text("出错了！");
			}
		}
	});
}