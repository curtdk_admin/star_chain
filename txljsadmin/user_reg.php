<?
require_once("../config/dbconn.php");require_once("../config/powercls.php");
//是不是第一个会员
$isdyhy=0;
$sqldy="select id from {$db_prefix}users where 1 order by id asc limit 1";
$rsdy=$db->get_one($sqldy);
if (!$rsdy['id']) $isdyhy=1;
if($act=='reg'){
	$msg='';
	//检测用户输入信息的开始
	if(trim($pwd)==''){
		$pwd="123456";$repwd=$pwd;
	}
	if(trim($pwd1)==''){
		$pwd1="123456";$repwd1=$pwd1;
	}
	if(trim($username)=='') $msg="请输入用户名\\n";
	if(trim($pwd)=='') $msg="请输入登陆密码\\n";
	if(trim($repwd)=='') $msg="请确认登陆密码\\n";
	if (trim($pwd)!=trim($repwd)) $msg="二次登陆密码不一致\\n";
	if(trim($pwd1)=='') $msg="请输入二级密码\\n";
	if(trim($repwd1)=='') $msg="请确认二级密码\\n";
	if (trim($pwd1)!=trim($repwd1)) $msg="二次二级密码不一致\\n";

	if ($isdyhy==0){
		if(trim($tjuser)=='') $msg="请输入推荐用户\\n";
		if(trim($gluser)=='') $msg="请输入管理用户\\n";
		if(trim($pos)=='') $msg="请选择区位\\n";
	}
	if(trim($nickname)=='') $msg.="请输入姓名\\n";
	if(trim($bankname)!=trim($nickname)) $msg="请确认姓名与卡号开户名是否一致\\n";
	
	if(trim($agree)!=1) $msg="请阅读并同意注册协议\\n";
	if ($msg!=''){
		echo "<script>alert('$msg');history.back();</script>";exit();
	}
	
	//检测用户名是否可以使用
	$sqlchk="select id from {$db_prefix}users where username='".trim($username)."'";
	$rschk=$db->get_one($sqlchk);
	if ($rschk['id']) $msg.="用户名已经被占用\\n";
	if ($msg!=''){
		echo "<script>alert('$msg');history.back();</script>";exit();
	}
	
	
	//检测推荐人是否存在
	if ($isdyhy==0){
		$sqlchk1="select * from {$db_prefix}users where username='".trim($tjuser)."' and state=1";
		$rschk1=$db->get_one($sqlchk1);
		if (!$rschk1['id']) $msg.="推荐用户不存在\\n";
		if ($msg!=''){
			echo "<script>alert('$msg');history.back();</script>";exit();
		}
		$tjuserid=$rschk1['id'];
		//这是会员推荐的第几个会员
		$sqltjs="select count(id) as c from {$db_prefix}users where tjuser='".trim($tjuser)."' and state=1";
		$rstjs=$db->get_one($sqltjs);
		$tjsnum=intval($rstjs['c'])+2;
	}
	//检测管理人是否存在
	if ($isdyhy==0){
		$sqlchk2="select id,glstr,gldepth from {$db_prefix}users where username='".trim($gluser)."'";
		$rschk2=$db->get_one($sqlchk2);
		if (!$rschk2['id']) $msg.="推荐用户不存在\\n";
		if ($msg!=''){
			echo "<script>alert('$msg');history.back();</script>";exit();
		}
		$gluserid=$rschk2['id'];
		//检测区位
		$sqlchk3="select id from {$db_prefix}users where gluser='".trim($gluser)."' and pos='$pos'";
		$rschk3=$db->get_one($sqlchk3);
		if ($rschk3['id']) $msg.="区位已经被占用\\n";
		if ($msg!=''){
			echo "<script>alert('$msg');history.back();</script>";exit();
		}
    }


    if ($msg!=''){
		echo "<script>alert('$msg');history.back();</script>";exit();
	}
	
	$gldepth=0;$tjdepth=0;$tjstr='';$glstr='';
	if ($isdyhy==0){
		$gldepth=$rschk2['gldepth']+1;  //管理层级
		$tjdepth=$rschk1['tjdepth']+1;  //推荐层级
		//推荐关系
		if ($rschk1['tjstr']) {
            $tjstr=$rschk1['tjstr'].",".$rschk1['id'];
        }else {
            $tjstr=$rschk1['id'];
        }
        if($rschk2['glstar']){
            $glstr=$rschk2['glstr'].",".$rschk2['id'];
        }else{
            $glstr=$rschk2['id'];
        }

	}

	//插入到数据库
	unset($dataArray);
	$dataArray['username']=trim($username);
	$dataArray['nickname']=trim($nickname);
	$dataArray['pwd']=authcode($pwd,'ENCODE');
	$dataArray['pwd1']=authcode($pwd1,'ENCODE');
	$dataArray['tjuser']=trim($tjuser);
	$dataArray['gluser']=trim($gluser);
	$dataArray['pos']=$pos;
	$dataArray['posnum']=$sysposnum;
	$dataArray['regtime']=$curtime;
	$dataArray['confirmtime']=$curtime;
	$dataArray['qq']=trim($qq);
	$dataArray['mobile']=trim($mobile);
	$dataArray['address']=trim($address);
	$dataArray['email']=trim($email);
	$dataArray['province']=trim($province);
	$dataArray['city']=trim($city);
	$dataArray['area']=trim($area);
	$dataArray['sex']=trim($sex);
	$dataArray['bank']=trim($bank);
	$dataArray['bankaddress']=trim($bankaddress);
	$dataArray['bankname']=trim($bankname);
	$dataArray['bankno']=trim($bankno);
	$dataArray['tjdepth']=$tjdepth;
	$dataArray['gldepth']=$gldepth;
	$dataArray['state']=1;
	$dataArray['idcard']=trim($idcard);
	$dataArray['glstr']=$glstr;
	$dataArray['tjstr']=$tjstr;
	$dataArray['ft_times']=trim($times);
	$dataArray['ft_backnumber']=0;
	$dataArray['ft_startdate']=$curtime;;
	$dataArray['ft_isend']=0;
	$dataArray['islock']=0;
	$db->insert("{$db_prefix}users",$dataArray);
	$newuserid=$db->insert_id();

	//注册成功
    if(!empty($newuserid)){
        if(!empty($glstr)){
            $db->insert("{$db_prefix}glgx",array('userid'=>$newuserid,'glstr'=>$glstr));
        }
        if(!empty($tjstr)){
            $db->insert("{$db_prefix}tjgx",array('userid'=>$newuserid,'tjstr'=>$tjstr));
        }
        echo "<script>alert('会员注册成功');location.href='user_lst.php';</script>";exit();
    }else{
        echo "<script>alert('未知原因，会员注册失败');location.href='user_reg.php';</script>";exit();

    }
}
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	
}
.red{
	color:red
}
td{
	font-size:12px;
}
-->
</style>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/utils.js"></script>
<script type="text/javascript" src="js/user_reg.js"></script>
<link href="images/skin.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" height="29" valign="top" background="images/mail_leftbg.gif"><img src="images/left-top-right.gif" width="17" height="29" /></td>
    <td width="935" height="29" valign="top" background="images/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">会员注册</div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="images/mail_rightbg.gif"><img src="images/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td height="71" valign="middle" background="images/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9"><table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td height="13" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td valign="top">
		<form id="form1" name="form1" method="post" action="?act=reg" onSubmit="return userregdo(this);">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td height="30" colspan="3">		  
			  <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td height="30" align="center" bgcolor="#f2f2f2" class="left_txt">用户名</td>
                  <td width="83%" height="30" bgcolor="#f2f2f2" class="left_txt">
				  <? //系统是否自动产生编号
				  if ($glo_autohybh==1){
				  	$curhybh='';
					function hybhchkf(){
						global $db,$db_prefix,$glo_autohypre,$curhybh;
						$hyautobh=$glo_autohypre.rand(111111,999999);
						$sqlf="select id from {$db_prefix}users where username='$hyautobh'";
						$rsf=$db->get_one($sqlf);
						if ($rsf['id']){
							hybhchkf();
						}else{
							$curhybh=$hyautobh;
						}
					}
					hybhchkf();
				  }
				  ?>
				  <label>
                    <input name="username" type="text" id="username" onBlur="usernamefocus();" value="<?=$curhybh?>">
                    <span class="red">*</span></label> <label id="username_notice" class="red"></label></td>
                </tr>
                <tr>
                  <td height="30" align="center" class="left_txt">姓名</td>
                  <td height="30" class="left_txt"><label>
                    <input name="nickname" type="text" id="nickname" onBlur="nicknamefocus();">
                    <span class="red">*</span> </label></td>
                </tr>
                <tr>
                  <td height="30" align="center" bgcolor="#f2f2f2" class="left_txt">性别</td>
                  <td height="30" bgcolor="#f2f2f2" class="left_txt"><label>
                    <select name="sex" id="sex">
					<option value="男">男</option>
					<option value="女">女</option>
                    </select>
                  </label></td>
                </tr>
                <tr>
                  <td height="30" align="center" class="left_txt">登陆密码</td>
                  <td height="30" class="left_txt"><label>
                    <input name="pwd" type="password" id="pwd">
                    <span class="red">* 不填写，默认密码是123456 </span></label></td>
                </tr>
                <tr>
                  <td height="30" align="center" bgcolor="#f2f2f2" class="left_txt">确认登陆密码</td>
                  <td height="30" bgcolor="#f2f2f2" class="left_txt"><input name="repwd" type="password" id="repwd">
                    <span class="red">*</span></td>
                </tr>
                <tr>
                  <td height="30" align="center" class="left_txt">二级密码</td>
                  <td height="30" class="left_txt"><input name="pwd1" type="password" id="pwd1">
                    <span class="red">*不填写，默认密码是123456</span></td>
                </tr>
                <tr>
                  <td height="30" align="center" bgcolor="#f2f2f2" class="left_txt">确认二级密码</td>
                  <td height="30" bgcolor="#f2f2f2" class="left_txt"><label>
                    <input name="repwd1" type="password" id="repwd1">
                    <span class="red">*</span></label></td>
                </tr>

				  <?
				  if ($isdyhy==0){
				  ?>
				   <tr>
				     <td height="30" align="center" class="left_txt">推荐用户</td>
				     <td height="30" class="left_txt">
                         <label>
				            <input name="tjuser" type="text" id="tjuser" onBlur="tjuserfocus();" value="<?=$tjuser?>">
				             <span class="red">*</span>
                         </label>
                         <label class="red" id="tjuser_notice"></label>
                     </td>
				   </tr>
                  <tr>
                      <td height="30" align="center" class="left_txt">管理用户</td>
                      <td height="30" class="left_txt">
                          <label>
                              <input name="gluser" type="text" id="gluser" onBlur="gluserfocus();" value="<?=$gluser?>">
                              <span class="red">*</span>
                          </label>
                          <label class="red" id="gluser_notice"></label>
                      </td>
                  </tr>
                  <tr style="display:">
                      <td height="30" align="center" class="left_txt">位置</td>
                      <td height="30" class="left_txt">
                          <label>
                              <select name="pos" id="pos" onChange="posfocus();">
                                  <option value="">请选择</option>
                                  <?
                                  foreach($sysposary as $k1=>$v1){
                                      echo "<option value='{$k1}'";
                                      if ($pos==$k1) echo " selected";
                                      echo ">{$v1}</option>";
                                  }
                                  ?>
                              </select>
                              <span class="red">*</span>
                          </label>
                          <label class="red" id="pos_notice"></label>
                      </td>
                  </tr>
				<?
				  }
				?>

                 
                      <tr>
				      <td height="30" align="center" class="left_txt">身份证</td>
				      <td height="30" class="left_txt"><label>
				        <input name="idcard" type="text" id="idcard">
				      </label></td>
				      </tr>				    
				    <tr>
				     <td height="30" align="center" class="left_txt">QQ</td>
				     <td height="30" class="left_txt"><label>
				       <input name="qq" type="text" id="qq">
				     </label></td>
				     </tr>
				   <tr>
				     <td height="30" align="center" bgcolor="#f2f2f2" class="left_txt">E-mail</td>
				     <td height="30" bgcolor="#f2f2f2" class="left_txt"><label>
				       <input name="email" type="text" id="email">
				     </label></td>
				     </tr>
				   <tr>
				     <td height="30" align="center" class="left_txt">手机</td>
				     <td height="30" class="left_txt"><label>
				       <input name="mobile" type="text" id="mobile">
				     </label></td>
				     </tr>
				   <tr style="display:">
				     <td height="30" align="center" bgcolor="#f2f2f2" class="left_txt">地址</td>
				     <td height="30" bgcolor="#f2f2f2" class="left_txt"><label>
				       <input name="address" type="text" id="address" size="50">
				     </label></td>
				     </tr>
				   <tr style="display:">
				     <td height="30" align="center" class="left_txt">所在地区</td>
				     <td height="30" class="left_txt">
				     <select name="province" id="province" onChange="provincechange(this.value)" style="width:140px;">
					   <option value="">请选择</option>
					   <?
					   $sqlsf="select * from {$db_prefix}province where 1";
					   $resultsf=$db->query($sqlsf);
					   while($rssf=$db->fetch_array($resultsf)){
					   	echo "<option value='{$rssf['provinceID']}'>{$rssf['province']}</option>";
					   }
					   $db->free_result($resultsf);
					   ?>
				         </select> 
						
				      城市： <select name="city" id="city" onChange="citychange(this.value)" style="width:160px;">
				         </select> 
						 
				      区县： <select name="area" id="area" style="width:160px;">
				         </select>
				      <br> 
				      <label id="province_notice" class="red"></label></td>
				     </tr>
				   <tr>
				     <td height="30" align="center" bgcolor="#f2f2f2" class="left_txt">开户银行</td>
				     <td height="30" bgcolor="#f2f2f2" class="left_txt"><label>
				       <select name="bank" id="bank">
					   <option value="">请选择</option>
					   <?
					   $sqlyh="select * from {$db_prefix}banks where 1";
					   $resultyh=$db->query($sqlyh);
					   while($rsyh=$db->fetch_array($resultyh)){
					   	echo "<option value='{$rsyh['bank']}'>{$rsyh['bank']}</option>";
					   }
					   $db->free_result($resultyh);
					   ?>
				         </select>
				     </label>
				     <label class="red" id="bank_notice"></label></td>
				     </tr>
				   <tr>
				     <td height="30" align="center" class="left_txt">银行地址</td>
				     <td height="30" class="left_txt"><label>
				       <input name="bankaddress" type="text" id="bankaddress" size="40">
				     </label></td>
				     </tr>
				   <tr>
				     <td height="30" align="center" bgcolor="#f2f2f2" class="left_txt">银行卡号</td>
				     <td height="30" bgcolor="#f2f2f2" class="left_txt"><label>
				       <input name="bankno" type="text" id="bankno">
				     </label></td>
				     </tr>
				   <tr>
				     <td height="30" align="center" class="left_txt">卡号开户名</td>
				     <td height="30" class="left_txt"><label>
				       <input name="bankname" type="text" id="bankname">
				     </label></td>
				     </tr>
				   <tr>
                  <td height="30" align="center" bgcolor="#f2f2f2" class="left_txt"><label><input name="agree" type="checkbox" id="agree" value="1" checked="checked"> <a href="user_regxy.php" target="_blank">同意注册协议</a></label>   <span class="red">*</span></td>
					<td><div style="width:400px; height:200px; overflow:scroll">
					<?
					//获取协议
					$sqlxy="select * from {$db_prefix}xieyi where 1";
					$rsxy=$db->get_one($sqlxy);
					echo stripslashes($rsxy['content']);
					?>
					</div>	
					
					</td>
                </tr>
              </table></td>
            </tr>
            
            <tr>
              <td height="30" colspan="3" align="center"><label id="reg_notice" class="red"></label></td>
            </tr>
            <tr>
              <td width="50%" height="30" align="right"><span class="left_txt">
                <input type="hidden" name="isdyhy" id="isdyhy" value="<?=$isdyhy?>">
                </span>
                <input type="submit" value="完成以上修改" name="B1" /></td><td width="6%" height="30" align="right">&nbsp;</td>
              <td width="44%" height="30"><input type="reset" value="取消设置" name="B12" /></td>
            </tr>
            <tr>
              <td height="30" colspan="3">&nbsp;</td>
            </tr>
          </table>
		  </form>
		  </td>
      </tr>
    </table></td>
    <td background="images/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="middle" background="images/mail_leftbg.gif"><img src="images/buttom_left2.gif" width="17" height="17" /></td>
      <td height="17" valign="top" background="images/buttom_bgs.gif"><img src="images/buttom_bgs.gif" width="17" height="17" /></td>
    <td background="images/mail_rightbg.gif"><img src="images/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>

</body>
</html>
