<?
require_once("../config/dbconn.php");require_once("../config/powercls.php");

//奖金结算
$lstperiods=$periods-1;

$sqlp="select * from {$db_prefix}salarym where periods='$periods'";
$rsp=$db->get_one($sqlp);
$jsyear=$rsp['jsyear'];$jsmonth=$rsp['jsmonth'];$curfhprice=$rsp['fhprice'];
$jsstate=$rsp['state'];$jsstate1=$rsp['state1'];$jsfhprice=$rsp['fhprice'];

$jsunixtime=mktime(0,0,0,$jsmonth,1,$jsyear);
$maxday=date('t',$jsunixtime);
$beginday=$jsyear.'-'.$jsmonth.'-01';
$endday=$jsyear.'-'.$jsmonth.'-'.$maxday;
$jstmptime=$jsyear*12+$jsmonth;
$jsmonth1=$jsyear.'-'.$jsmonth;


//新增的业绩
$filter1=" and datediff(from_unixtime(confirmtime),'{$endday}')<=0";

//发放了就不能结算
if ($jsstate==2){
	die("本期奖金已经发放了.不能再次结算<br>");
}
if ($jsstate1==1){
	die("本期奖金正在被其他管理员结算中。请勿重复结算<br>");
}
if ($jsstate==1){
	$condition="periods='$periods'";
	$db->delete("{$db_prefix}salarym1",$condition);
}

//插入到满足结算条件的会员到salary1
$sqlgx="insert into {$db_prefix}salarym1(periods,userid,username,rank,rank1) select '$periods',id,username,rank,rank1 from {$db_prefix}users where state=1 and datediff(from_unixtime(confirmtime,'%Y-%m-%d'),'{$endday}')<=0";
$db->query($sqlgx);

//更新会员的星级
$sql="select * from {$db_prefix}users where state=1".$filter1." order by gldepth asc";
$result=$db->query($sql);
while($rs=$db->fetch_array($result)){
	//获取下面的会员
	$hytdpv=0;unset($hytdpvary);$hytdpv1=0;$hytdpv2=0;
	$hytdpvary=array();
	$sqlxj="select * from {$db_prefix}users where gluser='".$rs['username']."' and state=1".$filter1;
	$resultxj=$db->query($sqlxj);
	while($rsxj=$db->fetch_array($resultxj)){
		$hytdpv+=$rsxj['hytdpv'];
		$hytdpvary[]=$rsxj['hytdpv'];
	}
	$db->free_result($resultxj);
	
	rsort($hytdpvary);
	$i=0;
	foreach($hytdpvary as $k1=>$v1){
		if ($i==0) $hytdpv1=$v1;
		$i++;
	}
	//小市场业绩
	$hytdpv2=$hytdpv-$hytdpv1;
	
	foreach($hyrank1ary as $k1=>$v1){
		$glornameyj="glo_starpvall_{$k1}";
		$yqyj=$$glornameyj*10000;
		if ($hytdpv>=$yqyj){
			if ($k1<5){
				//看小市场的业绩总额
				$glornameyj1="glo_starpvnew_{$k1}";
				if ($hytdpv2>=$$glornameyj1){
					$sqlgx="update {$db_prefix}salarym1 set rank1='$k1' where userid='".$rs['id']."' and periods='$periods'";
					$db->query($sqlgx);
				}
			}else{
				$k2=$k1-1;
				//其中两个部门中各有一个四星 
				$leftid=0;$rightid=0;
				$sqlzq="select * from {$db_prefix}users where gluser='".$rs['username']."' and state=1 and pos=1".$filter1;
				$rszq=$db->get_one($sqlzq);
				if ($rszq){
					$leftid=$rszq['id'];
				}
				$sqlzq="select * from {$db_prefix}users where gluser='".$rs['username']."' and state=1 and pos=2".$filter1;
				$rszq=$db->get_one($sqlzq);
				if ($rszq){
					$rightid=$rszq['id'];
				}
				//左右区都有会员
				if (($leftid>0)&&($rightid>0)){
					$isok5=0;
					$sql4="select * from {$db_prefix}salarym1 where userid='$leftid' and rank1>={$k2} and periods='$periods'";
					$rs4=$db->get_one($sql4);
					if ($rs4['id']){
						$sql5="select * from {$db_prefix}salarym1 where userid='$rightid' and rank1>={$k2} and periods='$periods'";
						$rs5=$db->get_one($sql5);
						if ($rs5['id']){
							$isok5=1;
						}
					}
					if($isok5==0){
						//判断其他下级是否满足了4行的要求
						$sql4="select id from {$db_prefix}salarym1 where userid in(select userid from {$db_prefix}glgx where find_in_set('$leftid',glstr)>0) and rank1>={$k2} and periods='$periods'";
						$rs4=$db->get_one($sql4);
						if ($rs4){
							$sql5="select id from {$db_prefix}salarym1 where userid in(select userid from {$db_prefix}glgx where find_in_set('$rightid',glstr)>0) and rank1>={$k2} and periods='$periods'";
							$rs5=$db->get_one($sql5);
							if ($rs5){
								$isok5=1;
							}
						}
					}
					//如果是
					if($isok5==1){
						$sqlgx="update {$db_prefix}salarym1 set rank1='$k1' where userid='".$rs['id']."' and periods='$periods'";
						$db->query($sqlgx);
					}
				}
			}
			
			//////////////////
		}
	}
}
$db->free_result($result);


//培育奖 上期未完成重复消费者不享受此奖项。
$sql="select * from {$db_prefix}salarym1 where periods='$periods'";
$result=$db->query($sql);
while($rs=$db->fetch_array($result)){
	//获取这个会员本月的经营奖
	$jyjprice=0;
	$sqlj="select sum(a.jyjprice) as c from {$db_prefix}salary1 a,{$db_prefix}salary b where a.periods=b.periods and a.userid='".$rs['userid']."' and left(b.beginday,7)='".$jsmonth1."'";
	$rsj=$db->get_one($sqlj);
	$jyjprice=floatval($rsj['c']);
	if ($jyjprice>0){
		//获取这个会员的上级
		$sqlgx="select * from {$db_prefix}tjgx where userid='".$rs['userid']."'";
		$rsgx=$db->get_one($sqlgx);
		$tjstr='';
		$tjstr=$rsgx['tjstr'];
		if ($tjstr){
			$tjary=explode(',',$tjstr);
			rsort($tjary);
			$tmpk=0;
			foreach($tjary as $k1=>$v1){
				$pyjprice=0;
				$sqlhy="select id,rank1 from {$db_prefix}salarym1 where periods='$periods' and userid='$v1' and rank1>0";
				$rshy=$db->get_one($sqlhy);
				
				if ($rshy['id']){
					//看会员上月是否有重复消费了、这个地方临时没有做
					$sqlzg1="select * from {$db_prefix}salarym1 where periods='$lstperiods' and userid='$v1'";
					$rszg1=$db->get_one($sqlzg1);
					if($rszg1['iscfxf']==1){
					
						$glornamedai="glo_pyjdai".$rshy['rank1'];
						if ($$glornamedai>=$tmpk){
							$glornamerate="glo_pyjrate".$rshy['rank1'];
							$jjrateary=explode(',',$$glornamerate);
							$pyjprice=$jyjprice*$jjrateary[$tmpk-1]/100;
							if ($pyjprice>0){
								$sqlj="update {$db_prefix}salarym1 set pyjprice=pyjprice+'$pyjprice' where periods='$periods' and userid='$v1'";
								$db->query($sqlj);
							}
							$tmpk++;
						}
					}
					
					
				}
			}
		}
	}
	//重复消费
	$sqlcf="select sum(price) as c from {$db_prefix}orders where username='".$rs['username']."' and state>0 and from_unixtime(zftime,'%Y-%m')='".$jsmonth1."'";
	$rscf=$db->get_one($sqlcf);
	$cfprice=floatval($rscf['c']);
	$iscfxf=0;
	if ($cfprice>=$glo_cfxfprice){
		$iscfxf=1;
		$sqlgx="update {$db_prefix}salarym1 set iscfxf=1,cfprice='".$cfprice."' where periods='$periods' and userid='".$rs['userid']."'";
		$db->query($sqlgx);
	}
	if ($iscfxf==1){
		//他的推荐上级
		$sqlgx="select * from {$db_prefix}tjgx where userid='".$rs['userid']."'";
		$rsgx=$db->get_one($sqlgx);
		$tjstr='';
		$tjstr=$rsgx['tjstr'];
		if ($tjstr){
			$tjary=explode(',',$tjstr);
			rsort($tjary);
			$tmpk=0;
			foreach($tjary as $k1=>$v1){
				$cfxfprice=0;
				$sqlhy="select id,rank from {$db_prefix}salarym1 where periods='$periods' and userid='$v1' and rank1>0";
				$rshy=$db->get_one($sqlhy);
				if ($rshy['id']){
					$glornamedai="glo_cfxfdai".$rshy['rank'];
					if ($$glornamedai>=$tmpk){
						$cfxfprice=$cfprice*$glo_cfxfrate/100;
						$sqlj="update {$db_prefix}salarym1 set cfxfprice=cfxfprice+'$cfxfprice' where periods='$periods' and userid='$v1'";
						$db->query($sqlj);
						$tmpk++;
					}
				}
			}
		}
	}
}
$db->free_result($result);
//旅游奖
if ($curfhprice==0){
	//获取公司本月的总业绩
	$gsuserid=0;
	$sqlgs="select id from {$db_prefix}users where ifnull(tjuser,0)=0 and ifnull(gluser,0)=0";
	$rsgs=$db->get_one($sqlgs);
	$gsuserid=$rsgs['id'];
	
	if ($gsuserid>0){
		$sqlyj="select (leftyj1+leftyj2+leftyj3+leftyj4) as c from {$db_prefix}hyyj where userid='$gsuserid' and left(curday,7)='{$jsmonth1}'";
		$rsyj=$db->get_one($sqlyj);
		$curfhprice=floatval($rsyj['c']);
	}
}

if ($curfhprice>0){
	$lvfhprice=$curfhprice*$glo_lvyourate/100;
	//获取四星以上者可享受该奖项。
	//b)要求当月合格（1500美金以上奖金者）。
	$sqlhy="select * from {$db_prefix}salarym1 where periods='$periods' and rank1>=4";
	$resulthy=$db->query($sqlhy);
	$hynum=$db->num_rows($resulthy);
	if ($hynum>0){
		while($rshy=$db->fetch_array($resulthy)){
			//获取本月的对碰将的总金额
			$sqldp="select sum(a.jyjprice) as c from {$db_prefix}salary1 a,{$db_prefix}salary b where a.periods=b.periods and a.userid='".$rshy['userid']."' and left(b.beginday,7)='{$jsmonth1}'";
			$rsdp=$db->get_one($sqldp);
			$dpprice=floatval($rsdp['c']);
			if($dpprice>=$glo_lvyouprice){
				$sqlgx="update {$db_prefix}salarym1 set islv=1 where periods='$periods' and userid='".$rshy['userid']."'";
				$db->query($sqlgx);
			}
		}
	}
	$db->free_result($resulthy);
	//获取满足旅游奖的会员
	$sqlhy="select * from {$db_prefix}salarym1 where periods='$periods' and islv=1";
	$resulthy=$db->query($sqlhy);
	$hynum=$db->num_rows($resulthy);
	if ($hynum>0){
		$lvfhpriceavg=$lvfhprice/$hynum;
		while($rshy=$db->fetch_array($resulthy)){
			$sqlj="update {$db_prefix}salarym1 set lvprice='$lvfhpriceavg' where periods='$periods' and userid='".$rshy['userid']."'";
			$db->query($sqlj);
		}
	}
	$db->free_result($resulthy);
}
//名车奖
if ($curfhprice>0){
	$mcfhprice=$curfhprice*$glo_mingcherate/100;
	//a)一钻以上者可享受该奖项。
	//b)要求当月合格（3000美金以上奖金者
	$sqlhy="select * from {$db_prefix}salarym1 where periods='$periods' and rank1>=5";
	$resulthy=$db->query($sqlhy);
	$hynum=$db->num_rows($resulthy);
	if ($hynum>0){
		while($rshy=$db->fetch_array($resulthy)){
			//获取本月的对碰将的总金额
			$sqldp="select sum(a.jyjprice) as c from {$db_prefix}salary1 a,{$db_prefix}salary b where a.periods=b.periods and a.userid='".$rshy['userid']."' and left(b.beginday,7)='{$jsmonth1}'";
			$rsdp=$db->get_one($sqldp);
			$dpprice=floatval($rsdp['c']);
			if($dpprice>=$glo_mingcheprice){
				$sqlgx="update {$db_prefix}salarym1 set ismc=1 where periods='$periods' and userid='".$rshy['userid']."'";
				$db->query($sqlgx);
			}
		}
	}
	$db->free_result($resulthy);
	//获取满足名车奖的会员
	$sqlhy="select * from {$db_prefix}salarym1 where periods='$periods' and ismc=1";
	$resulthy=$db->query($sqlhy);
	$hynum=$db->num_rows($resulthy);
	if ($hynum>0){
		$mcfhpriceavg=$mcfhprice/$hynum;
		while($rshy=$db->fetch_array($resulthy)){
			$sqlj="update {$db_prefix}salarym1 set mcprice='$mcfhpriceavg' where periods='$periods' and userid='".$rshy['userid']."'";
			$db->query($sqlj);
		}
	}
	$db->free_result($resulthy);
}

//分红奖
if ($curfhprice>0){
	$fenhongary=explode(',',$glo_fenhongstr);

	$fhfhprice=$curfhprice*$glo_fenhongrate/100;
	//一钻及以上享受此奖项
	//要求：
	//a)当月合格（5000美金以上奖金者）。

	$sqlhy="select * from {$db_prefix}salarym1 where periods='$periods' and rank1>=5";
	$resulthy=$db->query($sqlhy);
	$hynum=$db->num_rows($resulthy);
	$hynum1=0;
	if ($hynum>0){
		while($rshy=$db->fetch_array($resulthy)){
			//获取本月的对碰将的总金额
			$sqldp="select sum(a.jyjprice) as c from {$db_prefix}salary1 a,{$db_prefix}salary b where a.periods=b.periods and a.userid='".$rshy['userid']."' and left(b.beginday,7)='{$jsmonth1}'";
			$rsdp=$db->get_one($sqldp);
			$dpprice=floatval($rsdp['c']);
			if($dpprice>=$glo_fenhongprice){
				$fhnum=0;
				$fhnum=$fenhongary[$rshy['rank1']-5];
				$sqlgx="update {$db_prefix}salarym1 set isfh=1,fhnum='$fhnum' where periods='$periods' and userid='".$rshy['userid']."'";
				$db->query($sqlgx);
				//会员可以分几分？
				$hynum1+=$fhnum;
			}
		}
	}
	$db->free_result($resulthy);
	//获取满足粉红奖的会员
	if ($hynum1>0){
		$sqlhy="select * from {$db_prefix}salarym1 where periods='$periods' and isfh=1";
		$resulthy=$db->query($sqlhy);
		$hynum=$db->num_rows($resulthy);
		if ($hynum>0){
			$mcfhpriceavg=$mcfhprice/$hynum1;
			while($rshy=$db->fetch_array($resulthy)){
				$fhprice=$mcfhpriceavg*$rshy['fhnum'];
				$sqlj="update {$db_prefix}salarym1 set mcprice='$fhprice' where periods='$periods' and userid='".$rshy['userid']."'";
				$db->query($sqlj);
			}
		}
		$db->free_result($resulthy);
	}
}

//结算完成以后更新数据
$sqlgx="update {$db_prefix}salarym set state=1,state1=0,jstime='$curtime' where periods='$periods'";
$db->query($sqlgx);

echo "本期奖金结算完币<bR>";
echo "<a href='salary_recm.php?periods={$periods}' style='color:red'>进入本期奖金列表</a>";
exit();
?>