<?
require_once("../config/dbconn.php");require_once("../config/powercls.php");

$sql="select * from {$db_prefix}orders where id='$id'";
$rs=$db->get_one($sql);
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	
}
-->
</style>

<link href="images/skin.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
.red {	font-size:12px; color:#FF0000;
}
-->
</style>
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" height="29" valign="top" background="images/mail_leftbg.gif"><img src="images/left-top-right.gif" width="17" height="29" /></td>
    <td width="935" height="29" valign="top" background="images/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">购物订单查看</div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="images/mail_rightbg.gif"><img src="images/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td height="71" valign="middle" background="images/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9"><table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td height="13" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td valign="top">
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td colspan="3"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="nowtable">
                <tr>
                  <td class="left_bt2">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td height="30" colspan="3">
			  
			  <table width="100%" height="300" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td align="center" bgcolor="#f3f3f3" class="left_txt">订单编号</td>
                  <td align="center" class="left_txt"><? echo $rs['id']?>（
                    <? 
				  if($rs['type']==1) echo "零售订单";
				  elseif($rs['type']==2) echo "首购订单";
				  elseif($rs['type']==3) echo "补业绩订单";
				  elseif($rs['type']==4) echo "复消订单";
				  elseif($rs['type']==5) echo "提货订单";
				  ?>
                    ）</td>
                  <td align="center" bgcolor="#f3f3f3" class="left_txt">订购会员</td>
                  <td height="30" align="center" class="left_txt"><? echo $rs['username']?></td>
                </tr>
                <tr>
                  <td align="center" bgcolor="#f3f3f3" class="left_txt">市场价</td>
                  <td align="center" class="left_txt"><?=$rs['scprice']?>
                    </td>
                  <td align="center" bgcolor="#f3f3f3" class="left_txt">购物券</td>
                  <td height="30" align="center" class="left_txt"><?=$rs['price']?>
                    </td>
                </tr>
				  <tr>
				    <!--  <td align="center" bgcolor="#f3f3f3" class="left_txt">折扣</td>
				     <td align="center" class="left_txt"><? if($rs['zhekou']==10) echo "不打折";else echo $rs['zhekou']."折"?></td>-->
				     <td align="center" bgcolor="#f3f3f3" class="left_txt">购物券</td>
				     <td height="30" align="center" class="left_txt"><?=$rs['price']*$rs['zhekou']/10?></td>
			        </tr>
				   <tr>
				     <td align="center" bgcolor="#f3f3f3" class="left_txt">积分</td>
				     <td align="center" class="left_txt"><? echo $rs['pv']?></td>
				     <td align="center" bgcolor="#f3f3f3" class="left_txt">订单时间</td>
				     <td height="30" align="center" class="left_txt"><?=date("Y-m-d H:i:s",$rs['addtime'])?></td>
			        </tr>
				   <tr>
				     <td align="center" bgcolor="#f3f3f3" class="left_txt">订单状态</td>
				     <td align="center" class="left_txt"><? if($rs['state']==0) echo "未支付";elseif($rs['state']==1) echo "已支付";elseif($rs['state']==2) echo "已发货";elseif($rs['state']==3) echo "已收货";?></td>
				     <td align="center" bgcolor="#f3f3f3" class="left_txt">支付时间</td>
				     <td height="30" align="center" class="left_txt"><? if ($rs['zftime']) echo date("Y-m-d H:i:s",$rs['zftime'])?></td>
			        </tr>
				   <tr>
				     <td align="center" bgcolor="#f3f3f3" class="left_txt">发货时间</td>
				     <td align="center" class="left_txt"><?  if ($rs['fhtime']) echo date("Y-m-d H:i:s",$rs['fhtime'])?></td>
				     <td align="center" bgcolor="#f3f3f3" class="left_txt">物流公司</td>
				     <td height="30" align="center" class="left_txt"><?=$rs['wlgs']?></td>
			        </tr>
				   <tr>
				     <td align="center" bgcolor="#f3f3f3" class="left_txt">物流单号</td>
				     <td align="center" class="left_txt"><?=$rs['wlno']?></td>
				     <td align="center" bgcolor="#f3f3f3" class="left_txt">收货人</td>
				     <td height="30" align="center" class="left_txt"><?=$rs['sjname']?></td>
			        </tr>
				   <tr>
				     <td align="center" bgcolor="#f3f3f3" class="left_txt">收货地址</td>
				     <td align="center" class="left_txt"><?=$rs['sjaddress']?></td>
				     <td align="center" bgcolor="#f3f3f3" class="left_txt">联系电话（非常重要）</td>
				     <td height="30" align="center" class="left_txt"><?=$rs['contact']?></td>
			        </tr>
				   <tr>
				     <td align="center" bgcolor="#f3f3f3" class="left_txt">备注</td>
				     <td height="30" colspan="3" align="center" class="left_txt"><?=stripslashes($rs['memo'])?></td>
			        </tr>
				    <tr>
				     <td height="30" colspan="4" align="center" bgcolor="#f2f2f2" class="left_txt"><table width="100%" height="60" border="0" cellpadding="0" cellspacing="1" bgcolor="silver">
                       <tr>
                         <td height="30" align="center" bgcolor="#f2f2f2" class="left_txt">商品名称</td>
                         <td align="center" bgcolor="#f2f2f2" class="left_txt">市场价</td>
                         <td align="center" bgcolor="#f2f2f2" class="left_txt">购物券</td>
                         <td align="center" bgcolor="#f2f2f2" class="left_txt">积分</td>
                         <td align="center" bgcolor="#f2f2f2" class="left_txt">购买数量</td>
                         <td align="center" bgcolor="#f2f2f2" class="left_txt">小计</td>
                         </tr>
                       <tbody bgcolor="#FFFFFF">
                         <?
					$sqldd="select * from {$db_prefix}orders1 where orderid='$id'";
					$resultdd=$db->query($sqldd);
					while($rsdd=$db->fetch_array($resultdd)){
						$sqlp="select * from {$db_prefix}products where id='".$rsdd['proid']."'";
						$rsp=$db->get_one($sqlp);
				?>
                         <tr>
                           <td height="30" align="center" class="left_txt"><a href="../uploads/productimgs/<?=$rsp['pimg']?>" target="_blank"><img src="../uploads/productimgs/<?=$rsp['pimg']?>" border="0" width="100"></a><br>
                               <a href="product_view.php?id=<?=$rsdd['proid']?>">
                               <?=$rsdd['productname']?>
                             </a></td>
                           <td align="center" class="left_txt"><?=$rsdd['scprice']?>
                             </td>
                           <td align="center" class="left_txt"><?=$rsdd['price']?>
                             </td>
                           <td align="center" class="left_txt"><?=$rsdd['pv']?>
                             </td>
                           <td align="center" class="left_txt"><?=$rsdd['num']?>件</td>
                           <td align="center" class="left_txt"><? 
					  
					  echo ($rsdd['price']*$rsdd['num']);
					  ?>
                             </td>
                           </tr>
                         <?
					}
					$db->free_result($resultdd);
				?>
                       </tbody>
                     </table></td>
			        </tr>
              </table></td>
            </tr>
            
            <tr></tr>
            <tr>
              <td height="30" align="right"><span class="red">购物金额小计 ￥
                  <?=$rs['price']?>
                  比市场价 ￥
                  <?=$rs['scprice']?>
                   节省了 ￥
                  <?=($rs['scprice']-$rs['price'])?>
                 </span></td>
              <td height="30" align="right">&nbsp;</td>
              <td height="30">&nbsp;</td>
            </tr>
            <tr>
              <td width="50%" height="30" align="right"><input type="button" value="返回列表" name="B1" onClick="history.back();" /></td>
              <td width="6%" height="30" align="right">&nbsp;</td>
              <td width="44%" height="30">&nbsp;</td>
            </tr>
            <tr>
              <td height="30" colspan="3">&nbsp;</td>
            </tr>
          </table>
		 
		  </td>
      </tr>
    </table></td>
    <td background="images/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="middle" background="images/mail_leftbg.gif"><img src="images/buttom_left2.gif" width="17" height="17" /></td>
      <td height="17" valign="top" background="images/buttom_bgs.gif"><img src="images/buttom_bgs.gif" width="17" height="17" /></td>
    <td background="images/mail_rightbg.gif"><img src="images/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>

</body>
</html>
