<?php
header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
header("Content-Disposition: attachment; filename=\"tixian_lst.xls\"");
require_once("../config/dbconn.php");require_once("../config/powercls.php");
?>
<html>
<style type="text/css">
<!--
.red {color:#FF0000
}
-->
</style>

<body>
<table width="100%"  border="1" cellpadding="0" cellspacing="0">
  <tr>
    <td  >编号</td>
    <td  >用户名</td>
    <td  >银行</td>
    <td  >银行地址</td>
    <td  >卡号</td>
    <td  >开户名</td>
    <td  >金额</td>
    <td  >费用</td>
    <td  >实际金额</td>
    <td  >状态</td>
    <td  >申请时间</td>
    <td  >审核时间</td>
  </tr>
  <?
  $query='';
  if ($action=='query'){
  	if (trim($username)!='') $query.=" and username='".trim($username)."'";
	if (trim($price1)!='') $query.=" and price>='".trim($price1)."'";
	if (trim($price2)!='') $query.=" and price<='".trim($price2)."'";
	if (trim($state)!='') $query.=" and state='".trim($state)."'";
	if (trim($time1)!='') $query.=" and addtime>='".strtotime($time1)."'";
	if (trim($time2)!='') $query.=" and addtime<='".(strtotime($time1)+24*3600)."'";
  }
  $sql="select * from {$db_prefix}tixian where 1";
  if ($query!='') $sql.=$query;
  $result=$db->query($sql);
  while($rs=$db->fetch_array($result)){
  ?>
  <tr>
    <td height="30">
        <?=$rs['id']?></td>
    <td><?=$rs['username']?></td>
    <td><?=$rs['bank']?>    </td>
    <td ><?=$rs['bankaddress']?></td>
    <td ><?=$rs['bankno']?></td>
    <td ><?=$rs['bankname']?></td>
    <td ><?=$rs['price']?></td>
    <td  ><?=$rs['feiyong']?></td>
    <td  ><?=$rs['price']-$rs['feiyong']?></td>
    <td  ><? if ($rs['state']==0) echo "等待审核";elseif ($rs['state']==1) echo "提现成功";elseif($rs['state']==2) echo "提现失败";?></td>
    <td ><?=date("Y-m-d H:i:s",$rs['addtime'])?></td>
    <td ><? if ($rs['edittime']>0) echo date("Y-m-d H:i:s",$rs['edittime'])?></td>
  </tr>
  <?
  }
  $db->free_result($result);
  ?>
</table>
</body>
</html>
