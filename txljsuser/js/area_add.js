function area_addsubmit()
{
	var area  = Utils.trim($("#area").val());
	var province  = Utils.trim($("#province").val());
	var father  = Utils.trim($("#father").val());
	var msg = '';
	if (province == ""){
	    $("#province_notice").text("请选择省份");
		msg = 'area';
	}
	if (area == ""){
	    $("#area_notice").text("请输入区县");
		msg = 'area';
	}
	if (area != ""){
	    $("#area_notice").text("");
		msg = '';
	}
	if (msg != ""){
		$("#reg_notice").text("");
		return false;
	}
}

function provincechange()
{
  var province  = Utils.trim($("#province").val());
  document.getElementById('father').innerHTML='';
  var msg = '';
  if (province == "")
  {
	$("#province_notice").text("请输入选择省份");
	msg = 'province';
  }
  
  if(msg != ""){
	$("#reg_notice").text("");
	return false;
  }
  
  $("#province_notice").text("");
  
  $.ajax({
	    type: "POST",
	    url: "./ajax_getcity.php",
		data: "act=getcity&father="+province,
		dataType: "text",
		beforeSend: function(){
		    $("#reg_notice").text("稍等...");
		},
		success: function(data){
			var dataary=data.split(':');
			if(dataary[0].indexOf('send_success')!=-1){
					var dataary1=dataary[1].split('|');
					var dataary2=dataary1[0].split(',');
					var dataary3=dataary1[1].split(',');
					for (var i=0;i<dataary2.length;i++){
						var opt=document.createElement("option");
						opt.value=dataary2[i];
						opt.innerText=dataary3[i];
						document.getElementById('father').appendChild(opt);
					}
				    $("#reg_notice").text("城市获取成功");
				  }else{
				    $("#reg_notice").text("出错了！");
			}
		}
	});
}