function userregdo(frm)
{
	if(frm.username.value==''){
		alert('请输入用户名');
		frm.username.focus();
		return false;
	}else{
			var userpreg=/^[0-9]{1,}[a-zA-z0-9]{5,}|[a-zA-z]{1,}[a-zA-z0-9]{5,}$/;
			if (!userpreg.test(frm.username.value)){
				alert("用户名必须是数字和字母的组合。且不能少于6位 ");
				frm.username.focus();
				return false;
			}
		}
	/*if(frm.nickname.value==''){
		alert('请输入姓名');
		frm.nickname.focus();
		return false;
	}*/
	/*if(frm.pwd.value==''){
		alert('请输入登陆密码');
		frm.pwd.focus();
		return false;
	}
	if(frm.repwd.value==''){
		alert('请确认登陆密码');
		frm.repwd.focus();
		return false;
	}
	if(frm.pwd.value!=frm.repwd.value){
		alert('两次登陆密码不一致');
		frm.repwd.focus();
		return false;
	}
	if(frm.pwd1.value==''){
		alert('请输入二级密码');
		frm.pwd1.focus();
		return false;
	}
	if(frm.repwd1.value==''){
		alert('请确认二级密码');
		frm.repwd1.focus();
		return false;
	}
	if(frm.pwd1.value!=frm.repwd1.value){
		alert('两次二级密码不一致');
		frm.repwd1.focus();
		return false;
	}*/
	if(frm.rank0.value==''){
		alert('请选择注册级别');
		//frm.rank0.focus();
		return false;
	}
	var isdyhy=frm.isdyhy.value;
	if (isdyhy==0){
		if(frm.tjuser.value==''){
			alert('请输入推荐用户');
			frm.tjuser.focus();
			return false;
		}
		if(frm.gluser.value==''){
			alert('请输入接点用户');
			frm.gluser.focus();
			return false;
		}
		if(frm.pos.value==''){
			alert('请选择位置');
			frm.pos.focus();
			return false;
		}
		/*if(frm.bduser.value==''){
			alert('请输入报单中心');
			frm.bduser.focus();
			return false;
		}*/
	}
	
	/*if(frm.idcard.value==''){
		alert('请选择身份证');
		frm.idcard.focus();
		return false;
	}else{
		var repreg =/(^[0-9]{15}$)|(^[0-9]{17}([0-9]|X|x)$)/;  
	  	if (!repreg.test(frm.idcard.value)){
			alert('请选择正确的身份证');
			frm.idcard.focus();
			return false;
		}
	}
	
	if(frm.province.value==''){
		alert('请选择省份');
		frm.province.focus();
		return false;
	}
	if(frm.city.value==''){
		alert('请选择城市');
		frm.city.focus();
		return false;
	}
	if(frm.area.value==''){
		alert('请选择区县');
		frm.area.focus();
		return false;
	}
	if(frm.bank.value==''){
		alert('请输入开户银行');
		frm.bank.focus();
		return false;
	}
	if(frm.bankaddress.value==''){
		alert('请输入银行地址');
		frm.bankaddress.focus();
		return false;
	}
	if(frm.bankno.value==''){
		alert('请输入银行卡号');
		frm.bankno.focus();
		return false;
	}
	if(frm.bankname.value==''){
		alert('请输入卡号开户名');
		frm.bankname.focus();
		return false;
	}*/
	if(!frm.agree.checked){
		alert('请阅读并同意注册协议');
		frm.agree.focus();
		return false;
	}
	
	var hyregprice=frm.hyregprice.value;
	//return confirm('请确认注册信息，一旦确定，将扣除'+hyregprice+'电子货币，并且将无法删除？');
}

function rankchange(t){
	var frm=document.form1;
	frm.hyregprice.value=document.getElementById("regprice_"+t).value;
}

function usernamefocus(){
	var username  = Utils.trim($("#username").val());
	var msg = '';
	  if (username == "")
	  {
		$("#username_notice").text("请输入用户名");
		msg = 'username';
	  }else{
		var userpreg=/^[0-9]{1,}[a-zA-z0-9]{5,}|[a-zA-z]{1,}[a-zA-z0-9]{5,}$/;
			if (!userpreg.test(username)){
				
				$("#username_notice").text("用户名必须是数字和字母的组合。且不能少于6位 ");
				msg = 'username';
			}  
		}
	  if(msg != ""){
		return false;
	  }
	  
  $.ajax({
	    type: "POST",
	    url: "./ajax_chkusername.php",
		data: "act=chkusername&username="+username,
		dataType: "text",
		beforeSend: function(){
		    $("#username_notice").text("稍等...");
		},
		success: function(data){
			var dataary=data.split(':');
if(dataary[0].indexOf('send_success')!=-1){
					if (dataary[1]==1)
				    $("#username_notice").text("用户名可以使用");
					else
					$("#username_notice").text("用户名已经被占用");
}else{
				    $("#username_notice").text("出错了！");
			}
		}
	});
}

function tjuserfocus(){
	var tjuser  = Utils.trim($("#tjuser").val());
	var msg = '';
	  if (tjuser == "")
	  {
		$("#tjuser_notice").text("请输入推荐用户");
		msg = 'tjuser';
	  }
	  if(msg != ""){
		return false;
	  }
	  
  $.ajax({
	    type: "POST",
	    url: "./ajax_chkusername.php",
		data: "act=chktjuser&tjuser="+tjuser,
		dataType: "text",
		beforeSend: function(){
		    $("#tjuser_notice").text("稍等...");
		},
		success: function(data){
			var dataary=data.split(':');			
			if(dataary[0].indexOf('send_success')!=-1)
			{
				if (dataary[1]==1)
				{
					$("#tjuser_notice").text("推荐用户可以使用");
				}
				if (dataary[1]==2)
				{
					$("#tjuser_notice").text("推荐用户日分红结束，不能推荐");
				}
				if(dataary[1]==0) 
				{$("#tjuser_notice").text("推荐用户不存在");}
			}else{
				    $("#tjuser_notice").text("出错了！");
			}
		}
	});
}
function gluserfocus(){
	var gluser  = Utils.trim($("#gluser").val());
	var pos  = Utils.trim($("#pos").val());
	var msg = '';
	  if (gluser == "")
	  {
		$("#gluser_notice").text("请输入接点用户");
		msg = 'gluser';
	  }
	  if(msg != ""){
		return false;
	  }
	  
  $.ajax({
	    type: "POST",
	    url: "./ajax_chkusername.php",
		data: "act=chkgluser&gluser="+gluser+"&pos="+pos,
		dataType: "text",
		beforeSend: function(){
		    $("#gluser_notice").text("稍等...");
		},
		success: function(data){
			var dataary=data.split(':');
			if(dataary[0].indexOf('send_success')!=-1){
					if (dataary[1]==0)
				    $("#gluser_notice").text("接点用户不存在");
					if (dataary[1]==1)
				    $("#gluser_notice").text("接点用户可以使用");
					if (dataary[1]==2)
					$("#gluser_notice").text("接点用户的区位可以使用");
					if (dataary[1]==3)
					$("#gluser_notice").text("接点用户的区位已被占用");
			}else{
				    $("#gluser_notice").text("出错了！");
			}
		}
	});
}
function posfocus(){
	var gluser  = Utils.trim($("#gluser").val());
	var pos  = Utils.trim($("#pos").val());
	var msg = '';
	if (gluser == "")
	  {
		$("#gluser_notice").text("请输入接点用户");
		msg = 'gluser';
	  }
	  
	  if (pos == "")
	  {
		$("#pos_notice").text("请选择管理区位");
		msg = 'pos';
	  }
	  
	  if(msg != ""){
		return false;
	  }
	  
  $.ajax({
	    type: "POST",
	    url: "./ajax_chkusername.php",
		data: "act=chkpos&gluser="+gluser+"&pos="+pos,
		dataType: "text",
		beforeSend: function(){
		    $("#pos_notice").text("稍等...");
		},
		success: function(data){
			var dataary=data.split(':');
			if(dataary[0].indexOf('send_success')!=-1){
					if (dataary[1]==0)
				    $("#pos_notice").text("管理会员不存在");
					if (dataary[1]==1)
				    $("#pos_notice").text("区位可以使用");
					if (dataary[1]==2)
					$("#pos_notice").text("区位已经被占用");
				   }else{
				    $("#pos_notice").text("出错了！");
			}
		}
	});
}

function bduserfocus(){
	var bduser  = Utils.trim($("#bduser").val());
	var msg = '';
	  if (bduser == "")
	  {
		$("#bduser_notice").text("请输入报单用户");
		msg = 'bduser';
	  }
	  if(msg != ""){
		return false;
	  }
	  
  $.ajax({
	    type: "POST",
	    url: "./ajax_chkusername.php",
		data: "act=chkbduser&bduser="+bduser,
		dataType: "text",
		beforeSend: function(){
		    $("#bduser_notice").text("稍等...");
		},
		success: function(data){
			var dataary=data.split(':');
			if(dataary[0].indexOf('send_success')!=-1){
					if (dataary[1]==1)
				    $("#bduser_notice").text("报单用户可以使用");
					else
					$("#bduser_notice").text("报单用户不存在");
			}else{
				    $("#bduser_notice").text("出错了！");
			}
		}
	});
}

function provincechange()
{
  var province  = Utils.trim($("#province").val());
  document.getElementById('city').innerHTML='';
  document.getElementById('area').innerHTML='';
  var opt=document.createElement("option");
	opt.value="";
	opt.innerText="请选择城市";
	document.getElementById('city').appendChild(opt);
	var opt=document.createElement("option");
	opt.value="";
	opt.innerText="请选择区县";
	document.getElementById('area').appendChild(opt);
  var msg = '';
  if (province == "")
  {
	$("#province_notice").text("请输入选择省份");
	msg = 'province';
  }
  
  if(msg != ""){
	$("#reg_notice").text("");
	return false;
  }
  
  $("#province_notice").text("");
  
  $.ajax({
	    type: "POST",
	    url: "./ajax_getcity.php",
		data: "act=getcity&father="+province,
		dataType: "text",
		beforeSend: function(){
		    $("#reg_notice").text("稍等...");
		},
		success: function(data){
			var dataary=data.split(':');
			if(dataary[0].indexOf('send_success')!=-1){
					var dataary1=dataary[1].split('|');
					var dataary2=dataary1[0].split(',');
					var dataary3=dataary1[1].split(',');
					for (var i=0;i<dataary2.length;i++){
						var opt=document.createElement("option");
						opt.value=dataary2[i];
						opt.innerText=dataary3[i];
						document.getElementById('city').appendChild(opt);
					}
				    $("#reg_notice").text("城市获取成功");
				  }else{
				    $("#reg_notice").text("出错了！");
			}
		}
	});
}

function citychange()
{
  var city  = Utils.trim($("#city").val());
  document.getElementById('area').innerHTML='';
  var opt=document.createElement("option");
	opt.value="";
	opt.innerText="请选择区县";
	document.getElementById('area').appendChild(opt);
  var msg = '';
  if (city == "")
  {
	$("#province_notice").text("请输入选择城市");
	msg = 'city';
  }
  
  if(msg != ""){
	$("#reg_notice").text("");
	return false;
  }
  
  $("#province_notice").text("");
  
  $.ajax({
	    type: "POST",
	    url: "./ajax_getarea.php",
		data: "act=getarea&father="+city,
		dataType: "text",
		beforeSend: function(){
		    $("#reg_notice").text("稍等...");
		},
		success: function(data){
			var dataary=data.split(':');
if(dataary[0].indexOf('send_success')!=-1){
					var dataary1=dataary[1].split('|');
					var dataary2=dataary1[0].split(',');
					var dataary3=dataary1[1].split(',');
					for (var i=0;i<dataary2.length;i++){
						var opt=document.createElement("option");
						opt.value=dataary2[i];
						opt.innerText=dataary3[i];
						document.getElementById('area').appendChild(opt);
					}
				    $("#reg_notice").text("县区获取成功");
				    }else{
				    $("#reg_notice").text("出错了！");
			}
		}
	});
}