function usereditdo(frm)
{
	if(frm.username.value==''){
		alert('请输入用户名');
		frm.username.focus();
		return false;
	}
	if(frm.province.value==''){
		alert('请选择省份');
		frm.province.focus();
		return false;
	}
	if(frm.city.value==''){
		alert('请选择城市');
		frm.city.focus();
		return false;
	}
	if(frm.area.value==''){
		alert('请选择区县');
		frm.area.focus();
		return false;
	}
	if(frm.bank.value==''){
		alert('请输入开户银行');
		frm.bank.focus();
		return false;
	}
	if(frm.bankaddress.value==''){
		alert('请输入银行地址');
		frm.bankaddress.focus();
		return false;
	}
	if(frm.bankno.value==''){
		alert('请输入银行卡号');
		frm.bankno.focus();
		return false;
	}
	if(frm.bankname.value==''){
		alert('请输入卡号开户名');
		frm.bankname.focus();
		return false;
	}
}

function provincechange()
{
  var province  = Utils.trim($("#province").val());
  document.getElementById('city').innerHTML='';
  document.getElementById('area').innerHTML='';
  var opt=document.createElement("option");
	opt.value="";
	opt.innerText="请选择城市";
	document.getElementById('city').appendChild(opt);
	var opt=document.createElement("option");
	opt.value="";
	opt.innerText="请选择区县";
	document.getElementById('area').appendChild(opt);
  var msg = '';
  if (province == "")
  {
	$("#province_notice").text("请输入选择省份");
	msg = 'province';
  }
  
  if(msg != ""){
	$("#reg_notice").text("");
	return false;
  }
  
  $("#province_notice").text("");
  
  $.ajax({
	    type: "POST",
	    url: "./ajax_getcity.php",
		data: "act=getcity&father="+province,
		dataType: "text",
		beforeSend: function(){
		    $("#reg_notice").text("稍等...");
		},
		success: function(data){
			var dataary=data.split(':');
			if(dataary[0].indexOf('send_success')!=-1){
					var dataary1=dataary[1].split('|');
					var dataary2=dataary1[0].split(',');
					var dataary3=dataary1[1].split(',');
					for (var i=0;i<dataary2.length;i++){
						var opt=document.createElement("option");
						opt.value=dataary2[i];
						opt.innerText=dataary3[i];
						document.getElementById('city').appendChild(opt);
					}
				    $("#reg_notice").text("城市获取成功");
			}else{
				    $("#reg_notice").text("出错了！");
			}
		}
	});
}

function citychange()
{
  var city  = Utils.trim($("#city").val());
  document.getElementById('area').innerHTML='';
  var opt=document.createElement("option");
	opt.value="";
	opt.innerText="请选择区县";
	document.getElementById('area').appendChild(opt);
  var msg = '';
  if (city == "")
  {
	$("#province_notice").text("请输入选择城市");
	msg = 'city';
  }
  
  if(msg != ""){
	$("#reg_notice").text("");
	return false;
  }
  
  $("#province_notice").text("");
  
  $.ajax({
	    type: "POST",
	    url: "./ajax_getarea.php",
		data: "act=getarea&father="+city,
		dataType: "text",
		beforeSend: function(){
		    $("#reg_notice").text("稍等...");
		},
		success: function(data){
			var dataary=data.split(':');
			if(dataary[0].indexOf('send_success')!=-1){
					var dataary1=dataary[1].split('|');
					var dataary2=dataary1[0].split(',');
					var dataary3=dataary1[1].split(',');
					for (var i=0;i<dataary2.length;i++){
						var opt=document.createElement("option");
						opt.value=dataary2[i];
						opt.innerText=dataary3[i];
						document.getElementById('area').appendChild(opt);
					}
				    $("#reg_notice").text("县区获取成功");
				   }else{
					   $("#reg_notice").text("出错了！");
			}
		}
	});
}