<?
session_start();
require_once("../config/dbconn.php");

//是否关闭了系统
if($glo_sysclose==1){
	die($glo_closememo);
}

if($act=='userlogin'){
	$msg='';
	if(trim($username)=='') $msg.="请输入用户名\\n";
	if(trim($password)=='') $msg.="请输入密码\\n";
	
	/*if(trim($verifycode)=='') $msg.="请输入验证码\\n";
	else{
		if ($_SESSION['lgnrandcode2']!=trim($verifycode))$msg.="验证码错误\\n";
	}*/
	
	if ($msg!=''){
		echo "<script>alert('".$msg."');history.back();</script>";
		exit();
	}
	$sql="select * from {$db_prefix}users where username='".trim($username)."'";
	$rs=$db->get_one($sql);
	if (!$rs['id']){
		$msg.="用户名或密码错误\\n";
	}else{
		if (authcode($rs['pwd'],"DECODE")!=$password) $msg.="用户名或密码错误\\n";
	}
	if ($msg!=''){
		echo "<script>alert('".$msg."');history.back();</script>";
		exit();
	}
	$_SESSION['sys_userid']=$rs['id'];$_SESSION['sys_username']=$rs['username'];
	
	echo "<script>location.href='index.php';</script>";
	exit();
}
?>
<!DOCTYPE html>
<html lang="en" class="no-js">

    <head>

        <meta charset="utf-8">
        <title>Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- CSS -->
        
        <link rel="stylesheet" href="assets/css/reset.css">
        <link rel="stylesheet" href="assets/css/supersized.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

    
<script language=javascript>
function CheckForm()
{
	if(document.form1.username.value=="")
	{
		alert("请输入会员账户！");
		document.form1.username.focus();
		return false;
	}
	if(document.form1.password.value == "")
	{
		alert("请输入登录密码！");
		document.form1.password.focus();
		return false;
	}
	/*if (document.form1.verifycode.value==""){
       alert ("请输入您的验证码！");
       document.form1.verifycode.focus();
       return(false);
    }*/
	return true;
	
}

</script>
<script language="JavaScript">
if (self != top) top.location.href = window.location.href
</script>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<script language=JavaScript>
<!--
var message="";
///////////////////////////////////
function clickIE() {if (document.all) {(message);return false;}}
function clickNS(e) {if 
(document.layers||(document.getElementById&&!document.all)) {
if (e.which==2||e.which==3) {(message);return false;}}}
if (document.layers) 
{document.captureEvents(Event.MOUSEDOWN);document.onmousedown=clickNS;}
else{document.onmouseup=clickNS;document.oncontextmenu=clickIE;}
document.oncontextmenu=new Function("return false")
// --> 
</script>
</head>
<body>

<div class="page-container">
            <h1>Login</h1>
            <form name="form1" action="login.php?act=userlogin" method="post"  onSubmit="return CheckForm();">
                <input type="text" name="username" class="username" placeholder="会员账号">
                <input type="password" name="password" class="password" placeholder="密码">
                <input type="text" name="code" class="password code" placeholder="验证码">
                
                <input type="image" style="cursor:pointer; " title="刷新验证码" id="refresh2" border='0' src='lgncode1.php'
onclick="document.getElementById('refresh2').src='lgncode1.php?t='+Math.random()" / class="code-img">
                <button type="submit">登录</button>
                <div class="error"><span>+</span></div>
            </form>
        <!-- Javascript -->
        <script src="assets/js/jquery-1.8.2.min.js"></script>
        <script src="assets/js/supersized.3.2.7.min.js"></script>
        <script src="assets/js/supersized-init.js"></script>
        <script src="assets/js/scripts.js"></script>



</body>
</html>
