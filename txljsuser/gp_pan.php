<?
require_once("../config/dbconn.php");require_once("../config/usercls.php");
if ($glo_closegupiao==1){
	die("系统休市，暂停交易");
}

//获取今天的开盘价格
$kpdate=date('Y-m-d',$curtime);
$sqlkp="select * from {$db_prefix}gupiaokp where kpdate='".$kpdate."'";
$rskp=$db->get_one($sqlkp);
if (!$rskp['id']){
	$sqlkp1="insert into {$db_prefix}gupiaokp(kpdate,kpprice,addtime) values('".$kpdate."','$glo_gpcurprice','$curtime')";
	$db->get_one($sqlkp1);
	$curkpprice=$glo_gpcurprice;
}else{
	$curkpprice=$rskp['kpprice'];
}
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	
}
-->
</style>
<script language="javascript" type="text/javascript" src="../calendar/WdatePicker.js"></script>
<link href="images/skin.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" height="29" valign="top" background="images/mail_leftbg.gif"><img src="images/left-top-right.gif" width="17" height="29" /></td>
    <td width="935" height="29" valign="top" background="images/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt"><strong>基金币大盘/Broader market</strong></div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="images/mail_rightbg.gif"><img src="images/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td height="71" valign="middle" background="images/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9"><table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td height="13" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td valign="top">
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="100%"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="nowtable">
                <tr>
                  <td class="left_bt2">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td height="30">
			  
			  <table width="100%" height="60" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td align="center" bgcolor="#FFFFFF" class="left_txt">开盘价/Opening price</td>
                  <td align="center" bgcolor="#FFFFFF" class="left_txt">最高价/Highest Price</td>
                  <td align="center" bgcolor="#FFFFFF" class="left_txt">最低价/Lowest</td>
                  <td align="center" bgcolor="#FFFFFF" class="left_txt">收盘价/Closing price</td>
                  <td align="center" bgcolor="#FFFFFF" class="left_txt">涨幅/跌幅/Increase / decrease in</td>
                  <td align="center" bgcolor="#FFFFFF" class="left_txt">当前报价/Current quote</td>
                  <td height="30" align="center" bgcolor="#FFFFFF" class="left_txt">成交量/Volume</td>
                </tr>
                <tr>
                  <td align="center" bgcolor="#FFFFFF" class="left_txt"><?=$curkpprice?></td>
                  <td align="center" bgcolor="#FFFFFF" class="left_txt"><?
	  $sqlzg="select * from {$db_prefix}gp_trade where 1 order by price desc limit 1";
	  $rszg=$db->get_one($sqlzg);
	  echo floatval($rszg['price']);
	  ?></td>
                  <td align="center" bgcolor="#FFFFFF" class="left_txt"><?
	  $sqlzd="select * from {$db_prefix}gp_trade where 1 order by price asc limit 1";
	  $rszd=$db->get_one($sqlzd);
	  echo floatval($rszd['price']);
	  ?></td>
                  <td align="center" bgcolor="#FFFFFF" class="left_txt"><?
	  $sqlsp="select * from {$db_prefix}gp_trade where 1 order by id desc limit 1";
	  $rssp=$db->get_one($sqlsp);
	  if ($rssp['id']){
	  echo $spprice=floatval($rssp['price']);
	  }else{
	  echo $spprice=$curkpprice;
	  }
	  ?></td>
                  <td align="center" bgcolor="#FFFFFF" class="left_txt"><? if ($spprice>$curkpprice) echo number_format(($spprice-$curkpprice)*100/$curkpprice,2,'.','');else echo number_format(($curkpprice-$spprice)*100/$curkpprice,2,'.','');?>
                    %</td>
                  <td align="center" bgcolor="#FFFFFF" class="left_txt"><?
	  $sqldq="select * from {$db_prefix}gp_trade where 1 order by id desc limit 1";
	  $rsdq=$db->get_one($sqldq);
	  echo floatval($rsdq['price']);
	  ?></td>
                  <td height="30" align="center" bgcolor="#FFFFFF" class="left_txt"><?
	  //今日成交量
	  $sqlcj="select sum(num) as c from {$db_prefix}gp_trade where 1 and datediff(from_unixtime(addtime),from_unixtime('".time()."'))=0";
	  $rscj=$db->get_one($sqlcj);
	  echo intval($rscj['c']);
	  ?></td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td height="30">&nbsp;</td>
            </tr>
          </table>
		 
		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="100%"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="nowtable">
                  <tr>
                    <td class="left_bt2">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                  </tr>
              </table></td>
            </tr>
            <tr>
              <td height="30"><table width="100%" height="60" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td height="30" align="center" bgcolor="#FFFFFF" class="left_txt">开盘价/Opening price</td>
                    <td align="center" bgcolor="#FFFFFF" class="left_txt">收盘价/Closing price</td>
                    <td align="center" bgcolor="#FFFFFF" class="left_txt">今日涨跌/Today Change($)</td>
                    <td align="center" bgcolor="#FFFFFF" class="left_txt">涨幅/跌幅/Increase / decrease in % </td>
                    </tr>
                  <tr>
                    <td height="30" align="center" bgcolor="#FFFFFF" class="left_txt"><?=$curkpprice?></td>
                    <td align="center" bgcolor="#FFFFFF" class="left_txt"><?
	  echo $spprice;
	  ?></td>
                    <td align="center" bgcolor="#FFFFFF" class="left_txt"><? if ($spprice>$curkpprice) echo number_format(($spprice-$curkpprice),2,'.','')."  ↑";else echo number_format(($curkpprice-$spprice),2,'.','')." ↓";?>%</td>
                    <td align="center" bgcolor="#FFFFFF" class="left_txt"><? if ($spprice>$curkpprice) echo number_format(($spprice-$curkpprice)*100/$curkpprice,2,'.','');else echo number_format(($curkpprice-$spprice)*100/$curkpprice,2,'.','');?>
%</td>
                    </tr>
              </table></td>
            </tr>
            <tr>
              <td height="30">&nbsp;</td>
            </tr>
          </table>
		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="100%"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="nowtable">
                  <tr>
                    <td class="left_bt2">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                  </tr>
              </table></td>
            </tr>
            <tr>
              <td height="30"><table width="100%" height="60" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td height="30" align="center" bgcolor="#FFFFFF" class="left_txt">最新成交价/The latest transaction price</td>
                    <td align="center" bgcolor="#FFFFFF" class="left_txt">最新成交时间/Last traded time</td>
                    <td align="center" bgcolor="#FFFFFF" class="left_txt">最新交易量/Latest trading volume</td>
                    <td align="center" bgcolor="#FFFFFF" class="left_txt">最新涨跌幅度/Latest Change rate(%)</td>
                  </tr>
                  <tr>
                    <td height="30" align="center" bgcolor="#FFFFFF" class="left_txt"><?
	  $sqlnew="select * from {$db_prefix}gp_trade where 1 order by id desc limit 1";
	  $rsnew=$db->get_one($sqlnew);
	  echo $newprice=floatval($rsnew['price']);
	  ?></td>
                    <td align="center" bgcolor="#FFFFFF" class="left_txt"><?
	  if ($rsnew['id']){
	  	echo date("Y-m-d H:i:s",$rsnew['addtime']);
	  }
	  ?></td>
                    <td align="center" bgcolor="#FFFFFF" class="left_txt"><? 
		  //今日成交量
		   $sqlcj="select sum(num) as c from {$db_prefix}gp_trade where 1 and datediff(from_unixtime(addtime),from_unixtime('".time()."'))=0";
	  $rscj=$db->get_one($sqlcj);
	  echo intval($rscj['c']);
		  ?></td>
                    <td align="center" bgcolor="#FFFFFF" class="left_txt"><? if ($newprice>$curkpprice) echo number_format(($newprice-$curkpprice)*100/$curkpprice,2,'.','');else echo number_format(($curkpprice-$newprice)*100/$curkpprice,2,'.','');?>
%</td>
                  </tr>
              </table></td>
            </tr>
            <tr>
              <td height="30">&nbsp;</td>
            </tr>
          </table>
		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="100%"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="nowtable">
                  <tr>
                    <td class="left_bt2">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                  </tr>
              </table></td>
            </tr>
            <tr>
              <td height="30"><table width="100%" height="60" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td height="30" align="center" bgcolor="#FFFFFF" class="left_txt">最新放盘(买)/Latest release plate (buy)</td>
                    <td align="center" bgcolor="#FFFFFF" class="left_txt">开价(买)/Asking price (buy)</td>
                    <td align="center" bgcolor="#FFFFFF" class="left_txt">最新放盘(沽)/Latest release plate (Put)</td>
                    <td align="center" bgcolor="#FFFFFF" class="left_txt">开价(沽)/The offer (Put)</td>
                  </tr>
                  <tr>
                    <td height="30" align="center" bgcolor="#FFFFFF" class="left_txt"><?
	  $sqlnew="select * from {$db_prefix}gp_market where type=1 order by id desc limit 1";
	  $rsnew=$db->get_one($sqlnew);
	  echo intval($rsnew['num1']);
	  ?></td>
                    <td align="center" bgcolor="#FFFFFF" class="left_txt"><?
	  if ($rsnew['id']){
	  	echo floatval($rsnew['price']);
	  }
	  ?></td>
                    <td align="center" bgcolor="#FFFFFF" class="left_txt"><?
	  $sqlnew="select * from {$db_prefix}gp_market where type=2 order by id desc limit 1";
	  $rsnew=$db->get_one($sqlnew);
	  echo intval($rsnew['num1']);
	  ?></td>
                    <td align="center" bgcolor="#FFFFFF" class="left_txt"><?
	  if ($rsnew['id']){
	  	echo floatval($rsnew['price']);
	  }
	  ?></td>
                  </tr>
              </table></td>
            </tr>
            <tr>
              <td height="30">&nbsp;</td>
            </tr>
          </table>
		  <TABLE width="80%" border=0 align="center" cellPadding=0 cellSpacing=0 class=Table_xt>
            <TBODY>
              <TR></TR>
              <TR>
                <TD height="20" align="left" bgColor="#d4e8fa" ><table width="100%" border="0" cellpadding="3" cellspacing="1">
                    <TR>
                      <TD width="25%" height="20" align="center" valign="middle" bgColor="#FF0000" >今日最高价/Today's highest price：</TD>
                      <TD width="25%" align="center" valign="middle" bgColor="#FBFDFF" ><?
	  $sqlzg="select * from {$db_prefix}gp_market where datediff(from_unixtime(addtime),from_unixtime('".time()."'))=0 order by price desc limit 1";
	  $rszg=$db->get_one($sqlzg);
	  echo floatval($rszg['price']);
	  ?></TD>
                      <TD width="25%" align="center" valign="middle" bgColor="#0099FF" >今日最低价/Today, the lowest：</TD>
                      <TD width="25%" align="center" valign="middle" bgColor="#FBFDFF" ><?
	  $sqlzg="select * from {$db_prefix}gp_market where datediff(from_unixtime(addtime),from_unixtime('".time()."'))=0 order by price asc limit 1";
	  $rszg=$db->get_one($sqlzg);
	  echo floatval($rszg['price']);
	  ?></TD>
                    </TR>
                </table></TD>
              </TR>
                    </TABLE>
		  <p>&nbsp;</p></td>
      </tr>
    </table></td>
    <td background="images/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="middle" background="images/mail_leftbg.gif"><img src="images/buttom_left2.gif" width="17" height="17" /></td>
      <td height="17" valign="top" background="images/buttom_bgs.gif"><img src="images/buttom_bgs.gif" width="17" height="17" /></td>
    <td background="images/mail_rightbg.gif"><img src="images/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>

</body>
</html>