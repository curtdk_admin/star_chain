<?
set_time_limit(1000);
require_once("../config/dbconn.php");require_once("../config/usercls.php");require_once("../config/pagecls.php");require_once("../config/pagecls1.php");require_once("pwd1cls.php");

if ($glo_closegupiao==1){
	die("系统休市，暂停交易");
}

//获取今天的开盘价格
$kpdate=date('Y-m-d',$curtime);
$sqlkp="select * from {$db_prefix}gupiaokp where kpdate='".$kpdate."'";
$rskp=$db->get_one($sqlkp);
if (!$rskp['id']){
	$sqlkp1="insert into {$db_prefix}gupiaokp(kpdate,kpprice,addtime) values('".$kpdate."','$glo_gpcurprice','$curtime')";
	$db->get_one($sqlkp1);
	$curkpprice=$glo_gpcurprice;
}else{
	$curkpprice=$rskp['kpprice'];
}

if ($action=='trade'){
	if ($glo_closehygptrade==1){
		die("会员之间的交易已经关闭");
	}
	$hint='';
	if(!is_numeric($price)) $hint.="交易股价格不是数字\\n";
	if(intval($num)<0) $hint.="交易股数量错误\\n";
	if(intval($num)!=floatval($num)) $hint.="交易股数量不是整数\\n";
	else{
		if (intval($num)>$glo_lcbmax_1){
			$hint.="每次最多交易{$glo_lcbmax_1}股\\n";
		}
	}
	//if(trim($jztime)=='') $hint.="请输入放盘截止日期\\n";
	if($hint!=''){
		echo "<script>alert('$hint');history.back();</script>";exit();
	}
	
	//出价不得低于或高于开盘价的
	if (floatval($price)>$curkpprice){
		$gcrate=(floatval($price)-$curkpprice)/$curkpprice;
		if($gcrate>$glo_gp_pricelimit/100){
			$hint="出价不得高于开盘价的{$glo_gp_pricelimit}%";
			echo "<script>alert('$hint');history.back();</script>";exit();
		}
	}else{
		$gcrate=($curkpprice-floatval($price))/$curkpprice;
		if($gcrate>$glo_gp_pricelimit/100){
			$hint="出价不得低于开盘价的{$glo_gp_pricelimit}%";
			echo "<script>alert('$hint');history.back();</script>";exit();
		}
	}
	
	//最低交易量
	if (intval($num)<$glo_gp_lowertrade){
		$hint="最低交易量{$glo_gp_lowertrade}股";
		echo "<script>alert('$hint');history.back();</script>";exit();
	}
	
	if ($type==1){
		//会员账户中是否有足够的报单币
		$sqlhy="select * from {$db_prefix}users where username='".$_SESSION["sys_username"]."'";
		$rshy=$db->get_one($sqlhy);
		$curtradeprice=intval($num)*floatval($price);
		if ($rshy['gpprice']<$curtradeprice){
			$hint="报单币的余额不足。本次交易需要{$curtradeprice}美金";
			echo "<script>alert('$hint');history.back();</script>";exit();
		}
		//扣除会员的报单币金额
		$sqlq="update {$db_prefix}users set gpprice=gpprice-'$curtradeprice' where username='".$_SESSION["sys_username"]."'";
		$db->query($sqlq);
		//插入到电子政务记录中 注册人的电子政务
		$e_userid=$_SESSION["sys_userid"];$e_price=-$curtradeprice;$e_type=1;$e_ptype=4;$e_addtime=$curtime;$e_memo="买入报单币";
		hyepricejl($e_userid,$e_price,$e_type,$e_ptype,$e_addtime,$e_memo);
	}
	
	if ($type==2){
		//会员账户中是否有足够的报单币
		$sqlhy="select * from {$db_prefix}users where username='".$_SESSION["sys_username"]."'";
		$rshy=$db->get_one($sqlhy);
		$curtradenum=intval($num);
		if ($rshy['gpnum']<$curtradenum){
			$hint="报单币的数量不足。本次交易需要{$curtradenum}股";
			echo "<script>alert('$hint');history.back();</script>";exit();
		}
		//一天只可以卖出20%的可交易股
		$sqljt="select sum(num1) as c from {$db_prefix}gp_market where username='".$_SESSION["sys_username"]."' and datediff(from_unixtime(addtime),from_unixtime('".$curtime."'))=0 and type=2";
		$rsjt=$db->get_one($sqljt);
		$jttradenum=intval($rsjt['c']);
		
		if ((($jttradenum+intval($num))/($rshy['gpnum']+$jttradenum))>($glo_gp_cantraderate/100)){
			$hint="会员每天只可卖出{$glo_gp_cantraderate}%的可交易股";
			echo "<script>alert('$hint');history.back();</script>";exit();
		}
		
		//扣除会员的原始股量
		$sqlq="update {$db_prefix}users set gpnum=gpnum-'$curtradenum' where username='".$_SESSION["sys_username"]."'";
		$db->query($sqlq);
	}
	
	//插入交易记录中
	$sql="insert into {$db_prefix}gp_market(username,num,num1,price,jztime,type,addtime,state) values('".$_SESSION["sys_username"]."','".intval($num)."','".intval($num)."','".floatval($price)."','".strtotime($jztime)."','$type','".$curtime."',0)";
	$db->query($sql);
	
	$curmarketid=$db->insert_id();
	
	//即时交易开始
	$curtradenum1=0;
	
	if($type==1) require('gptrading.php');
	if($type==2) require('gptrading1.php');
	
	if ($curtradenum1>0){
		//更新原始股的数量
		$sqlsys="update {$db_prefix}gupiaoset set gpsaleoutnum1=gpsaleoutnum1+'".intval($curtradenum1)."' where 1";
		$db->query($sqlsys);
	}
	//看股票已经卖出多少了
	$curgpsaleoutnum1=$glo_gpsaleoutnum1+intval($curtradenum1);
	if ($curgpsaleoutnum1>=$glo_gp_scgupiaonum){
		$gupiaoscnum=floor($curgpsaleoutnum1/$glo_gp_scgupiaonum);
		$gpcurpriceup=$glo_gp_kpprice+$gupiaoscnum*0.0001;
		if ($gpcurpriceup>$glo_gpcurprice){
			//股票价格上涨了
			$sqlgx="update {$db_prefix}gupiaoset set gpcurprice='".$gpcurpriceup."' where 1";
			$db->query($sqlgx);
		}
	}
	
	//自动拆股
	if ($gpcurpriceup>=$glo_gp_kpprice*2){
		//更新价格和报单币数量
		$sqlgx1="update {$db_prefix}gupiaoset set gpcurprice='$glo_gp_kpprice',chaigunum=chaigunum+1,gpsaleoutnum1=0 where 1";
		$db->query($sqlgx1);
		//更新会员的报单币数量
		$sqlgx1="update {$db_prefix}users set gpnum=gpnum*2 where 1";
		$db->query($sqlgx1);
		$sqlgx1="update {$db_prefix}gp_trade set chaigunum=chaigunum+1 where salenum<3";
		$db->query($sqlgx1);
		$sqlgx1="update {$db_prefix}gp_tradeysg set chaigunum=chaigunum+1 where salenum<3";
		$db->query($sqlgx1);
	}else{
			//看是否可以有卖出的股票
			$autosalenum=count($gupiaoautosaleary);
			$curtradeallnum=0;
			foreach($gupiaoautosaleary as $mk1=>$mv1){	
				$sqlm="select * from {$db_prefix}gp_tradeysg where salenum=({$mk1}-1)";
				$resultm=$db->query($sqlm);
				while($rsm=$db->fetch_array($resultm)){
					$mgpsalebei=1;$chaigunum=0;
					$mgpsaleprice=$rsm['price'];
					if (($rsm['price']*$mv1)>=($glo_gp_kpprice*2)){
						$chaigunum+=1;
						if (($rsm['price']*$mv1-$glo_gp_kpprice*2)>=($glo_gp_kpprice*2)){
							$chaigunum+=1;
						}
					}
					$mgpsalebei=pow(2,$chaigunum);
					$mgpsaleprice=$mgpsaleprice/$chaigunum;
					if ($mgpsaleprice!=$gpcurpriceup){
						continue;
					}
					$price=$mgpsaleprice;
					$cursalenum=floor($rsm['num']/$autosalenum)*$mgpsalebei;
					//股票里边是否还有这么多的股票
					$sqlhy="select id,gpnum from {$db_prefix}users where username='".$rsm['username']."'";
					$rshy=$db->get_one($sqlhy);
					if ($rshy['gpnum']>=$cursalenum){
						//自动卖出股票
						$num=$cursalenum;
						$saleouthy=$rsm['username'];$saleouthyid=$rsm['id'];
						
						$sqlsaleout="insert into {$db_prefix}gp_market(username,num,num1,price,jztime,type,addtime,state) values('".$saleouthy."','".intval($num)."','".intval($num)."','".floatval($price)."','0','2','".$curtime."',0)";
						
						$db->query($sqlsaleout);
						$curmarketid=$db->insert_id();
						require("gptrading2.php");
						//自动卖出股票
						$sqlgx1="update {$db_prefix}gp_tradeysg set salenum=salenum+1 where id='".$rsm['id']."'";
						$db->query($sqlgx1);
					}
				}
				$db->free_result($resultm);
				
				//////////////////////////////////////////
				$sqlm="select * from {$db_prefix}gp_trade where salenum=({$mk1}-1)";
				$resultm=$db->query($sqlm);
				while($rsm=$db->fetch_array($resultm)){
					$mgpsalebei=1;$chaigunum=0;
					$mgpsaleprice=$rsm['price'];
					if (($rsm['price']*$mv1)>=($glo_gp_kpprice*2)){
						$chaigunum+=1;
						if (($rsm['price']*$mv1-$glo_gp_kpprice*2)>=($glo_gp_kpprice*2)){
							$chaigunum+=1;
						}
					}
					$mgpsalebei=pow(2,$chaigunum);
					$mgpsaleprice=$mgpsaleprice/$chaigunum;
					if ($mgpsaleprice!=$gpcurpriceup){
						continue;
					}
					$price=$mgpsaleprice;
					$cursalenum=floor($rsm['num']/$autosalenum)*$mgpsalebei;
					//股票里边是否还有这么多的股票
					$sqlhy="select id,gpnum from {$db_prefix}users where username='".$rsm['username']."'";
					$rshy=$db->get_one($sqlhy);
					if ($rshy['gpnum']>=$cursalenum){
						//自动卖出股票
						$num=$cursalenum;
						$saleouthy=$rsm['username'];$saleouthyid=$rsm['id'];
						
						$sqlsaleout="insert into {$db_prefix}gp_market(username,num,num1,price,jztime,type,addtime,state) values('".$saleouthy."','".intval($num)."','".intval($num)."','".floatval($price)."','0','2','".$curtime."',0)";
						
						$db->query($sqlsaleout);
						$curmarketid=$db->insert_id();
						require("gptrading2.php");
						//自动卖出股票
						$sqlgx1="update {$db_prefix}gp_trade set salenum=salenum+1 where id='".$rsm['id']."'";
						$db->query($sqlgx1);
					}
				}
				$db->free_result($resultm);
			}
			
			//看股票的价格是否又要上涨了
			if($curtradeallnum>0){
				$sqlsys1="update {$db_prefix}gupiaoset set gpsaleoutnum1=gpsaleoutnum1+'".intval($curtradeallnum)."' where 1";
				$db->query($sqlsys1);
				autosaleoutgpfunction($curgpsaleoutnum1,$curtradeallnum,$gpcurpriceup);
			}	
	}
	
	
	
	
	echo "<script>alert('交易委托中');location.href='gp_trade.php';</script>";exit();
}

////////////////////////////////////////////////////////////////////////////////////////////
if($action=='buy'){
	$sqlhy="select * from {$db_prefix}users where username='".$_SESSION["sys_username"]."'";
	$rshy=$db->get_one($sqlhy);
	$hint='';
	if(intval($num)<=0) $hint.="购买数量错误\\n";
	if(intval($num)!=floatval($num)) $hint.="购买数量不是整数\\n";
	else{
		if(intval($num)>$glo_lcbmax_1){
			$hint.="每次做多交易{$glo_lcbmax_1}股\\n";
		}
	}
	if($hint!=''){
		echo "<script>alert('$hint');history.back();</script>";exit();
	}
	
	//会员账户中是否有足够的报单币	
	$curtradeprice=intval($num)*$price;
	if ($rshy['gpprice']<$curtradeprice){
		$hint="报单币的余额不足。本次交易需要{$curtradeprice}美金";
		echo "<script>alert('$hint');history.back();</script>";exit();
	}
	
	//查看原始股数量是否足够
	if ($glo_gpysgnum<intval($num)){
		$hint="原始股数量不足{$num}";
		echo "<script>alert('$hint');history.back();</script>";exit();
	}
	
	//扣除会员的报单币金额
	$sqlq="update {$db_prefix}users set gpnum=gpnum+'".intval($num)."',gpprice=gpprice-'$curtradeprice' where username='".$_SESSION["sys_username"]."'";
	$db->query($sqlq);
	//插入到电子政务记录中 注册人的电子政务
	$e_userid=$_SESSION["sys_userid"];$e_price=-$curtradeprice;$e_type=1;$e_ptype=4;$e_addtime=$curtime;$e_memo="买入原始报单币";
	hyepricejl($e_userid,$e_price,$e_type,$e_ptype,$e_addtime,$e_memo);
	
	//插入原始股购买记录
	$sqljl="insert into {$db_prefix}gp_tradeysg(username,num,price,addtime) values('".$_SESSION["sys_username"]."','".intval($num)."','".$price."','".$modtime."')";
	$db->query($sqljl);
	
	//更新原始股的数量
	$sqlsys="update {$db_prefix}gupiaoset set gpysgnum=gpysgnum-'".intval($num)."',gpsaleoutnum=gpsaleoutnum+'".intval($num)."',gpsaleoutnum1=gpsaleoutnum1+'".intval($num)."' where 1";
	$db->query($sqlsys);
	
	//看股票已经卖出多少了
	$curgpsaleoutnum1=$glo_gpsaleoutsy+intval($num);
	if ($curgpsaleoutnum1>=$glo_gp_scgupiaonum){
		$gupiaoscnum=floor($curgpsaleoutnum1/$glo_gp_scgupiaonum);
		$gpcurpriceup=$glo_gpcurprice+$gupiaoscnum*0.0001;
		$gpsaleoutsycur=$curgpsaleoutnum1%$glo_gp_scgupiaonum;
		//看股票已经卖出多少了
		if ($gpcurpriceup>$glo_gpcurprice){
			//股票价格上涨了
			$sqlgx="update {$db_prefix}gupiaoset set gpcurprice='".$gpcurpriceup."',gpsaleoutsy='$gpsaleoutsycur' where 1";
			$db->query($sqlgx);
		}
	}
	
	//自动拆股
	if ($gpcurpriceup>=$glo_gp_kpprice*2){
		//更新价格和报单币数量
		$sqlgx1="update {$db_prefix}gupiaoset set gpcurprice='$glo_gp_kpprice',chaigunum=chaigunum+1,gpsaleoutnum1=0 where 1";
		$db->query($sqlgx1);
		//更新会员的报单币数量
		$sqlgx1="update {$db_prefix}users set gpnum=gpnum*2 where 1";
		$db->query($sqlgx1);
		//更新会员的报单币数量
		$sqlgx1="update {$db_prefix}gp_trade set chaigunum=chaigunum+1 where salenum<3";
		$db->query($sqlgx1);
		$sqlgx1="update {$db_prefix}gp_tradeysg set chaigunum=chaigunum+1 where salenum<3";
		$db->query($sqlgx1);
	}else{
			//看是否可以有卖出的股票
			$autosalenum=count($gupiaoautosaleary);
			
			$curtradeallnum=0;
			foreach($gupiaoautosaleary as $mk1=>$mv1){
				$sqlm="select * from {$db_prefix}gp_tradeysg where salenum=({$mk1}-1)";
				$resultm=$db->query($sqlm);
				while($rsm=$db->fetch_array($resultm)){
					$mgpsalebei=1;$chaigunum=0;
					$mgpsaleprice=$rsm['price'];
					if (($rsm['price']*$mv1)>=($glo_gp_kpprice*2)){
						$chaigunum+=1;
						if (($rsm['price']*$mv1-$glo_gp_kpprice*2)>=($glo_gp_kpprice*2)){
							$chaigunum+=1;
						}
					}
					$mgpsalebei=pow(2,$chaigunum);
					$mgpsaleprice=$mgpsaleprice/$chaigunum;
					if ($mgpsaleprice!=$gpcurpriceup){
						continue;
					}
					$price=$mgpsaleprice;
					$cursalenum=floor($rsm['num']/$autosalenum)*$mgpsalebei;
					//股票里边是否还有这么多的股票
					$sqlhy="select id,gpnum from {$db_prefix}users where username='".$rsm['username']."'";
					$rshy=$db->get_one($sqlhy);
					if ($rshy['gpnum']>=$cursalenum){
						//自动卖出股票
						$num=$cursalenum;
						$saleouthy=$rsm['username'];$saleouthyid=$rsm['id'];
						//插入交易记录中
						$sqlsaleout="insert into {$db_prefix}gp_market(username,num,num1,price,jztime,type,addtime,state) values('".$saleouthy."','".intval($num)."','".intval($num)."','".floatval($price)."','0','2','".$curtime."',0)";
						$db->query($sqlsaleout);
						$curmarketid=$db->insert_id();
						require("gptrading2.php");
						//自动卖出股票
						$sqlgx1="update {$db_prefix}gp_tradeysg set salenum=salenum+1 where id='".$rsm['id']."'";
						$db->query($sqlgx1);
					}
				}
				$db->free_result($resultm);
				
				//////////////////////////////////////////
				$sqlm="select * from {$db_prefix}gp_trade where salenum=({$mk1}-1)";
				$resultm=$db->query($sqlm);
				while($rsm=$db->fetch_array($resultm)){
					$mgpsalebei=1;$chaigunum=0;
					$mgpsaleprice=$rsm['price'];
					if (($rsm['price']*$mv1)>=($glo_gp_kpprice*2)){
						$chaigunum+=1;
						if (($rsm['price']*$mv1-$glo_gp_kpprice*2)>=($glo_gp_kpprice*2)){
							$chaigunum+=1;
						}
					}
					$mgpsalebei=pow(2,$chaigunum);
					$mgpsaleprice=$mgpsaleprice/$chaigunum;
					if ($mgpsaleprice!=$gpcurpriceup){
						continue;
					}
					$price=$mgpsaleprice;
					$cursalenum=floor($rsm['num']/$autosalenum)*$mgpsalebei;
					///////////////////////////
					//股票里边是否还有这么多的股票
					if ($rsm['type']==1){
					$sqlhy="select id,gpnum from {$db_prefix}users where username='".$rsm['username']."'";
					}else{
					$sqlhy="select id,gpnum from {$db_prefix}users where username='".$rsm['username1']."'";
					}
					$rshy=$db->get_one($sqlhy);
					if ($rshy['gpnum']>=$cursalenum){
						//自动卖出股票
						$num=$cursalenum;
						if ($rsm['type']==1){
						$saleouthy=$rsm['username'];
						}else{
						$saleouthy=$rsm['username1'];
						}
						$saleouthyid=$rsm['id'];
						
						//插入交易记录中
						$sqlsaleout="insert into {$db_prefix}gp_market(username,num,num1,price,jztime,type,addtime,state) values('".$saleouthy."','".intval($num)."','".intval($num)."','".floatval($price)."','0','2','".$curtime."',0)";
						$db->query($sqlsaleout);
						$curmarketid=$db->insert_id();
						require("gptrading2.php");
						//自动卖出股票
						$sqlgx1="update {$db_prefix}gp_trade set salenum=salenum+1 where id='".$rsm['id']."'";
						$db->query($sqlgx1);
					}
				}
				$db->free_result($resultm);
			}
			
			//看股票的价格是否又要上涨了
			if($curtradeallnum>0){
				$sqlsys1="update {$db_prefix}gupiaoset set gpsaleoutnum1=gpsaleoutnum1+'".intval($curtradeallnum)."' where 1";
				$db->query($sqlsys1);
				autosaleoutgpfunction($curgpsaleoutnum1,$curtradeallnum,$gpcurpriceup);
			}	
	}
	
	echo "<script>alert('交易委托中');location.href='gp_trade.php';</script>";exit();
}

//自动卖出的函数。循环卖出啊
function autosaleoutgpfunction($allgpsaleoutnum1,$num,$glogpcurprice){
	global $db,$db_prefix,$glo_gp_scgupiaonum,$glo_gp_kpprice,$gupiaoautosaleary,$curtime;
	//看股票已经卖出多少了
	$curgpsaleoutnum2=$allgpsaleoutnum1+intval($num);
	if ($curgpsaleoutnum2>=$glo_gp_scgupiaonum){
		$gupiaoscnum=floor($curgpsaleoutnum2/$glo_gp_scgupiaonum);
		$gpcurpriceup=$glo_gp_kpprice+$gupiaoscnum*0.0001;
		if ($gpcurpriceup>$glogpcurprice){
			//股票价格上涨了
			$sqlgx="update {$db_prefix}gupiaoset set gpcurprice='".$gpcurpriceup."' where 1";
			$db->query($sqlgx);
			//看是否可以有卖出的股票
			$autosalenum=count($gupiaoautosaleary);
			
			$curtradeallnum=0;
			foreach($gupiaoautosaleary as $mk1=>$mv1){
				$sqlm="select * from {$db_prefix}gp_tradeysg where salenum=({$mk1}-1)";
				$resultm=$db->query($sqlm);
				while($rsm=$db->fetch_array($resultm)){
					$mgpsalebei=1;$chaigunum=0;
					$mgpsaleprice=$rsm['price'];
					if (($rsm['price']*$mv1)>=($glo_gp_kpprice*2)){
						$chaigunum+=1;
						if (($rsm['price']*$mv1-$glo_gp_kpprice*2)>=($glo_gp_kpprice*2)){
							$chaigunum+=1;
						}
					}
					$mgpsalebei=pow(2,$chaigunum);
					$mgpsaleprice=$mgpsaleprice/$chaigunum;
					if ($mgpsaleprice!=$gpcurpriceup){
						continue;
					}
					$price=$mgpsaleprice;
					$cursalenum=floor($rsm['num']/$autosalenum)*$mgpsalebei;
					//股票里边是否还有这么多的股票
					$sqlhy="select id,gpnum from {$db_prefix}users where username='".$rsm['username']."'";
					$rshy=$db->get_one($sqlhy);
					if ($rshy['gpnum']>=$cursalenum){
						//自动卖出股票
						$num=$cursalenum;
						$saleouthy=$rsm['username'];$saleouthyid=$rsm['id'];
						//插入交易记录中
						$sqlsaleout="insert into {$db_prefix}gp_market(username,num,num1,price,jztime,type,addtime,state) values('".$saleouthy."','".intval($num)."','".intval($num)."','".floatval($price)."','0','2','".$curtime."',0)";
						$db->query($sqlsaleout);
						$curmarketid=$db->insert_id();
						require("gptrading2.php");
						//自动卖出股票
						$sqlgx1="update {$db_prefix}gp_tradeysg set salenum=salenum+1 where id='".$rsm['id']."'";
						$db->query($sqlgx1);
					}
				}
				$db->free_result($resultm);
				//////////////////////////////////////////
				$sqlm="select * from {$db_prefix}gp_trade where salenum=({$mk1}-1)";
				$resultm=$db->query($sqlm);
				while($rsm=$db->fetch_array($resultm)){
					$mgpsalebei=1;$chaigunum=0;
					$mgpsaleprice=$rsm['price'];
					if (($rsm['price']*$mv1)>=($glo_gp_kpprice*2)){
						$chaigunum+=1;
						if (($rsm['price']*$mv1-$glo_gp_kpprice*2)>=($glo_gp_kpprice*2)){
							$chaigunum+=1;
						}
					}
					$mgpsalebei=pow(2,$chaigunum);
					$mgpsaleprice=$mgpsaleprice/$chaigunum;
					if ($mgpsaleprice!=$gpcurpriceup){
						continue;
					}
					$price=$mgpsaleprice;
					$cursalenum=floor($rsm['num']/$autosalenum)*$mgpsalebei;
					//股票里边是否还有这么多的股票
					if ($rsm['type']==1){
					$sqlhy="select id,gpnum from {$db_prefix}users where username='".$rsm['username']."'";
					}else{
					$sqlhy="select id,gpnum from {$db_prefix}users where username='".$rsm['username1']."'";
					}
					$rshy=$db->get_one($sqlhy);
					if ($rshy['gpnum']>=$cursalenum){
						//自动卖出股票
						$num=$cursalenum;
						if ($rsm['type']==1){
						$saleouthy=$rsm['username'];
						}else{
						$saleouthy=$rsm['username1'];
						}
						$saleouthyid=$rsm['id'];
						$sqlsaleout="insert into {$db_prefix}gp_market(username,num,num1,price,jztime,type,addtime,state) values('".$saleouthy."','".intval($num)."','".intval($num)."','".floatval($price)."','0','2','".$curtime."',0)";
						$db->query($sqlsaleout);
						$curmarketid=$db->insert_id();
						require("gptrading2.php");
						//自动卖出股票
						$sqlgx1="update {$db_prefix}gp_trade set salenum=salenum+1 where id='".$rsm['id']."'";
						$db->query($sqlgx1);
					}
				}
				$db->free_result($resultm);
			}
			
			//看股票的价格是否又要上涨了
			if($curtradeallnum>0){
				$sqlsys1="update {$db_prefix}gupiaoset set gpsaleoutnum1=gpsaleoutnum1+'".intval($curtradeallnum)."' where 1";
				$db->query($sqlsys1);
				autosaleoutgpfunction($allgpsaleoutnum2,$curtradeallnum,$gpcurpriceup);
			}
		}
	}
}
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script language="javascript" type="text/javascript" src="../calendar/WdatePicker.js"></script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	
}
td{
font-size:12px;
}
-->
</style>
<script language="javascript">
function gppricedo(tt){
	var gpnum=document.getElementById('num').value;
	var gpprice=document.getElementById('price').value;
	document.getElementById('allgpprice').innerHTML=parseInt(gpnum)*parseFloat(gpprice);
}
function gppricedo1(tt){
	var gpnum=document.form1.num.value;
	var gpprice=document.form1.price.value;
	document.getElementById('allgpprice1').innerHTML=parseInt(gpnum)*parseFloat(gpprice);
}
</script>
<link href="images/skin.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
.STYLE1 {font-size: 12px}
-->
</style>
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" height="29" valign="top" background="images/mail_leftbg.gif"><img src="images/left-top-right.gif" width="17" height="29" /></td>
    <td width="935" height="29" valign="top" background="images/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt"><span class="STYLE1">报单币交易</span></div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="images/mail_rightbg.gif"><img src="images/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td height="71" valign="middle" background="images/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9"><table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td height="13" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="nowtable">
                <tr>
                  <td >&nbsp;&nbsp;&nbsp;&nbsp;<strong>当日开盘参照</strong></td>
                </tr>
            </table></td>
          </tr>
          <tr>
            <td height="30"><table width="100%" border="0" cellpadding="3" cellspacing="1">
              <TR>
                <TD height="30" align="center"  >开盘价</TD>
                <TD height="30" align="center"  >最高价</TD>
                <TD height="30" align="center"  >最低价</TD>
                <TD height="30" align="center"  >收盘价</TD>
                <TD height="30" align="center"  >涨幅/跌幅</TD>
                <TD height="30" align="center"  >当前报价</TD>
                <TD height="30" align="center"  >成交量</TD>
              </TR>
              <TR>
                <TD width="10%" height="30" align="center"  ><?=$curkpprice?></TD>
                <TD width="13%" height="30" align="center"  ><?
	  $sqlzg="select * from {$db_prefix}gp_trade where 1 order by price desc limit 1";
	  $rszg=$db->get_one($sqlzg);
	  echo floatval($rszg['price']);
	  ?>                </TD>
                <TD width="14%" height="30" align="center"  ><?
	  $sqlzd="select * from {$db_prefix}gp_trade where 1 order by price asc limit 1";
	  $rszd=$db->get_one($sqlzd);
	  echo floatval($rszd['price']);
	  ?></TD>
                <TD width="12%" height="30" align="center"  ><?
	  $sqlsp="select * from {$db_prefix}gp_trade where 1 order by id desc limit 1";
	  $rssp=$db->get_one($sqlsp);
	  if ($rssp){
	  echo $spprice=floatval($rssp['price']);
	  }else{
	  echo $spprice=$glo_gpcurprice;
	  }
	  ?></TD>
                <TD width="15%" height="30" align="center"  ><? if ($spprice>$curkpprice) echo number_format(($spprice-$curkpprice)*100/$curkpprice,2,'.','');else echo number_format(($curkpprice-$spprice)*100/$curkpprice,2,'.','');?>
                  %</TD>
                <TD width="18%" height="30" align="center"  ><?
	  echo $glo_gpcurprice;
	  ?></TD>
                <TD width="18%" height="30" align="center"  ><?
	  //今日成交量
	  $sqlcj="select sum(num) as c from {$db_prefix}gp_trade where 1 and datediff(from_unixtime(addtime),from_unixtime('".time()."'))=0";
	  $rscj=$db->get_one($sqlcj);
	  echo intval($rscj['c']);
	  ?>                </TD>
              </TR>
            </table></td>
          </tr>

          <tr>
            <td height="30">&nbsp;</td>
          </tr>
        </table>
		<?
$sqlhy="select * from {$db_prefix}users where username='".$_SESSION["sys_username"]."'";
$rshy=$db->get_one($sqlhy);

?>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="100%"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="nowtable">
                  <tr>
                    <td >&nbsp;&nbsp;&nbsp;&nbsp;账户信息</td>
                  </tr>
              </table></td>
            </tr>
            <tr>
              <td height="30"><table width="100%" border="0" cellpadding="3" cellspacing="1">
                <TR>
                  <TD width="10%" height="30" align="center"  >报单币静值：
                      <?=$rshy['gp']*$glo_gp_price?></TD>
                  <TD width="13%" height="30" align="center"  >报单币售出量：
                    <?
	  //售出量
	  $sqlout="select sum(num) as c from {$db_prefix}gp_trade where 1 and ((username='".$_SESSION["sys_username"]."' and type=2) or (username1='".$_SESSION["sys_username"]."' and type=1))";
	  $rsout=$db->get_one($sqlout);
	  echo intval($rsout['c']);
	  ?>                  </TD>
                </TR>
                <TR>
                  <TD height="30" align="center"  >报单币交易帐户持股量：
                      <?=$rshy['gpnum']?>                    </TD>
                  <TD height="30" align="center"  >报单币买入股量：
                      <?
	  //买入量
	  $sqlin="select sum(num) as c from {$db_prefix}gp_trade where 1 and ((username='".$_SESSION["sys_username"]."' and type=1) or (username1='".$_SESSION["sys_username"]."' and type=2))";
	  $rsin=$db->get_one($sqlin);
	  echo intval($rsin['c']);
	  ?></TD>
                </TR>
                <TR>
                  <TD height="30" align="center"  >挂入股市中报单币：
                      <?
	  //挂入量
	  $sqlg="select sum(num) as c from {$db_prefix}gp_market where state=0 and username='".$_SESSION["sys_username"]."'";
	  $rsg=$db->get_one($sqlg);
	  echo intval($rsg['c']);
	  ?></TD>
                  <TD height="30" align="center"  >报单币余额：
                    <?=$rshy['gpprice']?></TD>
                </TR>

              </table></td>
            </tr>
            <tr>
              <td height="30">&nbsp;</td>
            </tr>
          </table>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="100%"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="nowtable">
                  <tr>
                    <td width="48%" align="center" ><strong>&nbsp;&nbsp;&nbsp;&nbsp;卖盘</strong></td>
                    <td width="52%" align="center" ><strong>买盘</strong></td>
                  </tr>
              </table></td>
            </tr>
            <tr>
              <td height="30"><table width="100%" border="0" cellpadding="3" cellspacing="1">
                  <TR>
                    <TD width="10%" align="center" valign="top"  ><table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#e1e5ee">
					<tbody bgcolor="#FFFFFF">
                      <tr>
                        <td>卖家</td>
                        <td>数量</td>
                        <td>价格</td>
                        <td>总值</td>
                      </tr>
					  <?
					  $i=0;
					  $query='';
					  $sql="select count(id) as c  from {$db_prefix}gp_market where state=0 and type=2 and num>0";
					  if ($query!='') $sql.=$query;
					  $rs=$db->get_one($sql);
					  $page=new pagecls(10,intval($rs['c']));
					  $sql="select * from {$db_prefix}gp_market where state=0 and type=2 and num>0";
					  if ($query!='') $sql.=$query;
					  $sql.=" order by price asc limit {$page->pastnum},{$page->pagesize}";
					  $result=$db->query($sql);
					  while($rs=$db->fetch_array($result)){
					  ?>
                      <tr>
                        <td><?=$rs['username']?></td>
                        <td><?=$rs['num']?></td>
                        <td><?=$rs['price']?></td>
                        <td><?=($rs['price']*$rs['num'])?></td>
                      </tr>
					  <?
					  	$i++;
					  }
					  $db->free_result($result);
					  ?>
					  <?
					  for($j=$i+1;$j<=10;$j++){
					  	?>
						 <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>&nbsp;</td>
                      </tr>
						<?
					  }
					  ?>
					   <tr>
                        <td colspan="4"> 第
                          <?=$page->pageno?>/<?=$page->pagenum?>页 共<?=$page->recnum?>条 <a href='?<?=$page->url?>1'><img src="image1/first.gif" border="0"></a>   <a href='?<?=$page->url.($page->pageno-1)?>'><img src="image1/back.gif" border="0"></a>  <a href='?<?=$page->url.($page->pageno+1)?>'><img src="image1/next.gif" border="0"></a>   <a href='?<?=$page->url.$page->pagenum?>'><img src="image1/last.gif" border="0"></a></td>
                        </tr>
					  </tbody>
                    </table></TD>
                    <TD width="13%" align="center" valign="top"  ><table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#e1e5ee">
                      <tbody bgcolor="#FFFFFF">
                        <tr>
                          <td>买家</td>
                          <td>数量</td>
                          <td>价格</td>
                          <td>总值</td>
                        </tr>
						
						  <?
						  $i=0;
					  $query='';
					  $sql="select count(id) as c  from {$db_prefix}gp_market where state=0 and type=1 and num>0";
					  if ($query!='') $sql.=$query;
					  $rs=$db->get_one($sql);
					  $page1=new pagecls1(10,intval($rs['c']));
					  $sql="select * from {$db_prefix}gp_market where state=0 and type=1 and num>0";
					  if ($query!='') $sql.=$query;
					  $sql.=" order by price desc limit {$page1->pastnum},{$page1->pagesize}";
					  $result=$db->query($sql);
					  while($rs=$db->fetch_array($result)){
					  ?>
                       <tr>
                        <td><?=$rs['username']?></td>
                        <td><?=$rs['num']?></td>
                        <td><?=$rs['price']?></td>
                        <td><?=($rs['price']*$rs['num'])?></td>
                      </tr>
					  <?
					  	$i++;
					  }
					  $db->free_result($result);
					  ?>
					   <?
					  for($j=$i+1;$j<=10;$j++){
					  	?>
						 <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>&nbsp;</td>
                      </tr>
						<?
					  }
					  ?>
					   <tr>
                        <td colspan="4"> 第
                          <?=$page1->pageno?>/<?=$page1->pagenum?>页 共<?=$page1->recnum?>条 <a href='?<?=$page1->url?>1'><img src="image1/first.gif" border="0"></a>   <a href='?<?=$page1->url.($page1->pageno-1)?>'><img src="image1/back.gif" border="0"></a>  <a href='?<?=$page1->url.($page1->pageno+1)?>'><img src="image1/next.gif" border="0"></a>   <a href='?<?=$page1->url.$page1->pagenum?>'><img src="image1/last.gif" border="0"></a></td>
                        </tr>
                      </tbody>
                    </table></TD>
                  </TR>
                  
              </table></td>
            </tr>
          </table>
         <br>
		 <?
		 if ($glo_closehygptrade==0){
		 ?>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="100%"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="nowtable">
                  <tr>
                    <td >&nbsp;&nbsp;&nbsp;&nbsp;<strong>报单币购买</strong></td>
                  </tr>
              </table></td>
            </tr>
            <tr>
              <td height="30"><form name="form11" method="post" action="gp_trade.php?action=trade">
                <table width="100%" border="0" cellpadding="3" cellspacing="1">
                  <TR style="display:none">
                    <TD width="32%" align="right"  >&nbsp;</TD>
                    <TD width="22%" height="30" align="right"  >交易方式：</TD>
                    <TD width="46%" height="30" align="left"  ><label>
                      <select name="type">
                        <option value="1">买入</option>
                      </select>
                    </label></TD>
                  </TR>
                  <TR>
                    <TD align="right"  >&nbsp;</TD>
                    <TD height="30" align="right"  >交易股价格：</TD>
                    <TD height="30" align="left"  ><label>
                      <input name="price" type="hidden" id="price" value="<?=$glo_gpcurprice?>" onBlur="gppricedo()"><?=$glo_gpcurprice?>美金/股
                    </label></TD>
                  </TR>
                  <TR>
                    <TD align="center"  >&nbsp;</TD>
                    <TD height="30" align="right"  >交易股数量：</TD>
                    <TD height="30" align="left"  ><label>
                      <input name="num" type="text" id="num" value="<?=$glo_gp_lowertrade?>" onBlur="gppricedo()">
                    </label></TD>
                  </TR>
                  <TR style="display:">
                    <TD align="right"  >&nbsp;</TD>
                    <TD height="30" align="right"  ><strong>总价格:</strong></TD>
                    <TD height="30" align="left"  ><div id="allgpprice"></div></TD>
                  </TR>
                  <TR style="display:none">
                    <TD align="right"  >&nbsp;</TD>
                    <TD height="30" align="right"  >放盘截止日期：</TD>
                    <TD height="30" align="left"  ><label>
                      <input name="jztime" type="text" id="jztime" onClick="WdatePicker()">
                    </label></TD>
                  </TR>
                  <TR>
                    <TD height="30" colspan="3" align="center"  ><INPUT type=submit  id="but1" value="执行交易" name="but1"></TD>
                  </TR>
                </table>
              </form></td>
            </tr>
          </table>
		  
		  <?
		  }
		  ?>
		  
		  <?
		  $sqlhy="select * from {$db_prefix}users where username='".$_SESSION["sys_username"]."'";
			$rshy=$db->get_one($sqlhy);
		  ?>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="100%"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="nowtable">
                  <tr>
                    <td >&nbsp;&nbsp;&nbsp;&nbsp;<strong>原始报单币购买</strong></td>
                  </tr>
              </table></td>
            </tr>
            <tr>
              <td height="30"><form name="form1" method="post" action="?action=buy">
                <table width="100%" height="150" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td width="15%" align="right" bgcolor="#FFFFFF" class="left_txt">&nbsp;</td>
                    <td width="26%" height="30" align="right" bgcolor="#FFFFFF" class="left_txt">购买数量：</td>
                    <td width="59%" align="left" bgcolor="#FFFFFF" class="left_txt"><input name="num" type="text" id="num" size="10" value="<?=$glo_gp_lowertrade?>" onBlur="gppricedo1()"></td>
                  </tr>
                  <tr>
                    <td align="right" bgcolor="#FFFFFF" class="left_txt">系统报单币总数：
                      <?
					echo $glo_gpysgnum;				
					?></td>
                    <td height="30" align="right" bgcolor="#FFFFFF" class="left_txt">报单币当前价格：</td>
                    <td align="left" bgcolor="#FFFFFF" class="left_txt"><? if ($glo_ysgpricebz==1) echo $glo_gp_kpprice;else echo $glo_gpcurprice;?> <input name="price" type="hidden" id="price" value="<? if ($glo_ysgpricebz==1) echo $glo_gp_kpprice;else echo $glo_gpcurprice;?>"></td>
                  </tr>
                  <tr>
                    <td align="right" bgcolor="#FFFFFF" class="left_txt">&nbsp;</td>
                    <td height="30" align="right" bgcolor="#FFFFFF" class="left_txt">总价格：</td>
                    <td align="left" bgcolor="#FFFFFF" class="left_txt"><div id="allgpprice1"><?
					if ($glo_ysgpricebz==1) echo $glo_gp_lowertrade*$glo_gp_kpprice;else echo $glo_gp_lowertrade*$glo_gpcurprice;
					?></div></td>
                  </tr>
                  <tr>
                    <td align="right" bgcolor="#FFFFFF" class="left_txt">&nbsp;</td>
                    <td height="30" align="right" bgcolor="#FFFFFF" class="left_txt">报单币金额：</td>
                    <td align="left" bgcolor="#FFFFFF" class="left_txt"><?=$rshy['gpprice']?></td>
                  </tr>
                  <tr>
                    <td align="right" bgcolor="#FFFFFF" class="left_txt">&nbsp;</td>
                    <td height="30" align="right" bgcolor="#FFFFFF" class="left_txt">&nbsp;</td>
                    <td align="left" bgcolor="#FFFFFF" class="left_txt"><input type="submit" name="Submit" value="购买"></td>
                  </tr>
                </table>
              </form></td>
            </tr>
          </table>
          <p>&nbsp;</p></td>
      </tr>
    </table></td>
    <td background="images/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="middle" background="images/mail_leftbg.gif"><img src="images/buttom_left2.gif" width="17" height="17" /></td>
      <td height="17" valign="top" background="images/buttom_bgs.gif"><img src="images/buttom_bgs.gif" width="17" height="17" /></td>
    <td background="images/mail_rightbg.gif"><img src="images/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>

</body>
</html>
