<?
session_start();
require_once("../config/dbconn.php");

//是否关闭了系统
if($glo_sysclose==1){
	die($glo_closememo);
}

if($act=='userlogin'){
	$msg='';
	if(trim($username)=='') $msg.="请输入用户名\\n";
	if(trim($password)=='') $msg.="请输入密码\\n";
	if(trim($verifycode)=='') $msg.="请输入验证码\\n";
	else{
		if ($_SESSION['lgnrandcode2']!=trim($verifycode))$msg.="验证码错误\\n";
	}
	if ($msg!=''){
		echo "<script>alert('$msg');history.back();</script>";exit();
	}
	$sql="select * from {$db_prefix}users where username='".trim($username)."'";
	$rs=$db->get_one($sql);
	if (!$rs['id']){
		$msg.="用户名或密码错误\\n";
	}else{
		if (authcode($rs['pwd'],"DECODE")!=$password) $msg.="用户名或密码错误\\n";
	}
	if ($msg!=''){
		echo "<script>alert('$msg');history.back();</script>";exit();
	}
	$_SESSION['sys_userid']=$rs['id'];$_SESSION['sys_username']=$rs['username'];
	
	echo "<script>location.href='index.php';</script>";exit();
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML xmlns="http://www.w3.org/1999/xhtml"><HEAD><TITLE>用户登录</TITLE><LINK 
href="images/Default.css" type=text/css rel=stylesheet><LINK 
href="images/xtree.css" type=text/css rel=stylesheet><LINK 
href="images/User_Login.css" type=text/css rel=stylesheet>
<META http-equiv=Content-Type content="text/html; charset=utf-8">
<META content="MSHTML 6.00.6000.16674" name=GENERATOR></HEAD>
<BODY id=userlogin_body>
<DIV></DIV>
  <form action="?act=userlogin" method="post">
<DIV id=user_login>
<DL>
  <DD id=user_top>
  <UL>
    <LI class=user_top_l></LI>
    <LI class=user_top_c></LI>
    <LI class=user_top_r></LI></UL>
  <DD id=user_main>
  <UL>
    <LI class=user_main_l></LI>
    <LI class=user_main_c>
    <DIV class=user_main_box>
    <UL>
      <LI class=user_main_text>用户名： </LI>
      <LI class=user_main_input><INPUT class=TxtUserNameCssClass id=username 
      maxLength=20 name=username> </LI></UL>
    <UL>
      <LI class=user_main_text>密 码： </LI>
      <LI class=user_main_input><INPUT class=TxtPasswordCssClass id=password 
      type=password name=password> </LI></UL>
    <UL>
      <LI class=user_main_text>验证码： </LI>
      <LI class=user_main_input><input class=TxtPasswordCssClass name=verifycode type=text value="" maxLength=4 size=10> <img style="cursor:pointer" title="刷新验证码" id="refresh2" border='0' src='lgncode1.php'
onclick="document.getElementById('refresh2').src='lgncode1.php?t='+Math.random()"/></LI></UL></DIV></LI>
    <LI class=user_main_r><INPUT class=IbtnEnterCssClass id=IbtnEnter 
    style="BORDER-TOP-WIDTH: 0px; BORDER-LEFT-WIDTH: 0px; BORDER-BOTTOM-WIDTH: 0px; BORDER-RIGHT-WIDTH: 0px" type=image src="images/user_botton.gif" name=IbtnEnter> </LI></UL>
 </DD>
  <DD id=user_bottom>
  <UL>
    <LI class=user_bottom_l></LI>
    <LI class=user_bottom_c><SPAN style="MARGIN-TOP: 40px"></SPAN> </LI>
    <LI class=user_bottom_r></LI></UL></DD></DL></DIV><SPAN id=ValrUserName 
style="DISPLAY: none; COLOR: red"></SPAN><SPAN id=ValrPassword 
style="DISPLAY: none; COLOR: red"></SPAN><SPAN id=ValrValidateCode 
style="DISPLAY: none; COLOR: red"></SPAN>
<DIV id=ValidationSummary1 style="DISPLAY: none; COLOR: red"></DIV>
</DD>
</DL></DIV>
</FORM></BODY></HTML>
